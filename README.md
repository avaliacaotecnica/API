# API People Management

Esta api foi desenvolvida em um total de três dias e tem como objetivo a criação de uma base spring para a elaboração de uma api avançada e com múltiplas capacidades.
    
- Ornações
- Filtros
- Arquitetura
- Testando


# Ordenações

  - Ordenar de forma dinâmica as entidades da paginação.
  - Rápida configuração em code. 
  
Ex: 
````java
sort = nome asc, email desc
curl --request GET \
  --url 'http://localhost:9090/contatos?sort=nome%20asc%2C%20email%20desc'
````

Você pode:
  - Adicionar o query filter "sort" com valor "nome asc", que a api irá verificar a propriedade e ordenar os resultados. 
  - Adicionar multiplos campos de ordenação separados com ",". Ex: "nome asc, descricao desc"
  

# Filtros

  - Filtrar de maneira simples e dinâmica.
  - Rápida configuração em code. 
  
Você pode:
  - Adicionar o query filter "filter" com valor a query a ser filtrada. Ex: "nome elike "Teste" and descricao = "None"". Desta maneira simplificando muito a criação de front-ends dinâmicos. 

Ex: 
````java
filter = nome elike "Marlon" and telefone elike "99937"
curl --request GET \
  --url 'http://localhost:9090/contatos?filter=nome%20elike%20%22Marlon%22%20and%20telefone%20elike%20%2299937%22'
````

# Arquitetura

  - Generica
  - Simples de dar manutenção
  - Objectiva
  - Baixa quantidade de arquivos de código
  - Sem necessidade da criação de um DTO
  - Retorno nunca será um Domain. 
  

# Testando
### Pessoa

- O cadastro de pessoa é composto por quatro propriedade obrigatórias sendo que uma delas é uma lista de contatos. 
- Caso não seja informado um id ao vincular os contatos a pessoa, os contatos serão criados e vinculados automaticamente. 

#### Curl

*POST*
````JSON
Criando contato junto.

curl --request POST \
  --url 'http://localhost:9090/pessoas?=' \
  --header 'Content-Type: application/json' \
  --data '{
	"nome": "Exemplo",
	"cpf": "00000000000",
	"dataNascimento": "YYYY-MM-DD",
	"contatos": [
		{
			"nome": "Exemplo",
			"email": "exemplo@exemplo.com",
			"telefone": "00000000000"
		}
	]
}'

Vinculando contato existente

curl --request POST \
  --url 'http://localhost:9090/pessoas?=' \
  --header 'Content-Type: application/json' \
  --data '{
	"nome": "Exemplo",
	"cpf": "00000000000",
	"dataNascimento": "YYYY-MM-DD",
	"contatos": [
		{
		    "id": 1,
			"nome": "Exemplo",
			"email": "exemplo@exemplo.com",
			"telefone": "00000000000"
		}
	]
}'
````


*PUT*
````JSON
curl --request PUT \
  --url http://localhost:9090/pessoas/1 \
  --header 'Content-Type: application/json' \
  --data '{
	"nome": "Exemplo",
	"cpf": "00000000000",
	"dataNascimento": "YYYY-MM-DD",
	"contatos": [
		{
		    "id": 1,
			"nome": "Exemplo",
			"email": "exemplo@exemplo.com",
			"telefone": "00000000000"
		}
	]
}'
````

*DELETE*
````JSON
curl --request DELETE \
  --url http://localhost:9090/pessoas/1 \
  --header 'Content-Type: application/json'
````

*GET*
````JSON
curl --request GET \
  --url http://localhost:9090/pessoas
  
curl --request GET \
  --url http://localhost:9090/pessoas/1
````


### Pedido

- O cadastro de pedido é composto por quatro propriedades obrigatórias sendo que uma delas é uma pessoa.  

#### Curl

*POST*
````JSON
curl --request POST \
  --url 'http://localhost:9090/contatos?=' \
  --header 'Content-Type: application/json' \
  --data '{
	"nome": "Exemplo",
	"email": "exemplo@exemplo.com",
	"telefone": "00000000000"
}'
````


*PUT*
````JSON
curl --request PUT \
  --url http://localhost:9090/contatos/1 \
  --header 'Content-Type: application/json' \
  --data '{
	"nome": "Exemplo",
	"email": "exemplo@exemplo.com",
	"telefone": "00000000000"
}'
````

*DELETE*
````JSON
curl --request DELETE \
  --url http://localhost:9090/contatos/1 \
  --header 'Content-Type: application/json'
````

*GET*
````JSON
curl --request GET \
  --url http://localhost:9090/contatos
  
curl --request GET \
  --url http://localhost:9090/contatos/1
````
