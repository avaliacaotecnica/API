package com.marlon.peoplemanagement.common;

import com.marlon.peoplemanagement.common.model.context.ContextProvider;
import com.marlon.peoplemanagement.common.model.context.CoreAutoConfiguration;
import com.marlon.peoplemanagement.common.model.validation.ValidationAutoConfiguration;
import com.marlon.peoplemanagement.common.model.validation.Validator;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.context.ApplicationContext;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SetUpTeste {

    private ApplicationContext context;

    @Autowired
    Validator validator;

    @Before
    public void setUp() {
        context = mock(ApplicationContext.class);
        AutowireCapableBeanFactory autowireCapableBeanFactory = mock(AutowireCapableBeanFactory.class);
        ContextProvider.setContext(context);
        when(context.getAutowireCapableBeanFactory()).thenReturn(autowireCapableBeanFactory);
        when(context.getBean(Validator.class)).thenReturn(validator);
    }
}
