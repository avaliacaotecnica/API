package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.marlon.peoplemanagement.common.querydsl.ExpressionOperationResolver;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ImportAutoConfiguration({ValidationAutoConfiguration.class,
        JacksonAutoConfiguration.class,
        RepresentationAutoConfiguration.class})
public class ExtensionTest {

    @MockBean
    private ObjectLocator objectLocator;

    @MockBean
    private ExpressionOperationResolver expressionOperationResolver;

    @MockBean
    private FilterExpressionParserConfig filterExpressionParserConfig;

    @Autowired
    private RepresentationMapper mapper;

    @Test
    public void deveSerializarCamposPaiAoSerializarFilho() throws IOException {
        Foo foo = new Foo(1L, "name-1", 123);

        String json = mapper.objectToJson(FooRepresentation.class, foo);

        ObjectNode expected = new ObjectNode(JsonNodeFactory.instance)
                .put("id", 1L)
                .put("tipo", "FOO")
                .put("name", "name-1")
                .put("quantity", 123);

        assertEquals(expected.toString(), json);
    }

    @Test
    public void deveSerializarCamposFilhoAoSerializarRepresentacaoPai() throws IOException {
        Foo foo = new Foo(1L, "name-1", 123);

        String json = mapper.objectToJson(BarRepresentation.class, foo);

        ObjectNode expected = new ObjectNode(JsonNodeFactory.instance)
                .put("id", 1L)
                .put("tipo", "FOO")
                .put("name", "name-1")
                .put("quantity", 123);

        assertEquals(expected.toString(), json);
    }

    @Test
    public void deveDeserializarCamposPaiAoDeserealizerFilho() throws IOException {
        ObjectNode json = new ObjectNode(JsonNodeFactory.instance)
                .put("id", 1L)
                .put("tipo", "FOO")
                .put("name", "name-1")
                .put("quantity", 123);

        when(objectLocator.locate(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(Optional.empty());

        Foo foo = mapper.objectFromJson(FooRepresentation.class, json);

        assertEquals(Long.valueOf(1L), foo.getId());
        assertEquals("name-1", foo.getName());
        assertEquals(Integer.valueOf(123), foo.getQuantity());
    }

    @Test
    public void deveDeserializarCamposFilhoAoDeserealizerRepresentacaoPai() throws IOException {
        ObjectNode json = new ObjectNode(JsonNodeFactory.instance)
                .put("id", 1L)
                .put("tipo", "FOO")
                .put("name", "name-1")
                .put("quantity", 123);

        when(objectLocator.locate(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(Optional.empty());

        Foo foo = (Foo) mapper.objectFromJson(BarRepresentation.class, json);

        assertEquals(Long.valueOf(1L), foo.getId());
        assertEquals("name-1", foo.getName());
        assertEquals(Integer.valueOf(123), foo.getQuantity());
    }

    public enum Tipo {
        FOO, QUX
    }

    public static abstract class Bar {
        private final Tipo tipo;

        private Long id;
        private String name;

        public Bar(Tipo tipo) {
            this.tipo = tipo;
        }

        public Bar(Tipo tipo, Long id, String name) {
            this(tipo);
            this.id = id;
            this.name = name;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Tipo getTipo() {
            return tipo;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static class BarRepresentation implements RepresentationProvider<Bar, Bar> {
        @Override
        public Representation<Bar, Bar> getRepresentation() {
            return RepresentationBuilder.create(Bar.class)
                    .field(Long.class).identifier().name("id").setterMethod(Bar::setId).add()
                    .field(String.class).name("tipo").add()
                    .field(String.class).name("name").setterMethod(Bar::setName).add()
                    .expecialization(
                            FooRepresentation.class,
                            ConditionalExpecialization.jsonAttribute("tipo", "FOO"),
                            object -> object instanceof Foo
                    )
                    .expecialization(
                            QuxRepresentation.class,
                            ConditionalExpecialization.jsonAttribute("tipo", "QUX"),
                            object -> object instanceof Qux
                    )
                    .build();
        }
    }

    public static class Foo extends Bar {
        private Integer quantity;

        public Foo() {
            super(Tipo.FOO);
        }

        public Foo(Long id, String name, Integer quantity) {
            super(Tipo.FOO, id, name);
            this.quantity = quantity;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }
    }

    public static class FooRepresentation implements RepresentationProvider<Foo, Foo> {
        public FooRepresentation() {
        }

        @Override
        public Representation<Foo, Foo> getRepresentation() {
            return RepresentationBuilder.create(Foo.class)
                    .parent(BarRepresentation.class)
                    .field(Integer.class).name("quantity").setterMethod(Foo::setQuantity).add()
                    .createMethod(Foo::new)
                    .build();
        }
    }

    public static class Qux extends Bar {
        private Boolean active;

        public Qux() {
            super(Tipo.QUX);
        }

        public Qux(Long id, String name, Boolean active) {
            super(Tipo.QUX, id, name);
            this.active = active;
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }
    }

    public static class QuxRepresentation implements RepresentationProvider<Qux, Qux> {
        @Override
        public Representation<Qux, Qux> getRepresentation() {
            return RepresentationBuilder.create(Qux.class)
                    .parent(BarRepresentation.class)
                    .field(Boolean.class).name("active").setterMethod(Qux::setActive).add()
                    .createMethod(Qux::new)
                    .build();
        }
    }
}