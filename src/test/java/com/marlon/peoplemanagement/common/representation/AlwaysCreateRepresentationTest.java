package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.marlon.peoplemanagement.common.querydsl.ExpressionOperationResolver;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ImportAutoConfiguration({ValidationAutoConfiguration.class, ValidationAutoConfiguration.class, JacksonAutoConfiguration.class,
        RepresentationAutoConfiguration.class})
public class AlwaysCreateRepresentationTest {

    @MockBean
    private ObjectLocator objectLocator;

    @MockBean
    private ExpressionOperationResolver expressionOperationResolver;

    @MockBean
    private FilterExpressionParserConfig filterExpressionParserConfig;

    @Autowired
    private Deserializer deserializer;

    @Test
    public void deveCriarUmaEntidadeNova() throws IOException {

        Representation<Bar, BarBuilder> representation = RepresentationBuilder.create(Bar.class, BarBuilder.class)
                .field(Long.class).name("id").identifier().add()
                .field(String.class).name("name").add()
                .alwaysCreate()
                .build();

        Bar bar = new Bar(1L, "Teste");

        when(objectLocator.locate(eq(representation), any(), any()))
                .thenReturn(Optional.of(bar));

        ObjectNode json = new ObjectNode(JsonNodeFactory.instance)
                .put("id", 2)
                .put("name", "AAAAAAA");

        Bar fromJson = deserializer.fromJson(representation, null, json, objectLocator);

        assertNotNull(fromJson);
        assertEquals(Long.valueOf(2L), fromJson.getId());
        assertEquals("AAAAAAA", fromJson.getName());

        assertEquals(Long.valueOf(1L), bar.getId());
        assertEquals("Teste", bar.getName());

        verify(objectLocator, times(0)).locate(eq(representation), any(), any());
    }


    public static class BarBuilder {

        private Bar entity;

        public BarBuilder(Bar bar) {
            entity = bar;
        }

        public static BarBuilder create() {
            return new BarBuilder(new Bar());
        }

        public static BarBuilder from(Bar bar) {
            return new BarBuilder(bar);
        }

        public BarBuilder id(Long id) {
            entity.id = id;
            return this;
        }

        public BarBuilder name(String name) {
            entity.name = name;
            return this;
        }

        public Bar build() {
            return entity;
        }
    }

    public static class Bar {
        private Long id;
        private String name;

        public Bar() {
        }

        public Bar(Long id, String name) {
            this.id = id;
            this.name = name;
        }

        public Long getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
}
