package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.marlon.peoplemanagement.common.querydsl.ExpressionOperationResolver;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ImportAutoConfiguration({ValidationAutoConfiguration.class,
        JacksonAutoConfiguration.class,
        RepresentationAutoConfiguration.class})
public class CustomizerTest {

    @MockBean
    ObjectLocator locator;

    @MockBean
    ExpressionOperationResolver operationResolver;

    @MockBean
    FilterExpressionParserConfig filterExpressionParserConfig;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    RepresentationMapper mapper;

    @Test
    public void deveSerializarComSubNiveis() throws IOException {

        PessoaFisica pessoaFisica = PessoaFisica.Builder.create()
                .nome("João")
                .cpf("1023465")
                .rg("44444")
                .build();

        String json = mapper.objectToJson(PessoaRepresentation.class, pessoaFisica);

        JsonNode node = objectMapper.readTree(json);


        assertEquals("João", node.get("nome").asText());

        JsonNode fisicaParser = node.get("fisica");
        assertEquals("1023465", fisicaParser.get("cpf").asText());
        assertEquals("44444", fisicaParser.get("rg").asText());
    }

    @Test
    public void deveDeserializarComSubNiveis() throws IOException {

        when(locator.locate(ArgumentMatchers.any(Representation.class), ArgumentMatchers.any(JsonNode.class), ArgumentMatchers.any(ObjectMapper.class)))
                .thenReturn(Optional.empty());

        JsonNode json = new ObjectNode(JsonNodeFactory.instance)
                .put("nome", "João")
                .set("fisica", new ObjectNode(JsonNodeFactory.instance)
                        .put("cpf", "1023465")
                        .put("rg", "44444")
                );

        PessoaFisica pessoaFisica = mapper.objectFromJson(PessoaRepresentation.class, json);

        assertEquals("João", pessoaFisica.getNome());
        assertEquals("1023465", pessoaFisica.getCpf());
        assertEquals("44444", pessoaFisica.getRg());
    }

    public static class PessoaRepresentation implements RepresentationProvider<PessoaFisica, PessoaFisica.Builder> {

        @Override
        public Representation<PessoaFisica, PessoaFisica.Builder> getRepresentation() {
            return RepresentationBuilder
                    .create(PessoaFisica.class, PessoaFisica.Builder.class)
                    .field(String.class).name("nome").add()
                    .customizer(new RepresentationCustomizer<PessoaFisica, PessoaFisica.Builder>() {

                        @Override
                        public void onSerialize(SerializerContext<PessoaFisica, PessoaFisica.Builder> context) throws IOException {
                            context.getGenerator().writeFieldName("fisica");
                            context.getSerializer().writeObject(context.cloneContext(PessoaFisicaRepresentation.class));
                        }

                        @Override
                        public void onDeserialize(DeserializerContext<PessoaFisica, PessoaFisica.Builder> context) {
                            context.getDeserializer().readFields(context.cloneContext()
                                    .with(PessoaFisicaRepresentation.class, context.getNode().get("fisica")));
                        }
                    })
                    .ignoreUnknownFields()
                    .build();
        }
    }

    public static class PessoaFisicaRepresentation implements RepresentationProvider<PessoaFisica, PessoaFisica.Builder> {

        @Override
        public Representation<PessoaFisica, PessoaFisica.Builder> getRepresentation() {
            return RepresentationBuilder
                    .create(PessoaFisica.class, PessoaFisica.Builder.class)
                    .field(String.class).name("cpf").add()
                    .field(String.class).name("rg").add()
                    .ignoreUnknownFields()
                    .build();
        }
    }

    public static class PessoaFisica {

        private Long id;

        private String nome;

        private String cpf;

        private String rg;

        public Long getId() {
            return id;
        }

        public String getNome() {
            return nome;
        }

        public String getCpf() {
            return cpf;
        }

        public String getRg() {
            return rg;
        }

        public static class Builder {

            private PessoaFisica entity;

            public Builder(PessoaFisica entity) {
                this.entity = entity;
            }

            public static Builder create() {
                return new Builder(new PessoaFisica());
            }

            public static Builder from(PessoaFisica foo) {
                return new Builder(foo);
            }

            public Builder nome(String nome) {
                entity.nome = nome;
                return this;
            }

            public Builder cpf(String cpf) {
                entity.cpf = cpf;
                return this;
            }

            public Builder rg(String rg) {
                entity.rg = rg;
                return this;
            }

            public PessoaFisica build() {
                return entity;
            }
        }
    }


}
