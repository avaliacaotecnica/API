package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.marlon.peoplemanagement.common.querydsl.ExpressionOperationResolver;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ImportAutoConfiguration({ValidationAutoConfiguration.class, JacksonAutoConfiguration.class,
        RepresentationAutoConfiguration.class})
public class ReadOnlyRepresentationTest {

    @MockBean
    private ObjectLocator objectLocator;

    @MockBean
    ExpressionOperationResolver operationResolver;

    @MockBean
    FilterExpressionParserConfig filterExpressionParserConfig;

    @Autowired
    private Deserializer deserializer;

    @Test
    public void deveRetornarAEntidadeSemAlteracao() {

        Representation<Bar, Bar> representation = RepresentationBuilder.create(Bar.class, Bar.class)
                .field(Long.class).name("id").identifier().add()
                .field(String.class).name("name").add()
                .readOnly()
                .build();

        Bar bar = new Bar(1L, "Teste");

        when(objectLocator.locate(ArgumentMatchers.eq(representation), ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Optional.of(bar));

        ObjectNode json = new ObjectNode(JsonNodeFactory.instance)
                .put("id", 1)
                .put("name", "AAAAAAA");

        Bar fromJson = deserializer.fromJson(representation, null, json, objectLocator);

        assertNotNull(fromJson);
        assertEquals(fromJson, bar);
        assertEquals("Teste", fromJson.getName());
    }


    public static class Bar {
        private Long id;
        private String name;

        public Bar() {
        }

        public Bar(Long id, String name) {
            this.id = id;
            this.name = name;
        }

        public Long getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

}
