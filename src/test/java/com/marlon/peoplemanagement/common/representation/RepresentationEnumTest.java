package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marlon.peoplemanagement.common.querydsl.ExpressionOperationResolver;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ImportAutoConfiguration({ValidationAutoConfiguration.class,
        JacksonAutoConfiguration.class,
        RepresentationAutoConfiguration.class})
public class RepresentationEnumTest {

    @MockBean
    ObjectLocator locator;

    @MockBean
    ExpressionOperationResolver operationResolver;

    @MockBean
    FilterExpressionParserConfig filterExpressionParserConfig;

    @Autowired
    RepresentationMapper mapper;

    @Test
    public void deveSerializarEnum() {
        Foo foo = Foo.FooBuilder.create()
                .id(1L)
                .situacao(Foo.Situacao.ATIVO)
                .build();

        String json = mapper.objectToJson(FooRepresentation.class, foo);

        assertEquals("{\"id\":1,\"situacao\":\"ATIVO\"}", json);
    }

    @Test
    public void deveDeserializarEnum() throws IOException {
        when(locator.locate(ArgumentMatchers.any(Representation.class), ArgumentMatchers.any(JsonNode.class), ArgumentMatchers.any(ObjectMapper.class)))
                .thenReturn(Optional.empty());

        String json = "{\"id\":1,\"situacao\":\"ATIVO\"}";
        Foo foo = mapper.objectFromJson(FooRepresentation.class, json);
        assertEquals(Long.valueOf(1L), foo.getId());
        assertEquals(Foo.Situacao.ATIVO, foo.getSituacao());
    }

    @Test(expected = IllegalArgumentException.class)
    public void deveLancarExcecaoAoDeserializarEnum() throws IOException {
        when(locator.locate(ArgumentMatchers.any(Representation.class), ArgumentMatchers.any(JsonNode.class), ArgumentMatchers.any(ObjectMapper.class)))
                .thenReturn(Optional.empty());

        String json = "{\"id\":1,\"situacao\":\"ATVO\"}";
        mapper.objectFromJson(FooRepresentation.class, json);
    }

    public static class FooRepresentation implements RepresentationProvider<Foo, Foo.FooBuilder> {

        @Override
        public Representation<Foo, Foo.FooBuilder> getRepresentation() {
            return RepresentationBuilder.create(Foo.class, Foo.FooBuilder.class)
                    .field(Long.class).name("id").add()
                    .field(Foo.Situacao.class).name("situacao").add()
                    .build();
        }
    }

    public static class Foo {

        private Long id;
        private Situacao situacao;

        public Long getId() {
            return id;
        }

        public Situacao getSituacao() {
            return situacao;
        }

        enum Situacao {
            ATIVO, INATIVO
        }

        public static class FooBuilder {

            private Foo entity;

            public FooBuilder(Foo entity) {
                this.entity = entity;
            }

            public static FooBuilder create() {
                return new FooBuilder(new Foo());
            }

            public static FooBuilder from(Foo foo) {
                return new FooBuilder(foo);
            }

            public FooBuilder id(Long id) {
                entity.id = id;
                return this;
            }

            public FooBuilder situacao(Foo.Situacao situacao) {
                entity.situacao = situacao;
                return this;
            }

            public Foo build() {
                return entity;
            }
        }
    }
}
