package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.marlon.peoplemanagement.common.querydsl.ExpressionOperationResolver;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ImportAutoConfiguration({ValidationAutoConfiguration.class,
        JacksonAutoConfiguration.class,
        RepresentationAutoConfiguration.class})
public class CollectionRepresentationTest {

    @MockBean
    private ObjectLocator objectLocator;

    @MockBean
    private ExpressionOperationResolver expressionOperationResolver;

    @MockBean
    private FilterExpressionParserConfig filterExpressionParserConfig;

    @Autowired
    private Deserializer deserializer;

    @Test
    public void deveRetornarColecao() {

        Representation<Bar, Bar> representation = RepresentationBuilder.create(Bar.class, Bar.class)
                .field(Long.class).name("id").identifier().add()
                .field(String.class).name("name").add()
                .readOnly()
                .build();

        Bar bar = new Bar(1L, "AAAAAAA");

        when(objectLocator.locate(eq(representation), any(), any())).thenReturn(Optional.of(bar));

        ObjectNode firstNode = new ObjectNode(JsonNodeFactory.instance)
                .put("id", 1)
                .put("name", "AAAAAAA");

        ObjectNode secondNode = new ObjectNode(JsonNodeFactory.instance)
                .put("id", 2)
                .put("name", "BBBBBBB");

        ArrayNode json = new ArrayNode(JsonNodeFactory.instance);
        json.add(firstNode);
        json.add(secondNode);

        Collection<Bar> collectionFromJson = deserializer.collectionFromJson(representation, json, objectLocator);

        assertEquals(collectionFromJson.size(), 2);

        Iterator<Bar> iterator = collectionFromJson.iterator();

        Bar fromJson = iterator.next();

        assertNotNull(fromJson);
        assertEquals(fromJson, bar);
        assertEquals("AAAAAAA", fromJson.getName());

    }

    public static class Bar {
        private Long id;
        private String name;

        public Bar() {
        }

        public Bar(Long id, String name) {
            this.id = id;
            this.name = name;
        }

        public Long getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

}
