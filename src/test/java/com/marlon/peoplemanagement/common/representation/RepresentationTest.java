package com.marlon.peoplemanagement.common.representation;

import com.marlon.peoplemanagement.common.querydsl.ExpressionOperationResolver;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ImportAutoConfiguration({
        ValidationAutoConfiguration.class,
        JacksonAutoConfiguration.class,
        RepresentationAutoConfiguration.class})
public class RepresentationTest {

    @MockBean
    ObjectLocator objectLocator;

    @MockBean
    ExpressionOperationResolver operationResolver;

    @MockBean
    FilterExpressionParserConfig filterExpressionParserConfig;

    @Autowired
    private Serializer serializer;

    @Test
    public void deveAceitarSerializarColecao() {
        Representation<Bar, Bar> barRepresentation = RepresentationBuilder.create(Bar.class, Bar.class)
                .field(Long.class).name("id").identifier().getterMethod(Bar::getId).add()
                .field(String.class).name("name").getterMethod(Bar::getName).add()
                .build();

        Representation<Foo, FooBuilder> representation = RepresentationBuilder.create(Foo.class, FooBuilder.class)
                .field(Long.class).identifier().name("id").add()
                .collection(Bar.class, "bars", Foo::getBars).configuration(barRepresentation).add()
                .map(String.class, Bar.class, "barMap").configuration(barRepresentation).add()
                .build();

        Foo foo = FooBuilder.create()
                .id(1L)
                .bar(new Bar(2L, "Teste"))
                .bars(Arrays.asList(new Bar(3L, "Teste2"), new Bar(4L, "Teste3")))
                .build();

        String json = serializer.toJson(representation, foo);

        //JSONAssert.assertEquals();

        String expected = "{\"id\":1,\"bars\":[{\"id\":3,\"name\":\"Teste2\"},{\"id\":4,\"name\":\"Teste3\"}]}";

        assertEquals(expected, json);
    }

    public static class Foo {

        private Long id;

        private Bar bar;

        private List<Bar> bars = new ArrayList<>();

        public Long getId() {
            return id;
        }

        public Bar getBar() {
            return bar;
        }

        public List<Bar> getBars() {
            return bars;
        }
    }

    public static class Bar {

        private Long id;

        private String name;

        public Bar() {
        }

        public Bar(Long id, String name) {
            this.id = id;
            this.name = name;
        }

        public Long getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    private static class FooBuilder {

        private Foo entity;

        public FooBuilder(Foo entity) {
            this.entity = entity;
        }

        public static FooBuilder create() {
            return new FooBuilder(new Foo());
        }

        public static FooBuilder from(Foo foo) {
            return new FooBuilder(foo);
        }

        public FooBuilder id(Long id) {
            entity.id = id;
            return this;
        }

        public FooBuilder bar(Bar bar) {
            entity.bar = bar;
            return this;
        }

        public FooBuilder bars(Collection<Bar> bars) {
            entity.bars.addAll(bars);
            return this;
        }

        public Foo build() {
            return entity;
        }
    }


}