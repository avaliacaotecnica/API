package com.marlon.peoplemanagement.contato;

import com.marlon.peoplemanagement.api.ContatoController;
import com.marlon.peoplemanagement.common.SetUpTeste;
import com.marlon.peoplemanagement.common.model.Page;
import com.marlon.peoplemanagement.common.model.Pageable;
import com.marlon.peoplemanagement.common.model.Sort;
import com.marlon.peoplemanagement.common.model.context.CoreAutoConfiguration;
import com.marlon.peoplemanagement.common.model.validation.ValidationAutoConfiguration;
import com.marlon.peoplemanagement.model.Contato;
import com.marlon.peoplemanagement.repositorys.BasicRepository;
import com.marlon.peoplemanagement.services.contatos.ContatoService;
import com.querydsl.core.BooleanBuilder;
import org.jglue.cdiunit.AdditionalClasses;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static com.marlon.peoplemanagement.model.QContato.contato;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@AdditionalClasses({SetUpTeste.class})
@ImportAutoConfiguration({ValidationAutoConfiguration.class, CoreAutoConfiguration.class})
public class ContatoControllerTest {

    ContatoService service;
    BasicRepository repository;
    ContatoController controller;

    private final static int LIMIT = 20;
    private final static int OFFSET = 0;
    private final static long TOTAL = 1L;
    private final static long CONTATO_ID = 1L;

    @Before
    public void before(){
        this.repository = mock(BasicRepository.class);
        this.service = mock(ContatoService.class);
        this.controller = mock(ContatoController.class);
    }

    @Test
    public void shouldGetPageContatos() {

        List<Contato> content = Collections.singletonList(mock(Contato.class));
        Pageable pageable = Pageable.of(OFFSET, LIMIT, Sort.of(contato.nome.asc()));

        Page<Contato> page = new Page<>(content, pageable, TOTAL);
        when(repository.findAll(Contato.class, pageable, new BooleanBuilder())).thenReturn(page);

        Page<Contato> repositoryPage = repository.findAll(Contato.class, pageable, new BooleanBuilder());
        when(service.getPage(any(), any())).thenReturn(repositoryPage);

        Page<Contato> servicePage = service.getPage(pageable, new BooleanBuilder());
        when(controller.getPage(OFFSET, LIMIT, "", Sort.of(contato.nome.asc()))).thenReturn(servicePage);

        Page<Contato> controlerPage = controller.getPage(OFFSET, LIMIT, "", Sort.of(contato.nome.asc()));

        assertNotNull(controlerPage);
        assertEquals(controlerPage.getTotal(), page.getTotal());
        assertEquals(controlerPage.getPageable().getLimit(), page.getPageable().getLimit());
        assertEquals(controlerPage.getPageable().getOffset(), page.getPageable().getOffset());
        assertEquals(controlerPage.getPageable().getSort(), page.getPageable().getSort());
    }

    @Test
    public void shouldGetContatoById() {

        Contato entity = ContatoTestUtils.criarContatoPadrao(CONTATO_ID).build();
        when(repository.find(Contato.class, CONTATO_ID)).thenReturn(entity);

        Contato repositoryContato = repository.find(Contato.class, CONTATO_ID);
        when(service.getById(CONTATO_ID)).thenReturn(repositoryContato);

        Contato serviceContato = service.getById(CONTATO_ID);
        when(controller.getById(CONTATO_ID)).thenReturn(serviceContato);

        Contato contato = controller.getById(CONTATO_ID);

        assertNotNull(contato);
        assertEquals(contato.getId(), entity.getId());
        assertEquals(contato.getEmail(), entity.getEmail());
        assertEquals(contato.getTelefone(), entity.getTelefone());
        assertEquals(contato.getNome(), entity.getNome());
    }

    @Test
    public void shouldCreateContato() {

        Contato entity = ContatoTestUtils.criarContatoPadrao(CONTATO_ID).build();
        when(repository.save(any())).thenReturn(entity);

        Contato repositoryContato = repository.save(any());
        when(service.save(any())).thenReturn(repositoryContato);

        Contato serviceContato = service.save(any());
        when(controller.create(any())).thenReturn(serviceContato);

        Contato contato = controller.create(ContatoTestUtils.criarContatoPadrao(null).build());

        assertNotNull(contato);
        assertEquals(contato, entity);
    }

    @Test
    public void shouldMergeContato() {

        Contato entity = ContatoTestUtils.criarContatoPadrao(CONTATO_ID).build();
        when(repository.save(any())).thenReturn(entity);

        Contato repositoryContato = repository.save(any());
        when(service.save(any())).thenReturn(repositoryContato);

        Contato serviceContato = service.save(ContatoTestUtils.criarContatoPadrao(CONTATO_ID).build());
        when(controller.update(any(), any())).thenReturn(serviceContato);

        Contato contato = controller.update(CONTATO_ID, ContatoTestUtils.criarContatoPadrao(null).build());

        assertNotNull(contato);
        assertEquals(contato, entity);
    }

    @Test
    public void shouldDeleteContato() {
        controller.delete(CONTATO_ID);
    }
}
