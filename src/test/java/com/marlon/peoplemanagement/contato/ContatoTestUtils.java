package com.marlon.peoplemanagement.contato;

import com.marlon.peoplemanagement.model.Contato;

public class ContatoTestUtils {

    public static Contato.Builder criarContatoPadrao(Long id){
        return Contato.Builder.create()
                .id(id)
                .email("aaa@aaa.com")
                .nome("Marlon")
                .telefone("48999378172");
    }

}
