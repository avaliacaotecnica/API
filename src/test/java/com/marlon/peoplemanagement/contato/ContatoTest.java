package com.marlon.peoplemanagement.contato;

import com.marlon.peoplemanagement.common.SetUpTeste;
import com.marlon.peoplemanagement.common.model.context.CoreAutoConfiguration;
import com.marlon.peoplemanagement.common.model.validation.ValidationAutoConfiguration;
import org.jglue.cdiunit.AdditionalClasses;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;

@RunWith(SpringRunner.class)
@ImportAutoConfiguration({ValidationAutoConfiguration.class, CoreAutoConfiguration.class})
@AdditionalClasses({SetUpTeste.class})
public class ContatoTest {

    private final static long CONTATO_ID = 1L;

    @Test
    public void shouldCreateContato() {
            ContatoTestUtils.criarContatoPadrao(CONTATO_ID).build();
    }

    @Test(expected = ConstraintViolationException.class)
    public void notShouldCreateContatoWithoutEmail() {
        ContatoTestUtils.criarContatoPadrao(CONTATO_ID).email(null).build();
    }

    @Test(expected = ConstraintViolationException.class)
    public void notShouldCreateContatoWithoutEmailCorrectFormat() {
        ContatoTestUtils.criarContatoPadrao(CONTATO_ID).email("aaa@aaa").build();
    }

    @Test(expected = ConstraintViolationException.class)
    public void notShouldCreateContatoWithoutNome() {
        ContatoTestUtils.criarContatoPadrao(CONTATO_ID).nome(null).build();
    }

    @Test(expected = ConstraintViolationException.class)
    public void notShouldCreateContatoWithoutTelefone() {
        ContatoTestUtils.criarContatoPadrao(CONTATO_ID).telefone(null).build();
    }
}
