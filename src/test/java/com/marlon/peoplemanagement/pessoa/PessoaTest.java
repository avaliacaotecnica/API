package com.marlon.peoplemanagement.pessoa;

import com.marlon.peoplemanagement.common.SetUpTeste;
import com.marlon.peoplemanagement.common.model.context.CoreAutoConfiguration;
import com.marlon.peoplemanagement.common.model.validation.ValidationAutoConfiguration;
import com.marlon.peoplemanagement.common.model.validation.ValidationException;
import org.jglue.cdiunit.AdditionalClasses;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
@ImportAutoConfiguration({ValidationAutoConfiguration.class, CoreAutoConfiguration.class})
@AdditionalClasses({SetUpTeste.class})
public class PessoaTest {

    private final static long CONTATO_ID = 1L;

    @Test
    public void shouldCreateContato() {
            PessoaTestUtils.criarPessoaPadrao(CONTATO_ID).build();
    }

    @Test(expected = ConstraintViolationException.class)
    public void notShouldCreateContatoWithoutCPF() {
        PessoaTestUtils.criarPessoaPadrao(CONTATO_ID).cpf(null).build();
    }

    @Test(expected = ValidationException.class)
    public void notShouldCreatePessoaWithoutCpfCorrectFormat() {
        PessoaTestUtils.criarPessoaPadrao(CONTATO_ID).cpf("65156151156").build();
    }

    @Test(expected = ConstraintViolationException.class)
    public void notShouldCreatePessoaWithoutCpfCorrectSize() {
        PessoaTestUtils.criarPessoaPadrao(CONTATO_ID).cpf("65156").build();
    }

    @Test(expected = ConstraintViolationException.class)
    public void notShouldCreateContatoWithoutNome() {
        PessoaTestUtils.criarPessoaPadrao(CONTATO_ID).nome(null).build();
    }

    @Test(expected = ConstraintViolationException.class)
    public void notShouldCreateContatoWithoutDataNascimento() {
        PessoaTestUtils.criarPessoaPadrao(CONTATO_ID).dataNascimento(null).build();
    }

    @Test(expected = ValidationException.class)
    public void notShouldCreateContatoWithoutDataNascimentoBoforeToday() {
        PessoaTestUtils.criarPessoaPadrao(CONTATO_ID).dataNascimento(LocalDate.now().plusDays(10)).build();
    }


    @Test(expected = ConstraintViolationException.class)
    public void notShouldCreateContatoWithoutContato() {
        PessoaTestUtils.criarPessoaPadrao(CONTATO_ID).contatos(null).build();
    }
}
