package com.marlon.peoplemanagement.pessoa;

import com.marlon.peoplemanagement.api.PessoaController;
import com.marlon.peoplemanagement.common.SetUpTeste;
import com.marlon.peoplemanagement.common.model.Page;
import com.marlon.peoplemanagement.common.model.Pageable;
import com.marlon.peoplemanagement.common.model.Sort;
import com.marlon.peoplemanagement.common.model.context.CoreAutoConfiguration;
import com.marlon.peoplemanagement.common.model.validation.ValidationAutoConfiguration;
import com.marlon.peoplemanagement.model.Pessoa;
import com.marlon.peoplemanagement.repositorys.BasicRepository;
import com.marlon.peoplemanagement.services.pessoas.PessoaService;
import com.querydsl.core.BooleanBuilder;
import org.jglue.cdiunit.AdditionalClasses;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static com.marlon.peoplemanagement.model.QPessoa.pessoa;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@AdditionalClasses({SetUpTeste.class})
@ImportAutoConfiguration({ValidationAutoConfiguration.class, CoreAutoConfiguration.class})
public class PessoaControllerTest {

    PessoaService service;
    BasicRepository repository;
    PessoaController controller;

    private final static int LIMIT = 20;
    private final static int OFFSET = 0;
    private final static long TOTAL = 1L;
    private final static long PESSOA_ID = 1L;

    @Before
    public void before(){
        this.repository = mock(BasicRepository.class);
        this.service = mock(PessoaService.class);
        this.controller = mock(PessoaController.class);
    }

    @Test
    public void shouldGetPagePessoas() {

        List<Pessoa> content = Collections.singletonList(mock(Pessoa.class));
        Pageable pageable = Pageable.of(OFFSET, LIMIT, Sort.of(pessoa.nome.asc()));

        Page<Pessoa> page = new Page<>(content, pageable, TOTAL);
        when(repository.findAll(Pessoa.class, pageable, new BooleanBuilder())).thenReturn(page);

        Page<Pessoa> repositoryPage = repository.findAll(Pessoa.class, pageable, new BooleanBuilder());
        when(service.getPage(any(), any())).thenReturn(repositoryPage);

        Page<Pessoa> servicePage = service.getPage(pageable, new BooleanBuilder());
        when(controller.getPage(OFFSET, LIMIT, "", Sort.of(pessoa.nome.asc()))).thenReturn(servicePage);

        Page<Pessoa> controlerPage = controller.getPage(OFFSET, LIMIT, "", Sort.of(pessoa.nome.asc()));

        assertNotNull(controlerPage);
        assertEquals(controlerPage.getTotal(), page.getTotal());
        assertEquals(controlerPage.getPageable().getLimit(), page.getPageable().getLimit());
        assertEquals(controlerPage.getPageable().getOffset(), page.getPageable().getOffset());
        assertEquals(controlerPage.getPageable().getSort(), page.getPageable().getSort());
    }

    @Test
    public void shouldGetPessoaById() {

        Pessoa entity = PessoaTestUtils.criarPessoaPadrao(PESSOA_ID).build();
        when(repository.find(Pessoa.class, PESSOA_ID)).thenReturn(entity);

        Pessoa repositoryPessoa = repository.find(Pessoa.class, PESSOA_ID);
        when(service.getById(PESSOA_ID)).thenReturn(repositoryPessoa);

        Pessoa servicePessoa = service.getById(PESSOA_ID);
        when(controller.getById(PESSOA_ID)).thenReturn(servicePessoa);

        Pessoa pessoa = controller.getById(PESSOA_ID);

        assertNotNull(pessoa);
        assertEquals(pessoa.getId(), entity.getId());
        assertEquals(pessoa.getCpf(), entity.getCpf());
        assertEquals(pessoa.getDataNascimento(), entity.getDataNascimento());
        assertEquals(pessoa.getNome(), entity.getNome());
        assertEquals(pessoa.getContatos(), entity.getContatos());
    }

    @Test
    public void shouldCreatePessoa() {

        Pessoa entity = PessoaTestUtils.criarPessoaPadrao(PESSOA_ID).build();
        when(repository.save(any())).thenReturn(entity);

        Pessoa repositoryPessoa = repository.save(any());
        when(service.save(any())).thenReturn(repositoryPessoa);

        Pessoa servicePessoa = service.save(any());
        when(controller.create(any())).thenReturn(servicePessoa);

        Pessoa pessoa = controller.create(PessoaTestUtils.criarPessoaPadrao(null).build());

        assertNotNull(pessoa);
        assertEquals(pessoa, entity);
    }

    @Test
    public void shouldMergePessoa() {

        Pessoa entity = PessoaTestUtils.criarPessoaPadrao(PESSOA_ID).build();
        when(repository.save(any())).thenReturn(entity);

        Pessoa repositoryPessoa = repository.save(any());
        when(service.save(any())).thenReturn(repositoryPessoa);

        Pessoa servicePessoa = service.save(PessoaTestUtils.criarPessoaPadrao(PESSOA_ID).build());
        when(controller.update(any(), any())).thenReturn(servicePessoa);

        Pessoa pessoa = controller.update(PESSOA_ID, PessoaTestUtils.criarPessoaPadrao(null).build());

        assertNotNull(pessoa);
        assertEquals(pessoa, entity);
    }

    @Test
    public void shouldDeletePessoa() {
        controller.delete(PESSOA_ID);
    }
}
