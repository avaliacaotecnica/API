package com.marlon.peoplemanagement.pessoa;

import com.marlon.peoplemanagement.contato.ContatoTestUtils;
import com.marlon.peoplemanagement.model.Contato;
import com.marlon.peoplemanagement.model.Pessoa;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;

public class PessoaTestUtils {

    public static Pessoa.Builder criarPessoaPadrao(Long id){
        return Pessoa.Builder.create()
                .id(id)
                .nome("Marlon")
                .cpf("24405539057")
                .dataNascimento(LocalDate.of(2010, 12, 25))
                .contatos(new HashSet<>(Arrays.asList(ContatoTestUtils.criarContatoPadrao(1L).build())));
    }

}
