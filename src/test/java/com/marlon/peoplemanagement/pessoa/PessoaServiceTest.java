package com.marlon.peoplemanagement.pessoa;

import com.marlon.peoplemanagement.common.SetUpTeste;
import com.marlon.peoplemanagement.common.model.Page;
import com.marlon.peoplemanagement.common.model.Pageable;
import com.marlon.peoplemanagement.common.model.Sort;
import com.marlon.peoplemanagement.common.model.context.CoreAutoConfiguration;
import com.marlon.peoplemanagement.common.model.validation.ValidationAutoConfiguration;
import com.marlon.peoplemanagement.model.Pessoa;
import com.marlon.peoplemanagement.repositorys.BasicRepository;
import com.marlon.peoplemanagement.services.pessoas.PessoaService;
import com.querydsl.core.BooleanBuilder;
import org.jglue.cdiunit.AdditionalClasses;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static com.marlon.peoplemanagement.model.QPessoa.pessoa;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@AdditionalClasses({SetUpTeste.class})
@ImportAutoConfiguration({ValidationAutoConfiguration.class, CoreAutoConfiguration.class})
public class PessoaServiceTest {

    PessoaService service;
    BasicRepository repository;

    private final static int LIMIT = 20;
    private final static int OFFSET = 0;
    private final static long TOTAL = 1L;
    private final static long CONTATO_ID = 1L;

    @Before
    public void before(){
        this.repository = mock(BasicRepository.class);
        this.service = mock(PessoaService.class);
    }

    @Test
    public void shouldGetPagePessoas() {

        List<Pessoa> content = Collections.singletonList(mock(Pessoa.class));
        Pageable pageable = Pageable.of(OFFSET, LIMIT, Sort.of(pessoa.nome.asc()));

        Page<Pessoa> page = new Page<>(content, pageable, TOTAL);
        when(repository.findAll(Pessoa.class, pageable, new BooleanBuilder())).thenReturn(page);

        Page<Pessoa> repositoryPage = repository.findAll(Pessoa.class, pageable, new BooleanBuilder());
        when(service.getPage(any(), any())).thenReturn(repositoryPage);

        Page<Pessoa> pessoas = service.getPage(pageable, new BooleanBuilder());

        assertNotNull(pessoas);
        assertEquals(pessoas.getTotal(), page.getTotal());
        assertEquals(pessoas.getPageable().getLimit(), page.getPageable().getLimit());
        assertEquals(pessoas.getPageable().getOffset(), page.getPageable().getOffset());
        assertEquals(pessoas.getPageable().getSort(), page.getPageable().getSort());
    }

    @Test
    public void shouldGetPessoaById() {

        Pessoa entity = PessoaTestUtils.criarPessoaPadrao(CONTATO_ID).build();
        when(repository.find(Pessoa.class, CONTATO_ID)).thenReturn(entity);

        Pessoa repositoryPessoa = repository.find(Pessoa.class, CONTATO_ID);
        when(service.getById(CONTATO_ID)).thenReturn(repositoryPessoa);

        Pessoa pessoa = service.getById(CONTATO_ID);

        assertNotNull(pessoa);
        assertEquals(pessoa.getId(), entity.getId());
        assertEquals(pessoa.getCpf(), entity.getCpf());
        assertEquals(pessoa.getDataNascimento(), entity.getDataNascimento());
        assertEquals(pessoa.getNome(), entity.getNome());
        assertEquals(pessoa.getContatos(), entity.getContatos());
    }

    @Test
    public void shouldCreatePessoa() {

        Pessoa entity = PessoaTestUtils.criarPessoaPadrao(CONTATO_ID).build();
        when(repository.save(any())).thenReturn(entity);

        Pessoa repositoryPessoa = repository.save(any());
        when(service.save(any())).thenReturn(repositoryPessoa);

        Pessoa pessoa = service.save(PessoaTestUtils.criarPessoaPadrao(null).build());

        assertNotNull(pessoa);
        assertEquals(pessoa, entity);
    }

    @Test
    public void shouldMergePessoa() {

        Pessoa entity = PessoaTestUtils.criarPessoaPadrao(CONTATO_ID).build();
        when(repository.save(any())).thenReturn(entity);

        Pessoa repositoryPessoa = repository.save(any());
        when(service.save(any())).thenReturn(repositoryPessoa);

        Pessoa pessoa = service.save(PessoaTestUtils.criarPessoaPadrao(CONTATO_ID).build());

        assertNotNull(pessoa);
        assertEquals(pessoa, entity);
    }

    @Test
    public void shouldDeletePessoa() {
        service.remove(CONTATO_ID);
    }
}
