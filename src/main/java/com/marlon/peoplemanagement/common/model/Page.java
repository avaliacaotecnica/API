package com.marlon.peoplemanagement.common.model;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Objects;

public class Page<E> implements Iterable<E> {
    private final Collection<E> content;
    private final Pageable pageable;
    private final long total;

    public static final Page EMPTY = new Page(Collections.emptyList(), Pageable.of(0, 0), 0);

    public Page(Collection<E> content, Pageable pageable, long total) {
        Objects.requireNonNull(content);
        Objects.requireNonNull(pageable);
        this.content = content;
        this.pageable = pageable;
        this.total = total;
    }

    public Collection<E> getContent() {
        return content;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public long getTotal() {
        return total;
    }

    public Iterator<E> iterator() {
        return this.content.iterator();
    }

    public static <E> Page<E> empty() {
        return EMPTY;
    }
}