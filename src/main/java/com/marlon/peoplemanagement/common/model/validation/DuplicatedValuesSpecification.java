package com.marlon.peoplemanagement.common.model.validation;

import java.util.List;

public interface DuplicatedValuesSpecification {

    List<Long> getDuplicatedValues();

}
