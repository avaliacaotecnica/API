package com.marlon.peoplemanagement.common.model.validation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration.class)
public class ValidationAutoConfiguration {

    @Bean
    public Validator validator(javax.validation.Validator beanValidator,
                               ApplicationContext applicationContext) {
        return new Validator(beanValidator, applicationContext);
    }
}
