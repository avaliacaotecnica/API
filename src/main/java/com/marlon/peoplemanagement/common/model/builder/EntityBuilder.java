package com.marlon.peoplemanagement.common.model.builder;

import com.marlon.peoplemanagement.common.model.context.ContextProvider;
import com.marlon.peoplemanagement.common.model.validation.Validator;
import com.marlon.peoplemanagement.common.model.validation.ISpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

public abstract class EntityBuilder<T> implements IBuilder<T> {

    private Validator validator;

    protected T entity;

    private boolean validate = true;

    protected final EntityState state;

    private List<ISpecification> specifications;

    private List<Class<? extends ISpecification>> specificationClasses;

    protected EntityBuilder(T entity, EntityState state) {
        this(entity, state, true);
    }

    protected EntityBuilder(T entity, EntityState state, boolean autoinject) {
        requireNonNull(entity);
        requireNonNull(state);

        this.state = state;
        this.entity = entity;

        if (autoinject) {
            ApplicationContext applicationContext = ContextProvider.get();
            this.validator = (Validator)applicationContext.getBean(Validator.class);
            applicationContext.getAutowireCapableBeanFactory().autowireBean(this);
        }
    }

    public <B extends EntityBuilder<T>> B validate(final boolean validate) {
        this.validate = validate;
        return (B) this;
    }

    public <B extends EntityBuilder<T>> B skipValidate() {
        return (B) this.validate(false);
    }

    public <B extends EntityBuilder<T>> B addSpecification(ISpecification<T, ?> specification) {
        if (isNull(specifications)) {
            specifications = new ArrayList<>();
        }

        specifications.add(specification);
        return (B) this;
    }

    public <B extends EntityBuilder<T>> B addSpecificationClass(Class<? extends ISpecification<T, ?>> specification) {
        if (isNull(specificationClasses)) {
            specificationClasses = new ArrayList<>();
        }

        specificationClasses.add(specification);
        return (B) this;
    }

    public <B extends EntityBuilder<T>> B withValidator(Validator validator) {
        this.validator = validator;
        return (B) this;
    }

    protected void beforeValidate() {
    }

    protected void afterValidate() {
    }

    protected void validate(final Validator validator) {
        requireNonNull(validator, "É necessário fornecer um validator");
        validator.validate(entity);
        validator.validate(entity, specifications, specificationClasses);
    }

    @Override
    public final T build() {
        requireNonNull(entity);

        if (validate) {
            beforeValidate();
            validate(validator);
            afterValidate();
        }

        try {
            return entity;
        } finally {
            entity = null;
        }
    }

    protected enum EntityState {
        NEW,
        BUILT
    }

}
