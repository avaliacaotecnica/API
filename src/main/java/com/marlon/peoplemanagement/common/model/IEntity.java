package com.marlon.peoplemanagement.common.model;

import java.io.Serializable;

public interface IEntity<T> extends Serializable {

    T getId();
}
