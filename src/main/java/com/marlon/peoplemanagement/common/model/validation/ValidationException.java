package com.marlon.peoplemanagement.common.model.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.*;
import java.util.stream.Collectors;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Bad Request")
public class ValidationException extends javax.validation.ValidationException {

    private final Set<ValidationMessage> validationMessages;

    protected ValidationException(Set<ValidationMessage> validationMessages) {
        super(validationMessages.stream().map(ValidationMessage::getMessage)
                .collect(Collectors.joining(", ")));
        this.validationMessages = validationMessages;
    }


    public ValidationException(String validationMessage) {
        super(validationMessage);
        this.validationMessages = new HashSet(Arrays.asList(new ValidationMessage(validationMessage)));
    }


    public Set<ValidationMessage> getValidationMessages() {
        return Collections.unmodifiableSet(validationMessages);
    }

}