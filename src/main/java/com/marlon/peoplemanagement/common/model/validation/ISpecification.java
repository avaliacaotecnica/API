package com.marlon.peoplemanagement.common.model.validation;

public interface ISpecification<T, E extends ISpecification<T, E>> {

    boolean isSatisfiedBy(T candidate);

    E and(E right);

    E or(E right);

    E not();

    String getBean();

    String getMessage();

    String getProperty();

    E setBean(String bean);

    E setMessage(String message);

    E setProperty(String property);
}
