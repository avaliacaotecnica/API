package com.marlon.peoplemanagement.common.model.validation;

import org.springframework.context.ApplicationContext;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.groups.Default;
import java.util.*;

public class Validator {

    private final javax.validation.Validator beanValidator;

    private final ApplicationContext applicationContext;

    public Validator(javax.validation.Validator beanValidator, ApplicationContext applicationContext) {
        Objects.requireNonNull(beanValidator);
        Objects.requireNonNull(applicationContext);

        this.applicationContext = applicationContext;
        this.beanValidator = beanValidator;
    }

    public <T> T validate(final T bean, final Class<?>... groups) {
        final Set<ConstraintViolation<T>> violations = beanValidator.validate(bean, groups);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(new LinkedHashSet<ConstraintViolation<?>>(
                    violations));
        }

        return bean;
    }

    public <T> T validate(T bean, ISpecification specification) {
        return this.validate(bean, Collections.singletonList(specification), Collections.emptyList());
    }

    public <T> T validate(final T bean) {
        validate(bean, Default.class);
        return bean;
    }

    public <T> T validate(final T bean, final List<ISpecification> specifications,
                          List<Class<? extends ISpecification>> specificationClasses) {
        SpecificationValidator.create(applicationContext)
                .addSpecifications(specifications)
                .addSpecificationClasses(specificationClasses)
                .validateWithException(bean);
        return bean;
    }

}
