package com.marlon.peoplemanagement.common.model;

import java.util.Objects;

public final class Pageable {
    private final long offset;
    private final long limit;
    private final Sort sort;

    private static int LIMIT_NUMBER = 1000;
    private static int DEFAULT_LIMIT = 25;
    private static int ZERO = 0;

    public long getOffset() {
        return offset;
    }

    public long getLimit() {
        return limit;
    }

    public Sort getSort() {
        return sort;
    }

    private Pageable(int offset, int limit, Sort sort) {
        if (ZERO > offset) {
            throw new IllegalArgumentException("Offset must not be less than zero!");
        } else if (ZERO > limit) {
            throw new IllegalArgumentException("Limit must not be less than zero!");
        } else if (limit > LIMIT_NUMBER) {
            throw new IllegalArgumentException("Limit must not be greater than 1000!");
        } else {
            if (ZERO == limit) {
                limit = DEFAULT_LIMIT;
            }

            this.offset = offset;
            this.limit = limit;
            this.sort = sort;
        }
    }

    public static Pageable of(int offset, int limit) {
        return of(offset, limit, null);
    }

    public static Pageable of(int offset, int limit, Sort sort) {
        return new Pageable(offset, limit, sort);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (!(obj instanceof Pageable)) {
            return false;
        } else {
            Pageable that = (Pageable)obj;
            boolean offsetEqual = this.offset == that.offset;
            boolean limitEqual = this.limit == that.limit;
            boolean sortEqual = Objects.equals(this.sort, that.sort);
            return offsetEqual && limitEqual && sortEqual;
        }
    }
}
