package com.marlon.peoplemanagement.common.model.validation;

public class NotSpecification<T> extends BasicSpecification<T> {

    private ISpecification<T, BasicSpecification<T>> wrapped;
    private boolean result;

    public NotSpecification(ISpecification<T, BasicSpecification<T>> wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public String getBean() {
        if (super.getBean() != null) {
            return super.getBean();
        } else if (!result) {
            return wrapped.getBean();
        }
        return null;
    }

    @Override
    public String getMessage() {
        if (super.getMessage() != null) {
            return super.getMessage();
        } else if (!result) {
            return wrapped.getMessage();
        }
        return null;
    }

    @Override
    public String getProperty() {
        if (super.getProperty() != null) {
            return super.getProperty();
        } else if (!result) {
            return wrapped.getProperty();
        }
        return null;
    }

    @Override
    public boolean isSatisfiedBy(T candidate) {
        this.result = wrapped.isSatisfiedBy(candidate);

        return !result;
    }

    @Override
    public String toString() {
        return "!" + wrapped;
    }
}
