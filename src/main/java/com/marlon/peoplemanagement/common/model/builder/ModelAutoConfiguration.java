package com.marlon.peoplemanagement.common.model.builder;

import com.marlon.peoplemanagement.common.model.context.CoreAutoConfiguration;
import com.marlon.peoplemanagement.common.model.validation.ValidationAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigureAfter({CoreAutoConfiguration.class, ValidationAutoConfiguration.class})
public class ModelAutoConfiguration {

}