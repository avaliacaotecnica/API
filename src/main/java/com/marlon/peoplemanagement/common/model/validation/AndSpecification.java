package com.marlon.peoplemanagement.common.model.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AndSpecification<T> extends BasicSpecification<T> {

    private ISpecification<T, BasicSpecification<T>> a;
    private ISpecification<T, BasicSpecification<T>> b;
    private boolean resultA;
    private boolean resultB;

    public AndSpecification(ISpecification<T, BasicSpecification<T>> a,
                            ISpecification<T, BasicSpecification<T>> b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public String getBean() {
        if (super.getBean() != null) {
            return super.getBean();
        } else if (!resultA) {
            return a.getBean();
        } else if (!resultB) {
            return b.getBean();
        }
        return null;
    }

    @Override
    public String getMessage() {
        if (super.getMessage() != null) {
            return super.getMessage();
        } else if (!resultA) {
            return a.getMessage();
        } else if (!resultB) {
            return b.getMessage();
        }
        return null;
    }

    @Override
    public String getProperty() {
        if (super.getProperty() != null) {
            return super.getProperty();
        } else if (!resultA) {
            return a.getProperty();
        } else if (!resultB) {
            return b.getProperty();
        }
        return null;
    }

    @Override
    public boolean isSatisfiedBy(T candidate) {
        this.resultA = a.isSatisfiedBy(candidate);
        this.resultB = b.isSatisfiedBy(candidate);

        return this.resultA && this.resultB;
    }

    @Override
    public String toString() {
        return "(" + a + " && " + b + ")";
    }

    public static class Builder<T> {

        private final List<BasicSpecification<T>> specifications = new ArrayList<>();

        public Builder(BasicSpecification<T>... specifications) {
            this.specifications.addAll(Arrays.asList(specifications));
        }

        public Builder<T> and(BasicSpecification<T> specification) {
            this.specifications.add(specification);
            return this;
        }

        public BasicSpecification<T> build() {
            if (specifications.isEmpty()) {
                return new BasicSpecification<T>() {
                    @Override
                    public boolean isSatisfiedBy(T candidate) {
                        return true;
                    }
                };
            } else if (specifications.size() == 1) {
                return specifications.get(0);
            } else {
                AndSpecification andSpecification = new AndSpecification(specifications.get(0), specifications.get(1));

                for (int i = 2; i < specifications.size(); i++) {
                    andSpecification = new AndSpecification(andSpecification, specifications.get(i));
                }

                return andSpecification;
            }
        }
    }
}
