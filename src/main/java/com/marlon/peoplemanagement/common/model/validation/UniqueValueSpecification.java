package com.marlon.peoplemanagement.common.model.validation;

import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;

public class UniqueValueSpecification<T> extends BasicSpecification<T>
        implements DuplicatedValuesSpecification {

    private final BiFunction<T, UniqueValueSpecification, List<Long>> function;
    private List<Long> duplicatedValues;

    UniqueValueSpecification(String bean, String property, String message,
                             BiFunction<T, UniqueValueSpecification, List<Long>> function) {

        Objects.requireNonNull(function, "A função da especificação não pode ser nula");

        setBean(bean);
        setProperty(property);
        setMessage(message);

        this.function = function;
    }

    @Override
    public boolean isSatisfiedBy(T value) {
        this.duplicatedValues = function.apply(value, this);
        return duplicatedValues == null || duplicatedValues.isEmpty();
    }

    public List<Long> getDuplicatedValues() {
        return duplicatedValues;
    }
}
