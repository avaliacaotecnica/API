package com.marlon.peoplemanagement.common.model.validation;

public abstract class BasicSpecification<T> implements ISpecification<T, BasicSpecification<T>> {

    private String bean;
    private String property;
    private String message;

    @Override
    public BasicSpecification<T> and(BasicSpecification<T> other) {
        return new AndSpecification<>(this, other);
    }

    @Override
    public BasicSpecification<T> not() {
        return new NotSpecification<>(this);
    }

    @Override
    public BasicSpecification<T> or(BasicSpecification<T> other) {
        return new OrSpecification<>(this, other);
    }

    @Override
    public String getBean() {
        return bean;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getProperty() {
        return property;
    }

    @Override
    public BasicSpecification<T> setBean(String bean) {
        this.bean = bean;
        return this;
    }

    @Override
    public BasicSpecification<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public BasicSpecification<T> setProperty(String property) {
        this.property = property;
        return this;
    }

    @Override
    public String toString() {
        String specName;
        if (this.getClass().isAnonymousClass()) {
            specName =
                    getEnclosingClassString() + "." +
                            this.getClass().getEnclosingMethod().getName();
        } else if (this.getClass().getEnclosingClass() != null) {
            specName = getEnclosingClassString() + "." + this.getClass().getSimpleName();
        } else {
            specName = this.getClass().getSimpleName();
        }
        return specName;
    }

    private String getEnclosingClassString() {
        Class clazz = this.getClass();
        StringBuilder sb = new StringBuilder(clazz.getName().length() * 2);
        while ((clazz = clazz.getEnclosingClass()) != null) {
            if (sb.length() > 0) {
                sb.insert(0, ".");
            }
            sb.insert(0, clazz.getSimpleName());
        }
        return sb.toString();
    }
}
