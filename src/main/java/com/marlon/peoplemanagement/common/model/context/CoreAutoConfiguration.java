package com.marlon.peoplemanagement.common.model.context;

import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

@Configuration
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
public class CoreAutoConfiguration {

    @Bean
    public ContextProvider contextProvider(ApplicationContext context) {
        ContextProvider.setContext(context);
        return new ContextProvider();
    }
}
