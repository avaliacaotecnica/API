package com.marlon.peoplemanagement.common.model.context;


import org.springframework.context.ApplicationContext;

import java.util.Objects;

public class ContextProvider {
    private static ApplicationContext context;

    ContextProvider() {
    }

    public static ApplicationContext get() {
        Objects.requireNonNull(context, "Contexto não inicializado");
        return context;
    }

    public static void setContext(ApplicationContext context) {
        ContextProvider.context = context;
    }
}