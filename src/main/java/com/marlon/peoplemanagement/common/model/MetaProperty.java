package com.marlon.peoplemanagement.common.model;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Inherited
@Documented
@Target({FIELD})
@Retention(RUNTIME)
public @interface MetaProperty {

    String name();

    String value();
}




