package com.marlon.peoplemanagement.common.model.validation;

public class OrSpecification<T> extends BasicSpecification<T> {

    private ISpecification<T, BasicSpecification<T>> a;
    private ISpecification<T, BasicSpecification<T>> b;
    private boolean resultA;
    private boolean resultB;

    public OrSpecification(ISpecification<T, BasicSpecification<T>> a,
                           ISpecification<T, BasicSpecification<T>> b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public String getBean() {
        if (super.getBean() != null) {
            return super.getBean();
        } else if (!resultA) {
            return a.getBean();
        } else if (!resultB) {
            return b.getBean();
        }
        return null;
    }

    @Override
    public String getMessage() {
        if (super.getMessage() != null) {
            return super.getMessage();
        } else if (!resultA) {
            return a.getMessage();
        } else if (!resultB) {
            return b.getMessage();
        }
        return null;
    }

    @Override
    public String getProperty() {
        if (super.getProperty() != null) {
            return super.getProperty();
        } else if (!resultA) {
            return a.getProperty();
        } else if (!resultB) {
            return b.getProperty();
        }
        return null;
    }

    @Override
    public boolean isSatisfiedBy(T candidate) {
        this.resultA = a.isSatisfiedBy(candidate);
        this.resultB = b.isSatisfiedBy(candidate);

        return this.resultA || this.resultB;
    }

    @Override
    public String toString() {
        return "(" + a + " || " + b + ")";
    }
}
