package com.marlon.peoplemanagement.common.model.builder;

public interface IBuilder<T> {
    T build();
}
