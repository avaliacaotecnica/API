package com.marlon.peoplemanagement.common.model;

import com.marlon.peoplemanagement.common.model.IEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@MappedSuperclass
@Access(AccessType.FIELD)
@XmlAccessorType(XmlAccessType.NONE)
public abstract class PeopleManagementEntity<I> implements IEntity<I> {

    @Id
    @Column(name = "ID")
    @GeneratedValue
    private I id;

    @Override
    public I getId() {
        return id;
    }

    public void setId(I id) {
        this.id = id;
    }
}
