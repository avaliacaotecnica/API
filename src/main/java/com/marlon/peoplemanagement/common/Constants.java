package com.marlon.peoplemanagement.common;

public class Constants {
    public static final String DEFAULT_LIMIT_SIZE = "20";
    public static final String DEFAULT_OFFSET_SIZE = "0";
}
