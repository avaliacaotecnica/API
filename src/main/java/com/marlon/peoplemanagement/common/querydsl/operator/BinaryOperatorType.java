package com.marlon.peoplemanagement.common.querydsl.operator;

public enum BinaryOperatorType {

    AND("and"),
    OR("or");

    private final String symbol;

    BinaryOperatorType(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
