package com.marlon.peoplemanagement.common.querydsl.operator;

import com.marlon.peoplemanagement.common.querydsl.EvaluationContext;
import com.marlon.peoplemanagement.common.querydsl.EvaluationOperationException;
import com.marlon.peoplemanagement.common.querydsl.QueryDslEvaluationContext;
import com.querydsl.core.types.Predicate;

public class SimpleOperation<T> implements Operation {

    private final SimpleOperatorType operator;
    private final String property;
    private final T value;

    public SimpleOperation(String property, SimpleOperatorType operator, T value) {
        this.property = property;
        this.operator = operator;
        this.value = value;
    }

    public SimpleOperatorType getOperator() {
        return operator;
    }

    @Override
    public boolean isValid() {
        return value != null;
    }

    public T getValue() {
        return value;
    }

    @Override
    public void evaluate(EvaluationContext context) {
        context.visit(this);
    }

    public Predicate evaluate(QueryDslEvaluationContext context) {
        SimpleExpressionOperation current = (SimpleExpressionOperation) context.getOperation(property);

        Object objectValue = value;
        if (current instanceof CustomExpressionOperation) {
            objectValue = CustomExpressionOperation.class.cast(current).getValue(value);
        }

        switch (operator) {
            case EQ:
                return current.eq(objectValue);
            case NE:
                return current.ne(objectValue);
            case LT:
                return current.lt(objectValue);
            case GT:
                return current.gt(objectValue);
            case LOE:
                return current.loe(objectValue);
            case GOE:
                return current.goe(objectValue);
            default:
                throw new EvaluationOperationException("Don't know how to handle <" + operator + ">");
        }
    }

    @Override
    public String toString() {
        return property + " " + operator.getSymbol() + " " + value;
    }
}