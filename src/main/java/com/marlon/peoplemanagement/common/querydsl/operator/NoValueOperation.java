package com.marlon.peoplemanagement.common.querydsl.operator;

import com.marlon.peoplemanagement.common.querydsl.EvaluationContext;
import com.marlon.peoplemanagement.common.querydsl.EvaluationOperationException;
import com.marlon.peoplemanagement.common.querydsl.NoValueOperatorType;
import com.marlon.peoplemanagement.common.querydsl.QueryDslEvaluationContext;
import com.querydsl.core.types.Predicate;

public class NoValueOperation implements Operation {

    private final String property;
    private final NoValueOperatorType operator;

    public NoValueOperation(String property, NoValueOperatorType operator) {
        this.property = property;
        this.operator = operator;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    public NoValueOperatorType getOperator() {
        return operator;
    }

    @Override
    public void evaluate(EvaluationContext context) {
        context.visit(this);
    }

    public Predicate evaluate(QueryDslEvaluationContext context) {
        SimpleExpressionOperation<?> current = (SimpleExpressionOperation) context.getOperation(property);

        switch (operator) {
            case IS_NULL:
                return current.isNull();
            case IS_NOT_NULL:
                return current.isNotNull();
            case IS_TRUE:
                return current.isTrue();
            case IS_FALSE:
                return current.isFalse();
            case IS_EMPTY:
                return current.isEmpty();
            case IS_NOT_EMPTY:
                return current.isNotEmpty();
            default:
                throw new EvaluationOperationException("Don't know how to handle <" +
                        operator + ">");
        }
    }

    @Override
    public String toString() {
        return property + " " + operator.getSymbol();
    }
}

