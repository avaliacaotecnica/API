package com.marlon.peoplemanagement.common.querydsl;


import com.marlon.peoplemanagement.common.querydsl.operator.Operation;

public interface EvaluationContext extends OperationVisitor {
    Operation getOperation(String var1);
}

