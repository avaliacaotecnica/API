package com.marlon.peoplemanagement.common.querydsl.builder;

import com.marlon.peoplemanagement.common.querydsl.EvaluationOperationException;

import java.math.BigDecimal;
import java.math.BigInteger;

public class NumberBuilder {

    private final StringBuilder integer = new StringBuilder();

    private final StringBuilder decimal = new StringBuilder();

    public boolean negative() {
        integer.append('-');

        return true;
    }

    public boolean appendInteger(String value) {
        integer.append(value);

        return true;
    }

    public boolean appendDecimal(String value) {
        decimal.append(value);

        return true;
    }

    public Number toNumber() {
        try {
            if (decimal.length() == 0) {
                return new BigInteger(integer.toString());
            } else {
                integer.append(decimal);

                return new BigDecimal(integer.toString());
            }
        } catch (NumberFormatException nfe) {
            throw new EvaluationOperationException("Failed to convert <" +
                    integer.toString() + "> to Number", nfe);
        }
    }
}