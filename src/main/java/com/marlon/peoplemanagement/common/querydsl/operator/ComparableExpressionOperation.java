package com.marlon.peoplemanagement.common.querydsl.operator;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.ComparableExpression;

public class ComparableExpressionOperation<T extends Comparable<?>> extends
        SimpleExpressionOperation<T> {

    public ComparableExpressionOperation(ComparableExpression<T> property) {
        super(property);
    }

    @Override
    public ComparableExpression<T> getProperty() {
        return (ComparableExpression<T>) property;
    }

    @Override
    public final Predicate lt(T value) {
        return getProperty().lt(value);
    }

    @Override
    public final Predicate gt(T value) {
        return getProperty().gt(value);
    }

    @Override
    public final Predicate loe(T value) {
        return getProperty().loe(value);
    }

    @Override
    public final Predicate goe(T value) {
        return getProperty().goe(value);
    }

    @Override
    public final OrderSpecifier<T> orderAsc() {
        return getProperty().asc();
    }

    @Override
    public final OrderSpecifier<T> orderDesc() {
        return getProperty().desc();
    }
}