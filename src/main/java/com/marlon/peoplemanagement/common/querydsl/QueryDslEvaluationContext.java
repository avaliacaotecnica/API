package com.marlon.peoplemanagement.common.querydsl;

import com.marlon.peoplemanagement.common.querydsl.operator.*;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserConfig;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.SimpleExpression;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;
import java.util.Optional;

public class QueryDslEvaluationContext implements EvaluationContext {

    private final Deque<Predicate> predicates = new ArrayDeque<>();
    private final Deque<OrderSpecifier<?>> orders = new ArrayDeque<>();
    private final Map<String, SimpleExpression<?>> expressions;
    private final ExpressionOperationResolver expressionOperationResolver;
    private final FilterExpressionParserConfig filterExpressionParserConfig;


    public QueryDslEvaluationContext(final Map<String, SimpleExpression<?>> expressions,
                                     final ExpressionOperationResolver expressionOperationResolver,
                                     final FilterExpressionParserConfig filterExpressionParserConfig) {
        this.expressions = expressions;
        this.expressionOperationResolver = expressionOperationResolver;
        this.filterExpressionParserConfig = filterExpressionParserConfig;
    }

    @Override
    public void visit(NoValueOperation operation) {
        append(operation.evaluate(this));
    }

    @Override
    public void visit(SimpleOperation<?> operation) {
        append(operation.evaluate(this));
    }

    @Override
    public void visit(StringOperation operation) {
        append(operation.evaluate(this));
    }

    @Override
    public void visit(OrderOperation operation) {
        append(operation.evaluate(this));
    }

    @Override
    public void visit(SimpleExpressionOperation<?> operation) {
        operation.evaluate(this);
    }

    @Override
    public void visit(MultipleOperation<?> operation) {
        append(operation.evaluate(this));
    }

    @Override
    public void visit(BinaryOperation operation) {
        operation.getOperation1().evaluate(this);
        operation.getOperation2().evaluate(this);

        switch (operation.getOperator()) {
            case AND:
                Predicate right = popLastPredicate();
                Predicate left = popLastPredicate();

                append(ExpressionUtils.and(left, right));

                break;
            case OR:
                right = popLastPredicate();
                left = popLastPredicate();

                append(ExpressionUtils.or(left, right));

                break;
            default:
                throw new EvaluationOperationException("Don't know how to handle <" + operation
                        .getOperator() + ">");
        }
    }


    private void append(Predicate predicate) {
        predicates.add(predicate);
    }

    private Predicate popLastPredicate() {
        return predicates.removeLast();
    }

    private void append(OrderSpecifier<?> order) {
        orders.add(order);
    }

    public Iterable<Predicate> getPredicates() {
        return predicates::iterator;
    }

    public Iterable<OrderSpecifier<?>> getOrders() {
        return orders::descendingIterator;
    }

    public Map<String, SimpleExpression<?>> getExpressions() {
        return expressions;
    }

    private SimpleExpression<?> parseProperty(String property) {
        SimpleExpression<?> expression = expressions.get(property);
        if (expression == null) {
            throw new EvaluationOperationException("Invalid property <" + property + ">");
        }

        return expression;
    }

    @Override
    public Operation getOperation(String property) {
        final SimpleExpression<?> expression = parseProperty(property);
        final Optional<? extends Operation> operation = expressionOperationResolver
                .apply(expression);
        if (operation.isPresent()) {
            return operation.get();
        }

        throw new EvaluationOperationException("Don't know how to handle type <" + expression
                .getClass().getSimpleName() + "> of property <" + property + ">");
    }

    public FilterExpressionParserConfig getFilterExpressionParserConfig() {
        return filterExpressionParserConfig;
    }
}

