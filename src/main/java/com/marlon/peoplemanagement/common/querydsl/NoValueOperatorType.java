package com.marlon.peoplemanagement.common.querydsl;

public enum NoValueOperatorType {
    IS_NULL("is null"),
    IS_NOT_NULL("is not null"),
    IS_TRUE("is true"),
    IS_FALSE("is false"),
    IS_EMPTY("is empty"),
    IS_NOT_EMPTY("is not empty");

    private final String symbol;

    private NoValueOperatorType(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return this.symbol;
    }
}
