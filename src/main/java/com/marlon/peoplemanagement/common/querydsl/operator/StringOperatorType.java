package com.marlon.peoplemanagement.common.querydsl.operator;

public enum StringOperatorType {

    LIKE("like"),
    NOT_LIKE("not like"),
    LIKE_ESCAPE("like"),
    NOT_LIKE_ESCAPE("not like");

    private final String symbol;

    StringOperatorType(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}