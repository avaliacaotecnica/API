package com.marlon.peoplemanagement.common.querydsl.operator;

import com.marlon.peoplemanagement.common.querydsl.EvaluationOperationException;
import com.marlon.peoplemanagement.common.querydsl.EvaluationContext;
import com.marlon.peoplemanagement.common.querydsl.QueryDslEvaluationContext;
import com.querydsl.core.types.OrderSpecifier;

public class OrderOperation implements Operation {

    private final String property;
    private final OrderOperatorType operator;

    public OrderOperation(String property, OrderOperatorType operator) {
        this.property = property;
        this.operator = operator;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    public OrderOperatorType getOperator() {
        return operator;
    }

    @Override
    public void evaluate(EvaluationContext context) {
        context.visit(this);
    }

    public OrderSpecifier<?> evaluate(QueryDslEvaluationContext context) {
        SimpleExpressionOperation<?> current = (SimpleExpressionOperation) context.getOperation(
                property);

        switch (operator) {
            case ASC:
                return current.orderAsc();
            case DESC:
                return current.orderDesc();
            default:
                throw new EvaluationOperationException("Don't know how to handle <" +
                        operator + ">");
        }
    }

    @Override
    public String toString() {
        return property + " " + operator.getSymbol();
    }
}
