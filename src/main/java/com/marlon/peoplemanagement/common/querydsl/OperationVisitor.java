package com.marlon.peoplemanagement.common.querydsl;

import com.marlon.peoplemanagement.common.querydsl.operator.*;

public interface OperationVisitor {

    void visit(SimpleOperation<?> var1);

    void visit(StringOperation operation);

    void visit(NoValueOperation operation);

    void visit(SimpleExpressionOperation<?> operation);

    void visit(OrderOperation operation);

    void visit(MultipleOperation<?> operation);

    void visit(BinaryOperation operation);
}
