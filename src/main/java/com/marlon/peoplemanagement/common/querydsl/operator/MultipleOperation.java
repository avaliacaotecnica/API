package com.marlon.peoplemanagement.common.querydsl.operator;

import com.marlon.peoplemanagement.common.querydsl.EvaluationOperationException;
import com.marlon.peoplemanagement.common.querydsl.EvaluationContext;
import com.marlon.peoplemanagement.common.querydsl.QueryDslEvaluationContext;
import com.querydsl.core.types.Predicate;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public final class MultipleOperation<T> implements Operation {


    private final MultipleOperatorType operator;
    private final String property;
    private final T[] values;

    private MultipleOperation(String property, MultipleOperatorType operator, T[] values) {
        this.property = property;
        this.operator = operator;
        this.values = Arrays.copyOf(values, values.length);
    }

    public static <T> MultipleOperation<T> build(String property, MultipleOperatorType operator, T... values) {
        return new MultipleOperation<>(property, operator, values);
    }

    @Override
    public boolean isValid() {
        return values != null && values.length > 0;
    }

    @Override
    public void evaluate(EvaluationContext context) {
        context.visit(this);
    }

    public Predicate evaluate(QueryDslEvaluationContext context) {
        SimpleExpressionOperation current = (SimpleExpressionOperation) context.getOperation(property);

        Object[] objectValues = null;
        if (current instanceof CustomExpressionOperation) {
            for (int i = 0; i < values.length; i++) {
                Object value = CustomExpressionOperation.class.cast(current).getValue(values[i]);

                if (isNull(objectValues)) {
                    objectValues = (Object[]) Array.newInstance(value.getClass(), values.length);
                }

                objectValues[i] = value;
            }
        } else {
            objectValues = values;
        }

        switch (operator) {
            case IN:
                return current.in(objectValues);
            case NOT_IN:
                return current.notIn(objectValues);
            default:
                throw new EvaluationOperationException("Don't know how to handle <" +
                        operator + ">");
        }
    }

    @Override
    public String toString() {
        return property + " " + operator.getSymbol() + " " + Arrays.stream(values).map(Object::toString)
                .collect(Collectors.joining(","));
    }
}