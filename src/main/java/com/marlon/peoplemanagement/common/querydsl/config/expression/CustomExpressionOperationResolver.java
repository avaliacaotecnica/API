package com.marlon.peoplemanagement.common.querydsl.config.expression;

import com.marlon.peoplemanagement.common.querydsl.ExpressionOperationResolver;
import com.marlon.peoplemanagement.common.querydsl.ExpressionOperationResolverImpl;
import com.marlon.peoplemanagement.common.querydsl.operator.Operation;
import com.marlon.peoplemanagement.common.querydsl.operator.StringExpressionOperation;
import com.querydsl.core.types.dsl.ComparableExpression;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.querydsl.core.types.dsl.StringExpression;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.function.Function;

public class CustomExpressionOperationResolver implements ExpressionOperationResolver {

    private static final ExpressionOperationResolver DEFAULT_EXPRESSION_OPERATION_RESOLVER =
            new ExpressionOperationResolverImpl();

    @Override
    public Optional<? extends Operation> apply(final SimpleExpression<?> expression) {
        if (LocalDateTime.class.isAssignableFrom(expression.getType())) {
            return Optional.of(new GenericExpressionOperation((ComparableExpression) expression,
                    (Function<String, LocalDateTime>) LocalDateTime::parse));
        }
        if (String.class.isAssignableFrom(expression.getType())) {
            return Optional.of(new StringExpressionOperation((StringExpression) expression));
        }
        return DEFAULT_EXPRESSION_OPERATION_RESOLVER.apply(expression);
    }
}
