package com.marlon.peoplemanagement.common.querydsl.operator;

public interface CustomExpressionOperation<T, V> {

    T getValue(V value);
}
