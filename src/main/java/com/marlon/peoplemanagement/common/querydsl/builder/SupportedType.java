package com.marlon.peoplemanagement.common.querydsl.builder;

import com.marlon.peoplemanagement.common.querydsl.EvaluationOperationException;
import com.marlon.peoplemanagement.common.querydsl.operator.MultipleOperation;
import com.marlon.peoplemanagement.common.querydsl.operator.MultipleOperatorType;
import com.marlon.peoplemanagement.common.querydsl.operator.SimpleOperation;
import com.marlon.peoplemanagement.common.querydsl.operator.SimpleOperatorType;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.List;

public enum SupportedType {

    DATE(LocalDate.class),
    NUMBER(Number.class),
    BOOLEAN(Boolean.class),
    STRING(String.class);

    public static SimpleOperation<?> build(String property, SimpleOperatorType operator,
                                           Object value) {
        Class<?> valueType = value.getClass();

        for (SupportedType supportedType : SupportedType.values()) {
            final Class<?> type = supportedType.getType();

            if (type.isAssignableFrom(valueType)) {
                return new SimpleOperation(property, operator, type.cast(value));
            }
        }

        throw new EvaluationOperationException("Don't know how to handle type <" +
                valueType.getName() + "> for <" + value + ">");
    }

    private final Class<?> type;

    SupportedType(Class<?> type) {
        this.type = type;
    }

    public Class<?> getType() {
        return type;
    }

    public MultipleOperation<?> build(String property,
                                      MultipleOperatorType operator,
                                      List<Object> list) {
        Object array = Array.newInstance(type, list.size());
        int index = 0;

        for (Object o : list) {
            Array.set(array, index, o);

            ++index;
        }

        return MultipleOperation.build(property, operator, (Object[]) array);
    }
}
