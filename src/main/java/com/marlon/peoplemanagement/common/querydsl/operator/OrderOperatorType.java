package com.marlon.peoplemanagement.common.querydsl.operator;

public enum OrderOperatorType {

    ASC("asc"),
    DESC("desc");

    private final String symbol;

    OrderOperatorType(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
