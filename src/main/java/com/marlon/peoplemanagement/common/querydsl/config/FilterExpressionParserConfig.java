package com.marlon.peoplemanagement.common.querydsl.config;

import com.marlon.peoplemanagement.common.querydsl.operator.SimpleOperatorType;
import com.marlon.peoplemanagement.common.querydsl.operator.StringOperatorType;

public interface FilterExpressionParserConfig {
    String parseStringOperation(String value, StringOperatorType operator, String path);

    Object parseSimpleOperation(Object value, SimpleOperatorType operator, String path);
}