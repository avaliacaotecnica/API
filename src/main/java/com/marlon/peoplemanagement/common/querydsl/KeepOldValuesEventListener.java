package com.marlon.peoplemanagement.common.querydsl;


import com.marlon.peoplemanagement.common.representation.BeforeDeserializeEvent;
import com.marlon.peoplemanagement.common.representation.DeserializerContext;
import com.marlon.peoplemanagement.common.representation.Representation;
import com.marlon.peoplemanagement.common.representation.Serializer;
import org.springframework.context.event.EventListener;

import java.io.IOException;

public class KeepOldValuesEventListener {

    private final Serializer serializer;

    public KeepOldValuesEventListener(Serializer serializer) {
        this.serializer = serializer;
    }

    @EventListener
    public void onBeforeDeserialize(BeforeDeserializeEvent event) throws IOException {

        DeserializerContext context = event.getContext();
        Representation representation = context.getRepresentation();
        Object destination = context.getDestination();

        if (representation.keepOldValues() && !context.isCopyOnly() &&
                destination != null && representation.getOldValuesMethod() != null) {

            String oldValueJson = serializer.toJson(context.getRepresentation(),
                    destination);

            Object oldValue = context.getDeserializer().copyFromJson(representation,
                    oldValueJson, context.getLocator());

            representation.getOldValuesMethod().accept(context.getInput(), oldValue);
        }
    }

}
