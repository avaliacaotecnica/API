package com.marlon.peoplemanagement.common.querydsl.operator;

import com.marlon.peoplemanagement.common.querydsl.EvaluationContext;

public interface Operation {

    boolean isValid();

    void evaluate(EvaluationContext context);
}
