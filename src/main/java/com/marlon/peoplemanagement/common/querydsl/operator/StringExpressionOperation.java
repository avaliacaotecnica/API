package com.marlon.peoplemanagement.common.querydsl.operator;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.StringExpression;
import org.apache.commons.lang3.StringUtils;

public class StringExpressionOperation extends
        ComparableExpressionOperation<String> {

    public StringExpressionOperation(StringExpression property) {
        super(property);
    }

    @Override
    public StringExpression getProperty() {
        return (StringExpression) property;
    }

    @Override
    public Predicate like(String value, char escape) {
        return Expressions.stringTemplate("{0}", getProperty()).likeIgnoreCase("%" + StringUtils.stripAccents(value) + "%");
    }

    @Override
    public Predicate notLike(String value, char escape) {
        return like(value).not();
    }

    @Override
    public Predicate like(String value) {
        return getProperty().like(value);
    }

    @Override
    public Predicate notLike(String value) {
        return getProperty().notLike(value);
    }

    @Override
    public Predicate isEmpty() {
        return getProperty().isEmpty();
    }

    @Override
    public Predicate isNotEmpty() {
        return getProperty().isNotEmpty();
    }
}
