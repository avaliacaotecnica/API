package com.marlon.peoplemanagement.common.querydsl.operator;

import com.marlon.peoplemanagement.common.querydsl.EvaluationContext;
import com.marlon.peoplemanagement.common.querydsl.EvaluationOperationException;
import com.marlon.peoplemanagement.common.querydsl.QueryDslEvaluationContext;
import com.querydsl.core.types.Predicate;

public class StringOperation implements Operation {

    private final String property;
    private final StringOperatorType operator;
    private final String value;
    private final char escapeChar;

    public StringOperation(String property, StringOperatorType operator, String value) {
        this.property = property;
        this.operator = operator;
        this.value = value;
        this.escapeChar = '!';
    }

    public StringOperation(String property, StringOperatorType operator, String value, char escape) {
        this.property = property;
        this.operator = operator;
        this.value = value;
        this.escapeChar = escape;
    }

    @Override
    public boolean isValid() {
        return value != null;
    }

    @Override
    public void evaluate(EvaluationContext context) {
        context.visit(this);
    }

    public Predicate evaluate(QueryDslEvaluationContext context) {
        SimpleExpressionOperation current = (SimpleExpressionOperation) context.getOperation(property);

        switch (operator) {
            case LIKE:
                return current.like(value);
            case NOT_LIKE:
                return current.notLike(value);
            case LIKE_ESCAPE:
                return current.like(value, escapeChar);
            case NOT_LIKE_ESCAPE:
                return current.notLike(value, escapeChar);
            default:
                throw new EvaluationOperationException("Don't know how to handle <" +
                        operator + ">");
        }
    }

    @Override
    public String toString() {
        return property + " " + operator.getSymbol() + " " + value;
    }
}