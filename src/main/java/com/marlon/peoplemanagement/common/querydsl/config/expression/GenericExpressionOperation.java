package com.marlon.peoplemanagement.common.querydsl.config.expression;

import com.marlon.peoplemanagement.common.querydsl.operator.ComparableExpressionOperation;
import com.marlon.peoplemanagement.common.querydsl.operator.CustomExpressionOperation;
import com.querydsl.core.types.dsl.ComparableExpression;

import java.util.function.Function;

public class GenericExpressionOperation<T extends Comparable<?>>
        extends ComparableExpressionOperation<T>
        implements CustomExpressionOperation<T, String> {

    private final Function<String, T> parse;

    public GenericExpressionOperation(ComparableExpression<T> property, Function<String, T> parse) {
        super(property);
        this.parse = parse;
    }

    @Override
    public T getValue(final String value) {
        return (T) parse.apply(value);
    }
}
