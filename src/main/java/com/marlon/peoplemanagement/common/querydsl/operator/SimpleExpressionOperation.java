package com.marlon.peoplemanagement.common.querydsl.operator;


import com.marlon.peoplemanagement.common.querydsl.EvaluationContext;
import com.marlon.peoplemanagement.common.querydsl.EvaluationOperationException;
import com.marlon.peoplemanagement.common.querydsl.NoValueOperatorType;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.SimpleExpression;

public class SimpleExpressionOperation<T> implements Operation {
    protected final SimpleExpression<T> property;

    public SimpleExpressionOperation(SimpleExpression<T> property) {
        this.property = property;
    }

    public boolean isValid() {
        return true;
    }

    public SimpleExpression<T> getProperty() {
        return this.property;
    }

    public final void evaluate(EvaluationContext context) {
        context.visit(this);
    }

    public final Predicate isNull() {
        return this.property.isNull();
    }

    public final Predicate isNotNull() {
        return this.property.isNotNull();
    }

    public final Predicate eq(T value) {
        return this.property.eq(value);
    }

    public final Predicate ne(T value) {
        return this.property.ne(value);
    }

    public final Predicate in(T[] values) {
        return this.property.in(values);
    }

    public final Predicate notIn(T[] values) {
        return this.property.notIn(values);
    }

    public Predicate isTrue() {
        throw new EvaluationOperationException("Don't know how to handle <" + NoValueOperatorType.IS_TRUE.getSymbol() + "> within <" + this.property + ">");
    }

    public Predicate isFalse() {
        throw new EvaluationOperationException("Don't know how to handle <" + NoValueOperatorType.IS_FALSE.getSymbol() + "> within <" + this.property + ">");
    }

    public Predicate like(T value) {
        throw new EvaluationOperationException("Don't know how to handle <" + StringOperatorType.LIKE.getSymbol() + "> within <" + this.property + ">");
    }

    public Predicate like(T value, char escape) {
        throw new EvaluationOperationException("Don't know how to handle <e" + StringOperatorType.LIKE_ESCAPE.getSymbol() + "> within <" + this.property + ">");
    }

    public Predicate notLike(T value) {
        throw new EvaluationOperationException("Don't know how to handle <" + StringOperatorType.NOT_LIKE.getSymbol() + "> within <" + this.property + ">");
    }

    public Predicate notLike(T value, char escape) {
        throw new EvaluationOperationException("Don't know how to handle <e" + StringOperatorType.NOT_LIKE_ESCAPE.getSymbol() + "> within <" + this.property + ">");
    }

    public Predicate isEmpty() {
        throw new EvaluationOperationException("Don't know how to handle <" + NoValueOperatorType.IS_EMPTY.getSymbol() + "> within <" + this.property + ">");
    }

    public Predicate isNotEmpty() {
        throw new EvaluationOperationException("Don't know how to handle <" + NoValueOperatorType.IS_NOT_EMPTY.getSymbol() + "> within <" + this.property + ">");
    }

    public Predicate lt(T value) {
        throw new EvaluationOperationException("Don't know how to handle (" + SimpleOperatorType.LT.getSymbol() + ") within <" + this.property + ">");
    }

    public Predicate loe(T value) {
        throw new EvaluationOperationException("Don't know how to handle (" + SimpleOperatorType.LOE.getSymbol() + ") within <" + this.property + ">");
    }

    public Predicate gt(T value) {
        throw new EvaluationOperationException("Don't know how to handle (" + SimpleOperatorType.GT.getSymbol() + ") within <" + this.property + ">");
    }

    public Predicate goe(T value) {
        throw new EvaluationOperationException("Don't know how to handle (" + SimpleOperatorType.GOE.getSymbol() + ") within <" + this.property + ">");
    }

    public OrderSpecifier<?> orderAsc() {
        throw new EvaluationOperationException("Don't know how to handle <" + OrderOperatorType.ASC.getSymbol() + "> within <" + this.property + ">");
    }

    public OrderSpecifier<?> orderDesc() {
        throw new EvaluationOperationException("Don't know how to handle <" + OrderOperatorType.DESC.getSymbol() + "> within <" + this.property + ">");
    }

    public final String toString() {
        return this.property.toString();
    }
}

