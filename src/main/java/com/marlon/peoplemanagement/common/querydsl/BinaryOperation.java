package com.marlon.peoplemanagement.common.querydsl;

import com.marlon.peoplemanagement.common.querydsl.operator.BinaryOperatorType;
import com.marlon.peoplemanagement.common.querydsl.operator.Operation;

public class BinaryOperation implements Operation {

    private final BinaryOperatorType operator;

    private final Operation operation1;

    private final Operation operation2;

    public BinaryOperation(BinaryOperatorType operator, Operation operation1, Operation operation2) {
        this.operator = operator;
        this.operation1 = operation1;
        this.operation2 = operation2;
    }

    @Override
    public boolean isValid() {
        final boolean result = operation1.isValid();

        if (result != operation2.isValid()) {
            throw new EvaluationOperationException("Both operation must be in the same state");
        }

        return result;
    }

    public BinaryOperatorType getOperator() {
        return operator;
    }

    public Operation getOperation1() {
        return operation1;
    }

    public Operation getOperation2() {
        return operation2;
    }

    @Override
    public void evaluate(EvaluationContext context) {
        context.visit(this);
    }

    @Override
    public String toString() {
        return "(" + operation1 + " " + operator.getSymbol() + " " + operation2 + ")";
    }
}
