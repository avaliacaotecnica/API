package com.marlon.peoplemanagement.common.querydsl;

import com.marlon.peoplemanagement.common.querydsl.operator.Operation;
import com.querydsl.core.types.dsl.SimpleExpression;

import java.util.Optional;
import java.util.function.Function;

public interface ExpressionOperationResolver extends Function<SimpleExpression<?>, Optional<? extends Operation>> {
}