package com.marlon.peoplemanagement.common.querydsl;


import com.marlon.peoplemanagement.common.querydsl.operator.ComparableExpressionOperation;
import com.marlon.peoplemanagement.common.querydsl.operator.Operation;
import com.marlon.peoplemanagement.common.querydsl.operator.SimpleExpressionOperation;
import com.marlon.peoplemanagement.common.querydsl.operator.StringExpressionOperation;
import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.dsl.*;

import java.util.Optional;

public class ExpressionOperationResolverImpl implements ExpressionOperationResolver {

    @Override
    public Optional<? extends Operation> apply(final SimpleExpression<?> expression) {
        return Optional.ofNullable(resolve(expression));
    }

    protected Operation resolve(final SimpleExpression<?> expression) {
        if (expression instanceof StringExpression) {
            return new StringExpressionOperation((StringExpression) expression);
        } else if (expression instanceof ComparableExpression) {
            return new ComparableExpressionOperation<>((ComparableExpression) expression);
        } else if (expression instanceof EntityPath) {
            return new SimpleExpressionOperation(expression);
        } else if (expression instanceof SimpleExpression) {
            return new SimpleExpressionOperation<>(expression);
        }

        return null;
    }
}