package com.marlon.peoplemanagement.common.querydsl;

public class EvaluationOperationException extends UnsupportedOperationException {
    public EvaluationOperationException(String message) {
        super(message);
    }

    public EvaluationOperationException(String message, Throwable cause) {
        super(message, cause);
    }
}