package com.marlon.peoplemanagement.common.querydsl.builder;


import com.marlon.peoplemanagement.common.querydsl.EvaluationOperationException;

import java.time.LocalDate;

public class DateBuilder {

    private final StringBuilder sb = new StringBuilder(10);

    public DateBuilder(String year) {
        sb.append(year);
    }

    public boolean setMonth(String month) {
        sb.append('-');
        sb.append(month);

        return true;
    }

    public boolean setDay(String day) {
        sb.append('-');
        sb.append(day);

        return true;
    }

    public LocalDate toDate() {
        try {
            return LocalDate.parse(sb.toString());
        } catch (Exception e) {
            throw new EvaluationOperationException("Failed to parse Date <" +
                    sb.toString() + "> with pattern <yyyy-MM-dd>", e);
        }
    }
}