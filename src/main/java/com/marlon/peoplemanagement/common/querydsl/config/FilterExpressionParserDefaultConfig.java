package com.marlon.peoplemanagement.common.querydsl.config;

import com.marlon.peoplemanagement.common.querydsl.operator.SimpleOperatorType;
import com.marlon.peoplemanagement.common.querydsl.operator.StringOperatorType;

import java.net.URLDecoder;

public class FilterExpressionParserDefaultConfig implements FilterExpressionParserConfig {

    protected static final String DEFAULT_CHARSET = "UTF-8";

    private final String charset;
    private final boolean filterEncoded;
    private final boolean ignoreParseError;


    protected FilterExpressionParserDefaultConfig(boolean filterEncoded, String charset, boolean ignoreParseError) {
        this.filterEncoded = filterEncoded;
        this.charset = charset == null ? DEFAULT_CHARSET : charset;
        this.ignoreParseError = ignoreParseError;
    }

    protected FilterExpressionParserDefaultConfig(boolean filterEncoded, String charset) {
        this(filterEncoded, charset, false);
    }

    protected FilterExpressionParserDefaultConfig(boolean filterEncoded) {
        this(filterEncoded, DEFAULT_CHARSET, false);
    }

    protected FilterExpressionParserDefaultConfig() {
        this(false, DEFAULT_CHARSET, false);
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String parseStringOperation(String value, StringOperatorType operator, String path) {
        if (!this.filterEncoded) {
            return value;
        }
        try {
            return URLDecoder.decode(value, charset);
        } catch (Exception e) {
            if (ignoreParseError) {
                return value;
            }
            throw new RuntimeException("Erro ao decodificar valor '" + value + "'", e);
        }
    }

    @Override
    public Object parseSimpleOperation(Object value, SimpleOperatorType operator, String path) {
        return value;
    }

    public boolean isFilterEncoded() {
        return filterEncoded;
    }

    public boolean isIgnoreParseError() {
        return ignoreParseError;
    }

    public String getCharset() {
        return charset;
    }

    public static class Builder {

        private boolean encodedFilter = false;
        private String charset = DEFAULT_CHARSET;
        private boolean ignoreParseError = false;

        public FilterExpressionParserConfig build() {
            return new FilterExpressionParserDefaultConfig(encodedFilter, charset, ignoreParseError);
        }

        public Builder filterEncoded() {
            this.encodedFilter = true;
            return this;
        }

        public Builder filterEncoded(boolean encoded) {
            this.encodedFilter = encoded;
            return this;
        }

        public Builder charset(String charset) {
            this.charset = charset;
            return this;
        }

        public Builder ignoreParseError(boolean ignore) {
            this.ignoreParseError = ignore;
            return this;
        }

        public Builder ignoreParseError() {
            this.ignoreParseError = true;
            return this;
        }
    }
}
