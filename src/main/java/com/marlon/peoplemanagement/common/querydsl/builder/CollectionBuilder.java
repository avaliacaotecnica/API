package com.marlon.peoplemanagement.common.querydsl.builder;

import com.marlon.peoplemanagement.common.querydsl.EvaluationOperationException;
import com.marlon.peoplemanagement.common.querydsl.operator.MultipleOperation;
import com.marlon.peoplemanagement.common.querydsl.operator.MultipleOperatorType;

import java.util.ArrayList;
import java.util.List;

public class CollectionBuilder {

    private final List<Object> list = new ArrayList<Object>();

    private SupportedType supportedType;

    public boolean start(Object value) {
        Class<?> valueType = value.getClass();

        for (SupportedType s : SupportedType.values()) {
            if (s.getType().isAssignableFrom(valueType)) {
                this.supportedType = s;
                this.list.add(value);

                return true;
            }
        }

        throw new EvaluationOperationException("Don't know how to handle type <" +
                valueType.getName() + "> for <" + value + ">");
    }

    public boolean add(Object value) {
        Class<?> valueType = value.getClass();

        if (!supportedType.getType().isAssignableFrom(valueType)) {
            throw new EvaluationOperationException("Can't mix <" +
                    valueType.getName() + "> with <" +
                    supportedType.getType() + "> for <" + value + ">");
        }

        this.list.add(value);

        return true;
    }

    public MultipleOperation<?> toOperation(String property, MultipleOperatorType operator) {
        return supportedType.build(property, operator, list);
    }
}