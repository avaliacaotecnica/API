package com.marlon.peoplemanagement.common.querydsl;

import com.marlon.peoplemanagement.common.mvc.FilterExpressionParser;
import com.marlon.peoplemanagement.common.mvc.FilterExpressionParserConfigurable;
import com.marlon.peoplemanagement.common.mvc.SortExpressionParser;
import com.marlon.peoplemanagement.common.querydsl.operator.Operation;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserConfig;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.SimpleExpression;
import org.parboiled.Parboiled;
import org.parboiled.support.ValueStack;
import org.parboiled.Rule;
import org.parboiled.parserunners.ReportingParseRunner;
import org.parboiled.support.ParsingResult;

import java.util.HashMap;
import java.util.Map;

public final class QueryDslExpressionEvaluator {

    private final QueryDslEvaluationContext context;

    private QueryDslExpressionEvaluator(final Map<String, SimpleExpression<?>> expressions,
                                        final ExpressionOperationResolver expressionOperationResolver,
                                        final FilterExpressionParserConfig filterExpressionParserConfig) {
        this.context = new QueryDslEvaluationContext(expressions, expressionOperationResolver, filterExpressionParserConfig);
    }

    public static QueryDslExpressionBuilder builder() {
        return new QueryDslExpressionBuilder(new HashMap<>());
    }

    public static QueryDslExpressionBuilder builder(
            final Map<String, SimpleExpression<?>> expressions) {
        return builder().putAll(expressions);
    }

    public Iterable<Predicate> getPredicates() {
        return context.getPredicates();
    }

    public Iterable<OrderSpecifier<?>> getOrders() {
        return context.getOrders();
    }

    public QueryDslExpressionEvaluator filter(String filter) {
        String safe = safeString(filter);
        if (safe.length() > 0) {
            FilterExpressionParser parser = Parboiled.createParser(FilterExpressionParser.class);
            if(parser instanceof FilterExpressionParserConfigurable && this.context.getFilterExpressionParserConfig()!=null) {
                parser.configure(this.context.getFilterExpressionParserConfig());
            }
            evaluateOperation(parse(safe, parser.Expression()));
        }

        return this;
    }

    public QueryDslExpressionEvaluator sort(String sort) {
        String safe = safeString(sort);
        if (safe.length() > 0) {
            evaluateSort(parse(safe, Parboiled.createParser(SortExpressionParser.class).Expression()));
        }

        return this;
    }

    private String safeString(String s) {
        return s == null ? "" : s.trim();
    }

    private ValueStack<Operation> parse(String expression, Rule ruleTree) {
        ReportingParseRunner<Operation> runner = new ReportingParseRunner<>(ruleTree);
        ParsingResult<Operation> result = runner.run(expression);

        if (result.hasErrors()) {
            throw new IllegalArgumentException("Parser error: " + result);
        } else if (!result.matched) {
            throw new IllegalArgumentException("Parser error: No Match");
        }

        return result.valueStack;
    }

    private void evaluateOperation(ValueStack<Operation> operationStack) {
        if (operationStack.size() != 1) {
            throw new IllegalArgumentException("Parser error: Invalid Result");
        }

        Operation operation = operationStack.pop();
        if (operation.isValid()) {
            try {
                operation.evaluate(context);
            } catch (EvaluationOperationException e) {
                throw new IllegalArgumentException(e.getMessage(), e);
            }
        }
    }

    private void evaluateSort(ValueStack<Operation> operationStack) {
        if (operationStack.size() < 1) {
            throw new IllegalArgumentException("Parser error: Invalid Result");
        }

        while (!operationStack.isEmpty()) {
            Operation operation = operationStack.pop();
            if (operation.isValid()) {
                try {
                    operation.evaluate(context);
                } catch (EvaluationOperationException e) {
                    throw new IllegalArgumentException(e.getMessage(), e);
                }
            }
        }
    }

    public static final class QueryDslExpressionBuilder {

        private final Map<String, SimpleExpression<?>> expressions;
        private ExpressionOperationResolver expressionOperationResolver;
        private FilterExpressionParserConfig filterExpressionParserConfig;

        private QueryDslExpressionBuilder(final Map<String, SimpleExpression<?>> expressions) {
            this.expressions = expressions;
        }

        public QueryDslExpressionBuilder put(String property, SimpleExpression<?> expression) {
            this.expressions.put(property, expression);
            return this;
        }

        public QueryDslExpressionBuilder putAll(final Map<String, SimpleExpression<?>> expressions) {
            this.expressions.putAll(expressions);
            return this;
        }

        public QueryDslExpressionBuilder expressionOperationResolver(
                final ExpressionOperationResolver expressionOperationResolver) {
            this.expressionOperationResolver = expressionOperationResolver;
            return this;
        }

        public QueryDslExpressionBuilder filterExpressionParserConfig(FilterExpressionParserConfig filterExpressionParserConfig) {
            this.filterExpressionParserConfig = filterExpressionParserConfig;
            return this;
        }

        public QueryDslExpressionEvaluator build() {
            ExpressionOperationResolver expResolver = expressionOperationResolver == null ?
                    new ExpressionOperationResolverImpl() :
                    expressionOperationResolver;
            return new QueryDslExpressionEvaluator(expressions, expResolver, filterExpressionParserConfig);
        }
    }
}
