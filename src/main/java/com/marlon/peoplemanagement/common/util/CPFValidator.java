package com.marlon.peoplemanagement.common.util;

import java.util.Arrays;
import java.util.InputMismatchException;

public class CPFValidator {
    public static boolean validaCPF(String CPF) {
        if (Arrays.asList(
                "00000000000", "11111111111", "22222222222",
                "33333333333", "44444444444", "55555555555",
                "66666666666", "77777777777", "88888888888",
                "99999999999").contains(CPF) ||
                CPF.length() != 11 ||
                CPF.length() == 14) {
            return (false);
        } else {
            char dig10, dig11;
            int sm, i, r, num, peso;
            try {
                sm = 0;
                peso = 10;
                for (i = 0; i < 9; i++) {
                    num = (int) (CPF.charAt(i) - 48);
                    sm = sm + (num * peso);
                    peso = peso - 1;
                }
                r = 11 - (sm % 11);
                if ((r == 10) || (r == 11)) {
                    dig10 = '0';
                } else {
                    dig10 = (char) (r + 48);
                }
                sm = 0;
                peso = 11;
                for (i = 0; i < 10; i++) {
                    num = (int) (CPF.charAt(i) - 48);
                    sm = sm + (num * peso);
                    peso = peso - 1;
                }
                r = 11 - (sm % 11);
                if ((r == 10) || (r == 11)) {
                    dig11 = '0';
                } else {
                    dig11 = (char) (r + 48);
                }

                if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10))) {
                    return true;
                } else {
                    return (false);
                }
            } catch (InputMismatchException erro) {
                return (false);
            }

        }
    }

}
