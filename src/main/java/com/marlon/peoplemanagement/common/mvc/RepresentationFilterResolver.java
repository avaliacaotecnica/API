package com.marlon.peoplemanagement.common.mvc;

import com.marlon.peoplemanagement.common.representation.NoneRepresentationProvider;
import com.marlon.peoplemanagement.common.representation.RepresentationMapper;
import com.marlon.peoplemanagement.common.representation.RepresentationProvider;
import org.springframework.core.MethodParameter;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class RepresentationFilterResolver implements HandlerMethodArgumentResolver {

    private final RepresentationMapper mapper;

    public RepresentationFilterResolver(RepresentationMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(RepresentationFilter.class) != null;
    }

    @Nullable
    @Override
    public Object resolveArgument(MethodParameter methodParameter, @Nullable ModelAndViewContainer mavContainer,
                                          NativeWebRequest webRequest, @Nullable WebDataBinderFactory binderFactory) {

        RepresentationFilter annotation = methodParameter.getParameterAnnotation(RepresentationFilter.class);
        if (annotation != null) {
            String parameter = webRequest.getParameter(annotation.name());
            return mapper.parseFilter(getRepresentationProviderClass(methodParameter, annotation), parameter);

        }

        return null;
    }

    private <C, B> Class<? extends RepresentationProvider<C, B>> getRepresentationProviderClass(MethodParameter methodParameter, RepresentationFilter filter) {

        if (filter.provider() != NoneRepresentationProvider.class) {
            return (Class<? extends RepresentationProvider<C, B>>)filter.provider();
        }

        Representation representation = methodParameter.getMethodAnnotation(Representation.class);
        if (representation != null) {
            return (Class<? extends RepresentationProvider<C, B>>)representation.value();
        }
        return null;
    }

}
