package com.marlon.peoplemanagement.common.mvc;

import com.marlon.peoplemanagement.common.representation.NoneRepresentationProvider;
import com.marlon.peoplemanagement.common.representation.RepresentationProvider;

import java.lang.annotation.*;

@Documented
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface RepresentationFilter {

    String name() default "filter";

    Class<? extends RepresentationProvider> provider() default NoneRepresentationProvider.class;
}
