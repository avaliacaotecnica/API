package com.marlon.peoplemanagement.common.mvc;

import com.marlon.peoplemanagement.common.representation.NoneRepresentationProvider;
import com.marlon.peoplemanagement.common.representation.RepresentationMapper;
import org.springframework.core.MethodParameter;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class RepresentationSortResolver implements HandlerMethodArgumentResolver {

    private final RepresentationMapper mapper;

    public RepresentationSortResolver(RepresentationMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(RepresentationSort.class) != null;
    }

    @Nullable
    @Override
    public Object resolveArgument(MethodParameter methodParameter, @Nullable ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest, @Nullable WebDataBinderFactory binderFactory) throws Exception {
        RepresentationSort annotation = methodParameter.getParameterAnnotation(RepresentationSort.class);
        if (annotation != null) {
            String parameter = webRequest.getParameter(annotation.name());
            Class providerClass = getRepresentationProviderClass(methodParameter, annotation);
            if (providerClass != null) {
                return mapper.parseSort(providerClass, parameter, null);
            }
        }

        return null;
    }

    private Class getRepresentationProviderClass(MethodParameter methodParameter, RepresentationSort filter) {

        if (filter.provider() != NoneRepresentationProvider.class) {
            return filter.provider();
        }

        Representation representation = methodParameter.getMethodAnnotation(Representation.class);
        if (representation != null) {
            return representation.value();
        }
        return null;
    }
}
