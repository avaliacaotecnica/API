package com.marlon.peoplemanagement.common.mvc;

import com.marlon.peoplemanagement.common.representation.RepresentationProvider;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Representation {
    Class<? extends RepresentationProvider> value();
}
