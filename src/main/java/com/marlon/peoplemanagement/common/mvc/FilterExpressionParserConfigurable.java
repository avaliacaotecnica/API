package com.marlon.peoplemanagement.common.mvc;

import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserConfig;

public interface FilterExpressionParserConfigurable {
    void configure(FilterExpressionParserConfig config);

    FilterExpressionParserConfig getConfiguration();
}

