package com.marlon.peoplemanagement.common.mvc;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Optional;

@ControllerAdvice
public class RepresentationResponseBodyAdvice implements ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return getAnnotation(returnType) != null;
    }

    @Nullable
    @Override
    public Object beforeBodyWrite(@Nullable Object body, MethodParameter returnType,
                                  MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                  ServerHttpRequest request, ServerHttpResponse response) {

        Representation representation = getAnnotation(returnType);

        if (representation != null) {
            return new RepresentationEntity(body, representation.value());
        }

        return body;
    }

    private Representation getAnnotation(MethodParameter returnType) {
        return Optional.ofNullable(returnType.getParameterAnnotation(Representation.class))
                .orElseGet(() -> returnType.getMethodAnnotation(Representation.class));
    }
}
