package com.marlon.peoplemanagement.common.mvc;

import org.parboiled.MatcherContext;
import org.parboiled.matchers.CustomMatcher;

public class JavaIdentifierMatcher extends CustomMatcher {

    protected JavaIdentifierMatcher() {
        super("JavaIdentifierMatcher");
    }

    @Override
    public final boolean isSingleCharMatcher() {
        return true;
    }

    @Override
    public final boolean canMatchEmpty() {
        return false;
    }

    @Override
    public boolean isStarterChar(char c) {
        return acceptChar(c);
    }

    @Override
    public final char getStarterChar() {
        return 'a';
    }

    @Override
    public final <V> boolean match(MatcherContext<V> context) {
        if (!acceptChar(context.getCurrentChar())) {
            return false;
        }
        context.advanceIndex(1);
        context.createNode();
        return true;
    }

    protected boolean acceptChar(char c) {
        return Character.isJavaIdentifierPart(c);
    }
}
