package com.marlon.peoplemanagement.common.mvc;

import java.util.Objects;
import java.util.Optional;

import com.marlon.peoplemanagement.common.querydsl.BinaryOperation;
import com.marlon.peoplemanagement.common.querydsl.NoValueOperatorType;
import com.marlon.peoplemanagement.common.querydsl.builder.NumberBuilder;
import com.marlon.peoplemanagement.common.querydsl.builder.SupportedType;
import com.marlon.peoplemanagement.common.querydsl.operator.*;
import com.marlon.peoplemanagement.common.querydsl.builder.CollectionBuilder;
import com.marlon.peoplemanagement.common.querydsl.builder.DateBuilder;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserConfig;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserDefaultConfig;
import org.parboiled.BaseParser;
import org.parboiled.Rule;
import org.parboiled.annotations.MemoMismatches;
import org.parboiled.support.Var;

public class FilterExpressionParser extends BaseParser<Object> implements FilterExpressionParserConfigurable {

    private Optional<FilterExpressionParserConfig> filterExpressionParserConfig = Optional.of(FilterExpressionParserDefaultConfig.builder().filterEncoded(false).build());

    Rule EqualOperator() {
        return FirstOf(Ch('='), String("!="));
    }

    Rule CompareOperator() {
        return FirstOf(String("<="), Ch('<'), String(">="), Ch('>'));
    }

    Rule Digit() {
        return CharRange('0', '9');
    }

    Rule FourDigits() {
        return Sequence(Digit(), Digit(), Digit(), Digit());
    }

    Rule TwoDigits() {
        return Sequence(Digit(), Digit());
    }

    Rule Date() {
        Var<DateBuilder> v = new Var<DateBuilder>();

        return Sequence(FourDigits(), v.set(new DateBuilder(match())), Ch('-'),
                TwoDigits(), v.get().setMonth(match()), Ch('-'), TwoDigits(), v
                        .get().setDay(match()),
                push(v.get().toDate()));
    }

    Rule Negative() {
        return Sequence(Ch('-'), OptionalWhitespace());
    }

    Rule Integer() {
        return OneOrMore(Digit());
    }

    Rule Decimal() {
        return Sequence(Ch('.'), Integer());
    }

    Rule Number() {
        Var<NumberBuilder> v = new Var<NumberBuilder>(new NumberBuilder());

        return Sequence(Optional(Negative(), v.get().negative()), Integer(), v
                        .get().appendInteger(match()),
                Optional(Decimal(), v.get().appendDecimal(match())), push(v
                        .get().toNumber()));
    }

    Rule True() {
        return Sequence(String("true"), push(Boolean.TRUE));
    }

    Rule False() {
        return Sequence(String("false"), push(Boolean.FALSE));
    }

    Rule StringQuoteLiteral() {
        return Sequence(ZeroOrMore(NoneOf("\r\n'")), push(match()));
    }

    Rule StringQuote() {
        return Sequence('\'', StringQuoteLiteral(), '\'');
    }

    Rule StringDoubleQuoteLiteral() {
        return Sequence(ZeroOrMore(NoneOf("\r\n\"")), push(match()));
    }

    Rule StringDoubleQuote() {
        return Sequence('"', StringDoubleQuoteLiteral(), '"');
    }

    Rule String() {
        return FirstOf(StringQuote(), StringDoubleQuote());
    }

    Rule Value() {
        return FirstOf(Date(), Number(), String());
    }

    Rule ValueWithBoolean() {
        return FirstOf(True(), False(), Value());
    }

    Rule Whitespace() {
        return OneOrMore(Ch(' '));
    }

    Rule OptionalWhitespace() {
        return ZeroOrMore(Ch(' '));
    }

    Rule NotNullOperator() {
        return Sequence(String("not"), Whitespace(), String("null"),
                push(NoValueOperatorType.IS_NOT_NULL));
    }

    Rule NullOperator() {
        return Sequence(String("null"), push(NoValueOperatorType.IS_NULL));
    }

    Rule TrueOperator() {
        return Sequence(String("true"), push(NoValueOperatorType.IS_TRUE));
    }

    Rule FalseOperator() {
        return Sequence(String("false"), push(NoValueOperatorType.IS_FALSE));
    }

    Rule NotEmptyOperator() {
        return Sequence(String("not"), Whitespace(), String("empty"),
                push(NoValueOperatorType.IS_NOT_EMPTY));
    }

    Rule EmptyOperator() {
        return Sequence(String("empty"), push(NoValueOperatorType.IS_EMPTY));
    }

    Rule NoValueOperation() {
        return Sequence(Path(),
                Whitespace(),
                String("is"),
                Whitespace(),
                FirstOf(NotNullOperator(), NullOperator(), TrueOperator(), FalseOperator(),
                        NotEmptyOperator(), EmptyOperator()),
                pushNoValueOperation());
    }

    Rule NotInOperator() {
        return Sequence(String("not"), Whitespace(), String("in"),
                push(MultipleOperatorType.NOT_IN));
    }

    Rule InOperator() {
        return Sequence(String("in"), push(MultipleOperatorType.IN));
    }

    Rule Collection(Var<CollectionBuilder> v) {
        return Sequence(
                Value(),
                v.get().start(pop()),
                ZeroOrMore(Sequence(OptionalWhitespace(), Ch(','),
                        OptionalWhitespace(), Value(), v.get().add(pop()))));
    }

    Rule In() {
        Var<CollectionBuilder> v = new Var<CollectionBuilder>();

        return Sequence(Path(), Whitespace(), FirstOf(NotInOperator(), InOperator()),
                v.set(new CollectionBuilder()), OptionalWhitespace(), Ch('('),
                OptionalWhitespace(), Collection(v), OptionalWhitespace(),
                Ch(')'), pushMultipleOperation(v.get()));
    }

    Rule NotLikeOperator() {
        return Sequence(String("not"), Whitespace(), String("like"),
                push(StringOperatorType.NOT_LIKE));
    }

    Rule LikeOperator() {
        return Sequence(String("like"), push(StringOperatorType.LIKE));
    }

    Rule LikeEscapeOperator() {
        return Sequence(String("elike"), push(StringOperatorType.LIKE_ESCAPE));
    }

    Rule NotLikeEscapeOperator() {
        return Sequence(String("not"), Whitespace(), String("elike"),
                push(StringOperatorType.NOT_LIKE_ESCAPE));
    }

    Rule Like() {
        return Sequence(Path(), Whitespace(), FirstOf(NotLikeOperator(), LikeOperator(), LikeEscapeOperator(), NotLikeEscapeOperator()),
                Whitespace(), String(), pushStringOperation());
    }

    Rule Equal() {
        return Sequence(Path(), OptionalWhitespace(), EqualOperator(), pushSimpleOperatorType(),
                OptionalWhitespace(), ValueWithBoolean(), pushSimpleOperation());
    }

    Rule Compare() {
        return Sequence(Path(), OptionalWhitespace(), CompareOperator(), pushSimpleOperatorType(),
                OptionalWhitespace(), Value(), pushSimpleOperation());
    }

    Rule ConditionRightHandSide() {
        return FirstOf(Equal(), Compare(), NoValueOperation(), Like(), In());
    }

    Rule Condition() {
        return FirstOf(
                Sequence(Ch('('), OptionalWhitespace(), Or(),
                        OptionalWhitespace(), Ch(')')),
                ConditionRightHandSide());
    }

    Rule And() {
        return Sequence(
                Condition(),
                ZeroOrMore(Sequence(Whitespace(), String("and"), Whitespace(),
                        Condition(),
                        pushBinaryOperation(BinaryOperatorType.AND))));
    }

    Rule Or() {
        return Sequence(
                And(),
                ZeroOrMore(Sequence(Whitespace(), String("or"), Whitespace(),
                        And(), pushBinaryOperation(BinaryOperatorType.OR))));
    }

    @MemoMismatches
    Rule Identifier() {
        return OneOrMore(new JavaIdentifierMatcher());
    }

    Rule QualifiedIdentifier() {
        return Sequence(Identifier(), ZeroOrMore(".", Identifier()));
    }

    Rule Path() {
        return Sequence(QualifiedIdentifier(), push(match()));
    }

    public Rule Expression() {
        return Sequence(Or(), EOI);
    }

    boolean pushSimpleOperatorType() {
        String value = match();
        SimpleOperatorType operator = SimpleOperatorType.fromSymbol(value);
        return push(operator);
    }

    boolean pushMultipleOperation(CollectionBuilder builder) {
        MultipleOperatorType operator = (MultipleOperatorType) pop();
        String path = (String) pop();
        MultipleOperation<?> operation = builder.toOperation(path, operator);
        return push(operation);
    }

    boolean pushStringOperation() {
        String value = (String) pop();
        StringOperatorType operator = (StringOperatorType) pop();
        String path = (String) pop();
        value = filterExpressionParserConfig.get().parseStringOperation(value, operator, path);
        StringOperation operation = new StringOperation(path, operator, value);
        return push(operation);
    }

    boolean pushSimpleOperation() {
        Object value = pop();
        SimpleOperatorType operator = (SimpleOperatorType) pop();
        String path = (String) pop();
        value = filterExpressionParserConfig.get().parseSimpleOperation(value, operator, path);
        SimpleOperation<?> operation = SupportedType.build(path, operator, value);
        return push(operation);
    }

    boolean pushNoValueOperation() {
        NoValueOperatorType operator = (NoValueOperatorType) pop();
        String path = (String) pop();
        NoValueOperation operation = new NoValueOperation(path, operator);
        return push(operation);
    }

    boolean pushBinaryOperation(BinaryOperatorType operator) {
        Operation operation2 = (Operation) pop();
        Operation operation1 = (Operation) pop();
        BinaryOperation operation = new BinaryOperation(
                operator, operation1, operation2);
        return push(operation);
    }

    @Override
    public void configure(FilterExpressionParserConfig config) {
        Objects.requireNonNull(config);
        this.filterExpressionParserConfig = Optional.of(config);
    }

    @Override
    public FilterExpressionParserConfig getConfiguration() {
        return this.filterExpressionParserConfig.get();
    }
}