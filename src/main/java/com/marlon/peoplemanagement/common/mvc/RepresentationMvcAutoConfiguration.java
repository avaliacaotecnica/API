package com.marlon.peoplemanagement.common.mvc;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.marlon.peoplemanagement.common.representation.RepresentationAutoConfiguration;
import com.marlon.peoplemanagement.common.representation.RepresentationMapper;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
@AutoConfigureAfter({JacksonAutoConfiguration.class, RepresentationAutoConfiguration.class})
public class RepresentationMvcAutoConfiguration implements WebMvcConfigurer {

    private final ObjectMapper objectMapper;
    private final RepresentationMapper mapper;

    public RepresentationMvcAutoConfiguration(ObjectMapper objectMapper,
                                              RepresentationMapper mapper) {
        this.objectMapper = objectMapper;
        this.mapper = mapper;
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(0, new RepresentationMapperHttpMessageConverter(objectMapper, mapper));
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(0, new RepresentationFilterResolver(mapper));
        resolvers.add(0, new RepresentationSortResolver(mapper));
    }
}
