package com.marlon.peoplemanagement.common.mvc;

import com.marlon.peoplemanagement.common.representation.NoneRepresentationProvider;
import com.marlon.peoplemanagement.common.representation.RepresentationProvider;

import java.lang.annotation.*;

@Documented
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface RepresentationSort {

    String name() default "sort";

    Class<? extends RepresentationProvider> provider() default NoneRepresentationProvider.class;
}
