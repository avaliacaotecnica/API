package com.marlon.peoplemanagement.common.mvc;

import com.marlon.peoplemanagement.common.querydsl.operator.OrderOperation;
import com.marlon.peoplemanagement.common.querydsl.operator.OrderOperatorType;
import org.parboiled.BaseParser;
import org.parboiled.Rule;
import org.parboiled.annotations.MemoMismatches;

public class SortExpressionParser extends BaseParser<Object> {

    Rule Whitespace() {
        return OneOrMore(Ch(' '));
    }

    Rule OptionalWhitespace() {
        return ZeroOrMore(Ch(' '));
    }

    Rule AscOperator() {
        return Sequence(Whitespace(), String("asc"), push(OrderOperatorType.ASC));
    }

    Rule DescOperator() {
        return Sequence(Whitespace(), String("desc"), push(OrderOperatorType.DESC));
    }

    Rule DefaultOperator() {
        return Sequence(EMPTY, push(OrderOperatorType.ASC));
    }

    Rule SortValueOperation() {
        return Sequence(Path(),
                FirstOf(AscOperator(), DescOperator(), DefaultOperator()),
                pushSortOperation());
    }

    Rule Condition() {
        return SortValueOperation();
    }

    Rule OrderBy() {
        return Sequence(
                Condition(),
                ZeroOrMore(Sequence(OptionalWhitespace(), Ch(','), OptionalWhitespace(),
                        Condition())));
    }

    @MemoMismatches
    Rule Identifier() {
        return OneOrMore(new JavaIdentifierMatcher());
    }

    Rule QualifiedIdentifier() {
        return Sequence(Identifier(), ZeroOrMore(".", Identifier()));
    }

    Rule Path() {
        return Sequence(QualifiedIdentifier(), push(match()));
    }

    public Rule Expression() {
        return Sequence(OrderBy(), EOI);
    }

    boolean pushSortOperation() {
        OrderOperatorType operator = (OrderOperatorType) pop();
        String path = (String) pop();
        OrderOperation operation = new OrderOperation(path, operator);

        return push(operation);
    }
}
