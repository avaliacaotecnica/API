package com.marlon.peoplemanagement.common.mvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marlon.peoplemanagement.common.representation.RepresentationException;
import com.marlon.peoplemanagement.common.representation.RepresentationMapper;
import com.marlon.peoplemanagement.common.model.Page;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

public class RepresentationMapperHttpMessageConverter extends MappingJackson2HttpMessageConverter {

    private final RepresentationMapper mapper;

    public RepresentationMapperHttpMessageConverter(ObjectMapper objectMapper, RepresentationMapper mapper) {
        super(objectMapper);
        this.mapper = mapper;
    }

    @Override
    protected void writeInternal(Object o, @Nullable Type type, HttpOutputMessage outputMessage) throws IOException {
        if (o instanceof RepresentationEntity) {
            RepresentationEntity entity = (RepresentationEntity) o;
            writeMessage(outputMessage, entity);
        } else {
            super.writeInternal(o, type, outputMessage);
        }
    }

    private void writeMessage(HttpOutputMessage outputMessage, RepresentationEntity entity) throws IOException {
        try {
            Object value = entity.getValue();

            if (value instanceof Page) {
                mapper.pageToStream(entity.getProvider(), (Page) value, outputMessage.getBody());
                return;
            }

            if (value instanceof Collection) {
                mapper.collectionToStream(entity.getProvider(), (Collection) value, outputMessage.getBody());
                return;
            }

            if (value != null) {
                mapper.objectToStream(entity.getProvider(), value, outputMessage.getBody());
            }
        } catch (RepresentationException e) {
            if (e.getCause() instanceof ClientAbortException) {
                throw (ClientAbortException) e.getCause();
            }

            throw e;
        }
    }

    @Override
    public Object read(Type type, @Nullable Class<?> contextClass,
                       HttpInputMessage inputMessage) throws IOException {
        if (inputMessage instanceof RepresentationHttpInputMessage) {
            RepresentationHttpInputMessage representationHttpInputMessage = (RepresentationHttpInputMessage) inputMessage;
            Class provider = representationHttpInputMessage.getProvider();

            Type targetType = representationHttpInputMessage.getTargetType();
            if (targetType instanceof ParameterizedType) {
                targetType = ((ParameterizedType) targetType).getRawType();
            }
            if (Collection.class.isAssignableFrom((Class<?>) targetType)) {
                return mapper.collectionFromStream(provider, inputMessage.getBody());
            }

            return mapper.objectFromStream(provider, inputMessage.getBody());
        } else {
            return super.read(type, contextClass, inputMessage);
        }
    }
}
