package com.marlon.peoplemanagement.common.mvc;

import com.marlon.peoplemanagement.common.representation.RepresentationProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;

public class RepresentationHttpInputMessage implements HttpInputMessage {

    private Class<? extends RepresentationProvider> provider;

    private Type targetType;

    private InputStream body;

    private HttpHeaders headers;

    public RepresentationHttpInputMessage(Class<? extends RepresentationProvider> provider,
                                          Type targetType, InputStream body, HttpHeaders headers) {
        this.provider = provider;
        this.targetType = targetType;
        this.body = body;
        this.headers = headers;
    }

    public Class<? extends RepresentationProvider> getProvider() {
        return provider;
    }


    public Type getTargetType() {
        return targetType;
    }

    @Override
    public InputStream getBody() throws IOException {
        return body;
    }

    @Override
    public HttpHeaders getHeaders() {
        return headers;
    }
}
