package com.marlon.peoplemanagement.common.mvc;


import com.marlon.peoplemanagement.common.representation.RepresentationProvider;

public class RepresentationEntity<T, R extends RepresentationProvider<T, ?>> {

    private Object value;
    private Class<R> provider;

    public RepresentationEntity(Object value, Class<R> provider) {
        this.provider = provider;
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public Class<R> getProvider() {
        return provider;
    }
}
