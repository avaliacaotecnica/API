package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marlon.peoplemanagement.repositorys.BasicRepository;

import java.io.IOException;
import java.io.Serializable;
import java.util.Optional;

public class JpaObjectLocator implements ObjectLocator {

    private final BasicRepository repository;

    public JpaObjectLocator(BasicRepository repository) {
        this.repository = repository;
    }

    @Override
    public <C, B> Optional<C> locate(Representation<C, B> configuration, JsonNode node, ObjectMapper objectMapper) {
        FieldDescriptor<C, B, ?> identifier = configuration.getIdentifier();
        if (identifier == null || !node.has(identifier.getName())) {
            return Optional.empty();
        }

        try {
            Object value = readField(identifier, node, objectMapper);
            if (value instanceof ValidableId && !((ValidableId) value).isValid()) {
                return Optional.empty();
            }

            return Optional.ofNullable(repository.findOrNull(configuration.getOutputClass(), (Serializable) value));
        } catch (IOException e) {
            throw new ObjectLocatorException("Unable to read " + configuration.getOutputClass().getName() + " identifier", e);
        }
    }

    @Override
    public <C> C locate(Class<C> clazz, Serializable id) {
        return repository.find(clazz, id);
    }

    protected <C, B> Object readField(FieldDescriptor<C, B, ?> field, JsonNode node, ObjectMapper objectMapper) throws IOException {
        return node.get(field.getName()).traverse(objectMapper).readValueAs(field.getType());
    }
}
