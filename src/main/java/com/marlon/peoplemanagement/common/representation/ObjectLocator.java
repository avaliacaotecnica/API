package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;
import java.util.Optional;

public interface ObjectLocator {
    <C, B> Optional<C> locate(Representation<C, B> configuration, JsonNode node, ObjectMapper objectMapper);

    <C> C locate(Class<C> clazz, Serializable id);
}
