package com.marlon.peoplemanagement.common.representation;

import com.marlon.peoplemanagement.common.model.MetaDescription;
import com.marlon.peoplemanagement.common.model.MetaProperties;
import com.marlon.peoplemanagement.common.model.MetaProperty;
import com.querydsl.core.types.Path;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class FieldDescriptorBuilder<C, B, F> {

    private final FieldDescriptor<C, B, F> field;

    private final RepresentationBuilder<C, B> representationBuilder;
    private final Representation<C, B> representation;

    private final Path<F> path;

    FieldDescriptorBuilder(Representation<C, B> representation, Class<F> type,
                           RepresentationBuilder<C, B> representationBuilder, Path<F> path) {
        this.representationBuilder = representationBuilder;
        this.representation = representation;
        this.path = path;
        field = new FieldDescriptor<>(representation, type);
    }

    public FieldDescriptorBuilder<C, B, F> name(String name) {
        field.setName(name);
        return this;
    }

    public FieldDescriptorBuilder<C, B, F> identifier() {
        if (!Serializable.class.isAssignableFrom(field.getType())) {
            throw new IllegalStateException("somente campos serializaveis podem ser marcados como identificadores");
        }

        field.setIdentifier(true);
        return this;
    }

    public FieldDescriptorBuilder<C, B, F> collection() {
        field.setCollection(true);
        return this;
    }

    public FieldDescriptorBuilder<C, B, F> collectionType(Class<?> collectionType) {
        field.setCollectionType(collectionType);
        return this;
    }

    public FieldDescriptorBuilder<C, B, F> map() {
        field.setMap(true);
        return this;
    }

    public FieldDescriptorBuilder<C, B, F> mapKeyType(Class<?> mapKeyType) {
        field.setMapKeyType(mapKeyType);
        return this;
    }

    public FieldDescriptorBuilder<C, B, F> configuration(Representation<F, ?> configuration) {
        field.setConfiguration(configuration);
        return this;
    }


    public FieldDescriptorBuilder<C, B, F> configuration(RepresentationProvider<F, ?> configurationProvider) {
        return configuration(configurationProvider.getRepresentation());
    }


    public FieldDescriptorBuilder<C, B, F> configuration(Class<? extends RepresentationProvider<F, ?>> configurationProvider) {
        field.setConfiguration(configurationProvider);
        return this;
    }

    public FieldDescriptorBuilder<C, B, F> getterMethod(Function<C, ?> getterMethod) {
        field.setGetterMethod(getterMethod);
        return this;
    }

    public FieldDescriptorBuilder<C, B, F> setterMethod(BiConsumer<B, F> setterMethod) {
        field.setSetterMethod(setterMethod);
        return readonly(setterMethod == null);
    }

    public FieldDescriptorBuilder<C, B, F> readonly(boolean readonly) {
        field.setReadonly(readonly);
        return this;
    }

    public RepresentationBuilder<C, B> add() {
        representation.addField(build());
        return representationBuilder;
    }

    protected FieldDescriptor<C, B, F> build() {
        Objects.requireNonNull(field.getName(), "o nome do campo é obrigatório");

        if (field.getSetterMethod() == null && !field.isReadOnly()) {
            configureSetter();
        }

        if (field.getGetterMethod() == null) {
            configureGetter();
        }

        configureMetaDescription();
        configureMetaProperties();

        return field;
    }

    private void configureGetter() {
        Method method = locateGetterMethod();
        if (method != null) {
            field.setGetterMethod(target -> InvocationHelper.invoke(method, target));
        }
    }

    private Method locateGetterMethod() {
        Class<C> outputClass = field.getRepresentation().getOutputClass();
        String capitalizedName = StringUtils.capitalize(field.getName());

        Method method;
        if (field.getType().equals(Boolean.class) &&
                (method = MethodUtils.getMatchingAccessibleMethod(outputClass, "is" + capitalizedName)) != null) {
            return method;
        }

        return MethodUtils.getMatchingAccessibleMethod(outputClass, "get" + capitalizedName);
    }

    private void configureSetter() {
        Class<B> inputClass = field.getRepresentation().getInputClass();

        Method method;
        if (field.isCollection()) {
            method = MethodUtils.getMatchingAccessibleMethod(inputClass, field.getName(), field.getCollectionType());
        } else if (field.isMap()) {
            method = MethodUtils.getMatchingAccessibleMethod(inputClass, field.getName(), Map.class);
        } else {
            method = MethodUtils.getMatchingAccessibleMethod(inputClass, field.getName(), field.getType());
        }

        if (method != null) {
            field.setSetterMethod((target, value) -> InvocationHelper.invoke(method, target, value));
        }
    }

    private void configureMetaDescription() {

        Field outputField = FieldUtils.getField(representation.getOutputClass(), this.field.getName(), true);
        if (isNull(outputField)) {
            return;
        }

        MetaDescription description = outputField.getAnnotation(MetaDescription.class);
        if (isNull(description)) {
            return;
        }

        if (isNull(field.getDescription())) {
            field.setDescription(description.value());
        }

        if (isNull(field.getObservations())) {
            field.setObservations(description.observations());
        }
    }

    private void configureMetaProperties() {
        Map<String, String> properties = null;
        Field outputField = FieldUtils.getField(representation.getOutputClass(), this.field.getName(), true);
        if (isNull(outputField)) {
            return;
        }

        MetaProperties metaProperties = outputField.getAnnotation(MetaProperties.class);
        if (nonNull(metaProperties)) {
            properties = new HashMap<>();
            for (MetaProperty metaProperty : metaProperties.value()) {
                String name = metaProperty.name();
                String value = metaProperty.value();

                properties.put(name, value);
            }
        }
        field.setProperties(properties);
    }
}