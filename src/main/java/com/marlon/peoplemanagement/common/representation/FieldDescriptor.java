package com.marlon.peoplemanagement.common.representation;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class FieldDescriptor<C, B, F> {

    private Representation<C, B> representation;

    private final Class<F> type;

    private Representation<F, ?> configuration;

    private Class<? extends RepresentationProvider<F, ?>> configurationProvider;

    private String name;
    private String description;
    private String observations;
    private Map<String, String> properties;

    private boolean identifier;
    private boolean collection;
    private boolean map;
    private boolean standard;
    private boolean readonly;
    private boolean needInitialization;

    private ValueConverter converter;
    private Class<?> convertTo;

    private Function<C, ?> getterMethod;
    private BiConsumer<B, ?> setterMethod;

    private Class<?> collectionType;
    private Class<?> collectionImplementationType;

    private Class<?> mapKeyType;
    private Class<?> mapImplementationType;

    FieldDescriptor(Representation<C, B> representation, Class<F> type) {
        Objects.requireNonNull(representation, "a representação é obrigatória");
        Objects.requireNonNull(type, "o tipo é obrigatório");

        this.representation = representation;
        this.type = type;
    }

    void setName(String name) {
        this.name = name;
    }

    void setDescription(String description) {
        this.description = description;
    }

    void setObservations(String observations) {
        this.observations = observations;
    }

    void setIdentifier(boolean identifier) {
        this.identifier = identifier;
    }

    void setCollection(boolean collection) {
        this.collection = collection;
    }

    void setMap(boolean map) {
        this.map = map;
    }

    void setCollectionType(Class<?> collectionType) {
        this.collectionType = collectionType;
    }

    void setCollectionImplementationType(Class<?> collectionImplementationType) {
        this.collectionImplementationType = collectionImplementationType;
    }

    void setMapKeyType(Class<?> mapKeyType) {
        this.mapKeyType = mapKeyType;
    }

    void setMapImplementationType(Class<?> mapImplementationType) {
        this.mapImplementationType = mapImplementationType;
    }

    void setStandard(boolean standard) {
        this.standard = standard;
    }

    void setConfiguration(Representation<F, ?> configuration) {
        this.configuration = configuration;
    }

    void setConfiguration(Class<? extends RepresentationProvider<F, ?>> configurationProvider) {
        this.configurationProvider = configurationProvider;
    }

    <T> void setConverter(Class<T> to, ValueConverter<F, T> converter) {
        this.convertTo = to;
        this.converter = converter;
    }

    void setGetterMethod(Function<C, ?> getterMethod) {
        this.getterMethod = getterMethod;
    }

    void setSetterMethod(BiConsumer<B, ?> setterMethod) {
        this.setterMethod = setterMethod;
    }

    void setNeedInitialization(boolean needInitialization) {
        this.needInitialization = needInitialization;
    }

    void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getObservations() {
        return observations;
    }

    public Class<F> getType() {
        return type;
    }

    public Representation<C, B> getRepresentation() {
        return representation;
    }

    public Class<?> getOutputType() {
        return convertTo != null ? convertTo : type;
    }

    public boolean isIdentifier() {
        return identifier;
    }

    public boolean isCollection() {
        return collection;
    }

    public Class<?> getCollectionType() {
        return collectionType;
    }

    public Class<?> getCollectionImplementationType() {
        return collectionImplementationType;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public Collection createCollection() {
        try {
            if (collectionImplementationType != null) {
                return (Collection) collectionImplementationType.newInstance();
            }

            if (collectionType != null && Set.class.isAssignableFrom(collectionType)) {
                return new LinkedHashSet();
            }

            return new ArrayList();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalStateException("falha ao criar uma nova coleção para o campo " + this, e);
        }
    }

    public Collection createEmptyCollection() {
        if (Set.class.isAssignableFrom(collectionType)) {
            return Collections.emptySet();
        }

        return Collections.emptyList();
    }

    public boolean isMap() {
        return map;
    }

    public Class<?> getMapImplementationType() {
        return mapImplementationType;
    }

    public Map createMap() {
        try {
            Class mapImplementationType = getMapImplementationType();
            return mapImplementationType != null ? (Map) mapImplementationType.newInstance() : new LinkedHashMap();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalStateException("falha ao criar um novo mapa para o campo " + this, e);
        }
    }

    public Class<?> getMapKeyType() {
        return mapKeyType;
    }

    public boolean isStandard() {
        return standard;
    }

    public boolean hasConverter() {
        return converter != null;
    }

    public ValueConverter getConverter() {
        return converter;
    }

    public BiConsumer<B, ?> getSetterMethod() {
        return setterMethod;
    }

    public Function<C, ?> getGetterMethod() {
        return getterMethod;
    }

    public boolean isReadOnly() {
        return readonly;
    }

    public boolean needInitialization() {
        return needInitialization;
    }

    public Representation<F, ?> getConfiguration() {
        return configuration;
    }

    public Class<? extends RepresentationProvider<F, ?>> getConfigurationProvider() {
        return configurationProvider;
    }

    public boolean hasConfiguration() {
        return configuration != null || configurationProvider != null;
    }

    @Override
    public String toString() {
        return representation + "#" + name;
    }
}
