package com.marlon.peoplemanagement.common.representation;

import java.io.IOException;

public abstract class RepresentationCustomizer<C, B> {

    public void onSerialize(SerializerContext<C, B> context) throws IOException {

    }
    public void onDeserialize(DeserializerContext<C, B> context) {

    }
}
