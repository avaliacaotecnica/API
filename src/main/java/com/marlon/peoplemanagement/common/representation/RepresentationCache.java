package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class RepresentationCache {

    private final Map<Class, Representation> cache = new ConcurrentHashMap<>();

    public <C, B> Optional<Representation<C, B>> resolveExpecialization(Representation<C, B> representation, Object target) {
        if (!representation.getExpecializations().isEmpty()) {
            return representation.getExpecializations().stream()
                    .filter(rep -> rep.getApplyEntity().apply(target))
                    .findFirst()
                    .map(ConditionalExpecialization::getRepresentationClass)
                    .map(clazz -> resolve((Class<? extends RepresentationProvider<C, B>>) clazz));
        }
        return Optional.of(representation);
    }

    public <C, B> Optional<Representation<C, B>> resolveExpecialization(Representation<C, B> representation, JsonNode target) {
        if (!representation.getExpecializations().isEmpty()) {
            return representation.getExpecializations().stream()
                    .filter(rep -> rep.getApplyJson().apply(target))
                    .findFirst()
                    .map(ConditionalExpecialization::getRepresentationClass)
                    .map(clazz -> resolve((Class<? extends RepresentationProvider<C, B>>) clazz));
        }
        return Optional.of(representation);
    }

    public <C, B> Representation<C, B> resolve(Class<? extends RepresentationProvider<C, B>> provider) {
        if (provider == null) {
            return null;
        }
        return cache.computeIfAbsent(provider, this::instantiate);
    }


    private <C, B> Representation<C, B> instantiate(Class<? extends RepresentationProvider<C, B>> representation) {
        try {
            return representation.newInstance().getRepresentation();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
