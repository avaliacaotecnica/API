package com.marlon.peoplemanagement.common.representation;

public interface ValueConverter<F, T> {
    T toRepresentation(F value, SerializerContext context);

    F fromRepresentation(T value, DeserializerContext context);
}
