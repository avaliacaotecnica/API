package com.marlon.peoplemanagement.common.representation;

public class NoneRepresentationProvider implements RepresentationProvider {

    private NoneRepresentationProvider() {
    }

    @Override
    public Representation getRepresentation() {
        return null;
    }
}
