package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class Context<X extends Context, C, B> {

    private X parent;

    private ObjectMapper objectMapper;

    private RepresentationCache representationCache;

    private Representation<C, B> representation;

    protected Context(X context) {
        this.parent = (X) context.getParent();
        this.objectMapper = context.getObjectMapper();
        this.representationCache = context.getRepresentationCache();
        this.representation = context.getRepresentation();
    }

    protected Context(X context, Representation<C, B> representation) {
        this(context);
        this.parent = context;
        this.representation = representation;
    }

    protected Context(ObjectMapper objectMapper,
                      RepresentationCache representationCache, Representation<C, B> representation) {
        this.objectMapper = objectMapper;
        this.representationCache = representationCache;
        this.representation = representation;
    }

    public X representation(Class<? extends RepresentationProvider<C, B>> provider) {
        return representation(representationCache.resolve(provider));
    }

    public X representation(Representation<C, B> representation) {
        this.representation = representation;
        return (X) this;
    }

    public X getParent() {
        return parent;
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public RepresentationCache getRepresentationCache() {
        return representationCache;
    }

    public Representation<C, B> getRepresentation() {
        return representation;
    }
}
