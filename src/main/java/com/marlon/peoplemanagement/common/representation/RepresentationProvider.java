package com.marlon.peoplemanagement.common.representation;

import com.marlon.peoplemanagement.common.model.MetaIgnore;

@MetaIgnore
public interface RepresentationProvider<O, I> {
    Representation<O, I> getRepresentation();
}
