package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

public class DeserializerContext<C, B> extends Context<DeserializerContext, C, B> implements Serializable {

    private static final long serialVersionUID = 2405172041950251800L;

    private Deserializer deserializer;

    private ObjectLocator locator;

    private JsonNode node;

    private C destination;

    private B input;

    private boolean copyOnly;

    public static <C, B> DeserializerContext<C, B> of(Representation<C, B> representation, Deserializer deserializer,
                                                      ObjectMapper objectMapper, ObjectLocator objectLocator,
                                                      RepresentationCache representationCache) {
        return new DeserializerContext<>(representation, deserializer, objectMapper, objectLocator, representationCache);
    }

    public DeserializerContext(Representation<C, B> representation, Deserializer deserializer, ObjectMapper objectMapper,
                               ObjectLocator objectLocator, RepresentationCache representationCache) {
        super(objectMapper, representationCache, representation);
        this.deserializer = deserializer;
        this.locator = objectLocator;
    }


    private DeserializerContext(DeserializerContext context, Representation<C, B> representation) {
        super(context, representation);
        this.deserializer = context.deserializer;
        this.locator = context.locator;
        this.copyOnly = context.copyOnly;
    }

    private DeserializerContext(DeserializerContext<C, B> context) {
        super(context);
        this.deserializer = context.deserializer;
        this.locator = context.locator;
        this.node = context.node;
        this.destination = context.destination;
        this.input = context.input;
    }

    public DeserializerContext<C, B> with(JsonNode node) {
        this.node = node;
        return this;
    }

    public DeserializerContext<C, B> with(JsonNode node, C destination) {
        this.node = node;
        this.destination = destination;
        return this;
    }

    public DeserializerContext<C, B> with(Class<? extends RepresentationProvider<C, B>> provider, JsonNode node) {
        return with(getRepresentationCache().resolve(provider), node);
    }

    public DeserializerContext<C, B> with(Representation<C, B> representation, JsonNode node) {
        representation(representation);
        this.node = node;
        return this;
    }

    public <X, Z> DeserializerContext<X, Z> child(Class<? extends RepresentationProvider<X, Z>> provider, JsonNode node) {
        return child(getRepresentationCache().resolve(provider), node);
    }

    public <X, Z> DeserializerContext<X, Z> child(Representation<X, Z> representation, JsonNode node) {
        return new DeserializerContext<>(this, representation).with(node);
    }

    public DeserializerContext<C, B> child(JsonNode node) {
        return new DeserializerContext<>(this, getRepresentation()).with(node);
    }

    public DeserializerContext<C, B> cloneContext() {
        return new DeserializerContext<>(this);
    }

    public DeserializerContext<C, B> node(JsonNode node) {
        this.node = node;
        return this;
    }

    public DeserializerContext<C, B> destination(C destination) {
        this.destination = destination;
        return this;
    }

    public DeserializerContext<C, B> input(B input) {
        this.input = input;
        return this;
    }

    public DeserializerContext<C, B> copyOnly(boolean copyOnly) {
        this.copyOnly = copyOnly;
        return this;
    }

    public Deserializer getDeserializer() {
        return deserializer;
    }

    public ObjectLocator getLocator() {
        return locator;
    }

    public JsonNode getNode() {
        return node;
    }

    public C getDestination() {
        return destination;
    }

    public B getInput() {
        return input;
    }

    public boolean hasDestination() {
        return destination != null;
    }

    public boolean isCopyOnly() {
        return copyOnly;
    }
}
