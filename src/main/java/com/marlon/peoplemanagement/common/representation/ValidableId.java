package com.marlon.peoplemanagement.common.representation;

public interface ValidableId {
    boolean isValid();
}
