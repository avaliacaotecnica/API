package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marlon.peoplemanagement.common.model.Page;
import org.apache.catalina.connector.ClientAbortException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import static com.marlon.peoplemanagement.common.representation.SerializerContext.of;
import static java.util.Objects.isNull;

public class Serializer {

    private static final String UNABLE_TO_WRITE_OBJECT_MESSAGE = "não foi possível escrever o objeto";

    private static final Logger LOGGER = LoggerFactory.getLogger(Serializer.class);

    private RepresentationCache representationCache;
    private ObjectMapper objectMapper;
    private ObjectInitializer objectInitializer;
    private RepresentationProperties configuration;

    public Serializer(RepresentationCache representationCache, ObjectMapper objectMapper,
                      ObjectInitializer objectInitializer, RepresentationProperties configuration) {
        this.representationCache = representationCache;
        this.objectMapper = objectMapper;
        this.objectInitializer = objectInitializer;
        this.configuration = configuration;
    }

    public <C, B> void writeObject(SerializerContext<C, B> context) {
        try {

            representationCache
                    .resolveExpecialization(context.getRepresentation(), context.getObject())
                    .ifPresent(context::representation);

            JsonGenerator generator = context.getGenerator();
            generator.writeStartObject();

            Class<? extends RepresentationProvider> parentProvider = context.getRepresentation().getParent();
            Representation<C, B> parent = representationCache
                    .resolve((Class<? extends RepresentationProvider<C, B>>) parentProvider);
            if (parent != null) {
                SerializerContext<C, B> parentContext = context.cloneContext().representation(parent);
                writeFields(parentContext);
            }

            writeFields(context);

            for (RepresentationCustomizer customizer : context.getRepresentation().getCustomizers()) {
                executeCustomizer(customizer, context);
            }

            generator.writeEndObject();
        } catch (IOException e) {
            throw new RepresentationException(UNABLE_TO_WRITE_OBJECT_MESSAGE, e);
        }
    }

    public <C, B> void writeFields(SerializerContext<C, B> context) throws IOException {
        for (FieldDescriptor property : context.getRepresentation().getFields().values()) {
            writeField(property, context);
        }
    }

    private <C, B> void executeCustomizer(RepresentationCustomizer customizer, SerializerContext<C, B> context) throws IOException {
        try {
            customizer.onSerialize(context);
        } catch (Exception e) {
            if (e instanceof ClientAbortException) {
                throw e;
            }

            LOGGER.error("não foi possível executar o customizer {}", customizer.getClass().getName(), e);
        }
    }

    private <C, B> void writeField(FieldDescriptor field, SerializerContext<C, B> context) throws IOException {
        Function<C, ?> getterMethod = field.getGetterMethod();
        if (isNull(getterMethod)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("ignorando o campo " + field.getName() + " por não possuir um metodo de get configurado");
            }
            return;
        }


        JsonGenerator generator = context.getGenerator();

        try {
            generator.writeFieldName(field.getName());
            Object value = getterMethod.apply(context.getObject());
            if (value == null) {
                generator.writeNull();
                return;
            }

            if (field.needInitialization()) {
                value = objectInitializer.initialize(value);
            }

            if (field.isCollection()) {
                writeCollection(value, field, context);
            } else if (field.isMap()) {
                writeMap(value, field, context);
            } else {
                writeSimpleField(value, field, context);
            }
        } catch (Exception e) {
            if (e instanceof ClientAbortException) {
                throw e;
            }

            LOGGER.error("Não foi possível escrever o campo {} do objeto {}",
                    field.getName(), context.getObject().getClass().getName(), e);
            writeNull(field, context, e);
        }
    }

    private <C, B> void writeNull(FieldDescriptor field, SerializerContext<C, B> context, Exception e) {
        try {
            context.getGenerator().writeNull();
        } catch (IOException ex) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("falha ao excrever null no campo {} do objeto {} após erro de serialização",
                        field.getName(), context.getObject().getClass().getName(), e);
            }
        }
    }

    private <C, B> void writeMap(Object value, FieldDescriptor field, SerializerContext<C, B> context) throws IOException {
        if (!(value instanceof Map)) {
            throw new IllegalArgumentException("Coleção invalida para o campo " + field.getName());
        }

        JsonGenerator generator = context.getGenerator();
        generator.writeStartObject();
        Set<Map.Entry> set = ((Map) value).entrySet();
        for (Map.Entry entry : set) {
            generator.writeFieldName(entry.getKey().toString());
            writeSimpleField(entry.getValue(), field, context);
        }

        generator.writeEndObject();
    }

    private <C, B> void writeCollection(Object value, FieldDescriptor field, SerializerContext<C, B> context) throws IOException {

        if (!(value instanceof Collection)) {
            throw new IllegalArgumentException("Coleção invalida para o campo " + field.getName());
        }

        JsonGenerator generator = context.getGenerator();
        generator.writeStartArray();

        Collection collection = (Collection) value;
        for (Object current : collection) {
            writeSimpleField(current, field, context);
        }

        generator.writeEndArray();
    }

    private <C, B> void writeSimpleField(Object value, FieldDescriptor field, SerializerContext<C, B> context) throws IOException {
        if (field.getConfiguration() != null) {
            writeObject(context.child(value, field.getConfiguration()));
        } else if (field.getConfigurationProvider() != null) {
            writeObject(context.child(value, field.getConfigurationProvider()));
        } else {
            JsonGenerator generator = context.getGenerator();
            if (field.hasConverter()) {
                generator.writeObject(field.getConverter().toRepresentation(value, context));
            } else {
                generator.writeObject(value);
            }
        }
    }

    public <C, B> String toJson(Representation<C, B> representation, C object) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            toStream(representation, object, outputStream);
            return outputStream.toString(configuration.getCharset());
        } catch (IOException e) {
            throw new RepresentationException(UNABLE_TO_WRITE_OBJECT_MESSAGE, e);
        }
    }

    public <C, B> void toStream(Representation<C, B> configuration, Page<C> page, OutputStream output) throws IOException {

        JsonGenerator generator = objectMapper.getFactory().createGenerator(output);

        generator.writeStartObject();
        generator.writeNumberField("offset", page.getPageable().getOffset());
        generator.writeNumberField("limit", page.getPageable().getLimit());
        generator.writeNumberField("total", page.getTotal());
        generator.writeBooleanField("hasNext", page.getTotal() > page.getPageable().getOffset() +
                page.getPageable().getLimit());

        generator.writeFieldName("content");
        generator.writeStartArray();

        page.getContent().forEach(c -> writeObject(of(configuration, this, objectMapper,
                generator, representationCache).with(c)));

        generator.writeEndArray();

        generator.flush();
        generator.close();
    }

    public <C, B> void toStream(Representation<C, B> configuration, Collection<C> collection, OutputStream output) throws IOException {
        JsonGenerator generator = objectMapper.getFactory().createGenerator(output);

        generator.writeStartArray();

        collection.forEach(c -> writeObject(of(configuration, this, objectMapper,
                generator, representationCache).with(c)));

        generator.writeEndArray();

        generator.flush();
        generator.close();
    }

    public <C, B> void toStream(Representation<C, B> configuration, C object, OutputStream outputStream) throws IOException {
        JsonGenerator generator = objectMapper.getFactory().createGenerator(outputStream);
        writeObject(of(configuration, this, objectMapper, generator, representationCache).with(object));
        generator.flush();
        generator.close();
    }
}
