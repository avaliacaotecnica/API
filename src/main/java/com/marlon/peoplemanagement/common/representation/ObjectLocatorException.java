package com.marlon.peoplemanagement.common.representation;

public class ObjectLocatorException extends RuntimeException {
    public ObjectLocatorException(String message, Exception exception) {
        super(message, exception);
    }
}
