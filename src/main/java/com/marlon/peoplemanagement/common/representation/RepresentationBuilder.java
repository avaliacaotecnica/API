package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.JsonNode;
import com.marlon.peoplemanagement.common.model.MetaDescription;
import com.marlon.peoplemanagement.common.model.MetaName;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.ListPath;
import com.querydsl.core.types.dsl.MapPath;
import com.querydsl.core.types.dsl.SetPath;
import com.querydsl.core.types.dsl.SimpleExpression;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.MethodUtils;

import java.lang.reflect.Method;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.util.Objects.nonNull;

public class RepresentationBuilder<O, I> {

    private final Representation<O, I> representation;

    private RepresentationBuilder(Class<O> objectClass, Class<I> builderClass) {
        representation = new Representation<>(objectClass, builderClass);
    }

    public static <T> RepresentationBuilder<T, T> create(Class<T> outputClass) {
        return new RepresentationBuilder<>(outputClass, outputClass);
    }

    public static <T, B> RepresentationBuilder<T, B> create(Class<T> outputClass, Class<B> inputClass) {
        return new RepresentationBuilder<>(outputClass, inputClass);
    }

    public RepresentationBuilder<O, I> name(String name) {
        representation.setName(name);
        return this;
    }

    public <F> FieldDescriptorBuilder<O, I, F> field(Class<F> type, String name) {
        return new FieldDescriptorBuilder<>(representation, type, this, null)
                .name(name);
    }

    public <F> FieldDescriptorBuilder<O, I, F> field(Class<F> type, String name, Function<O, F> getterMethod) {
        return new FieldDescriptorBuilder<>(representation, type, this, null)
                .name(name).getterMethod(getterMethod);
    }

    public <F> FieldDescriptorBuilder<O, I, F> field(Class<F> type, String name, Function<O, F> getterMethod,
                                                     BiConsumer<I, F> setterMethod) {
        return new FieldDescriptorBuilder<>(representation, type, this, null)
                .name(name).getterMethod(getterMethod).setterMethod(setterMethod);
    }

    public <F> FieldDescriptorBuilder<O, I, F> field(Class<F> type) {
        return new FieldDescriptorBuilder<>(representation, type, this, null);
    }

    public <F> FieldDescriptorBuilder<O, I, F> identifier(Path<F> path) {
        return field(path).identifier();
    }

    public <F> FieldDescriptorBuilder<O, I, F> field(Path<F> path) {
        return new FieldDescriptorBuilder<>(representation, (Class<F>) path.getType(), this, path)
                .name(path.getMetadata().getName());
    }

    public <F> FieldDescriptorBuilder<O, I, F> map(MapPath<?, F, ?> path) {
        return new FieldDescriptorBuilder<>(representation, path.getValueType(), this, null)
                .mapKeyType(path.getKeyType())
                .name(path.getMetadata().getName())
                .map();
    }

    public <K, F> FieldDescriptorBuilder<O, I, F> map(Class<K> keyType, Class<F> type, String name) {
        return new FieldDescriptorBuilder<>(representation, type, this, null)
                .mapKeyType(keyType)
                .name(name)
                .map();
    }

    public <K, F> FieldDescriptorBuilder<O, I, F> map(Class<K> keyType, Class<F> type, String name,
                                                      Function<O, Map<K, F>> getterMethod) {
        return new FieldDescriptorBuilder<>(representation, type, this, null)
                .mapKeyType(keyType)
                .name(name)
                .getterMethod(getterMethod)
                .map();
    }

    public <F> RepresentationBuilder<O, I> expression(Path<F> path) {
        return expression(path, null);
    }

    public <F> RepresentationBuilder<O, I> expression(Path<F> path, String description) {
        String name = getExpressionName(path);
        if (!(path instanceof SimpleExpression)) {
            throw new IllegalArgumentException("o path " + name +
                    " precisa ser uma SimpleExpression");
        }

        return expression(name, (SimpleExpression<?>) path, description,
                true, true);
    }

    private <F> String getExpressionName(Path<F> path) {
        return StringUtils.substringAfter(path.toString(), ".");
    }

    public RepresentationBuilder<O, I> expression(String name, SimpleExpression<?> expression) {
        return expression(name, expression, null);
    }

    public RepresentationBuilder<O, I> expression(String name, SimpleExpression<?> expression, String description) {
        return expression(name, expression, description, true, true);
    }

    public RepresentationBuilder<O, I> expression(String name, SimpleExpression<?> expression,
                                                  boolean filterable, boolean sortable) {
        return expression(name, expression, null, filterable, sortable);
    }

    public RepresentationBuilder<O, I> expression(String name, SimpleExpression<?> expression, String description,
                                                  boolean filterable, boolean sortable) {

        if (filterable) {
            representation.addFilterExpression(name, expression, description);
        }

        if (sortable) {
            representation.addSortExpression(name, expression, description);
        }

        return this;
    }

    public Representation<O, I> build() {
        configureCreateMethod();
        configureFromMethod();
        configureBuildMethod();
        configureSkipValidateMethod();
        configureOldValuesMethod();

        configureName();
        configureDescription();

        return representation;
    }

    private void configureOldValuesMethod() {
        if (representation.keepOldValues() && representation.getOldValuesMethod() == null) {
            Method method = MethodUtils.getMatchingAccessibleMethod(representation.getInputClass(), "withOldValues",
                    representation.getOutputClass());
            if (method != null) {
                representation.setOldValuesMethod((input, old) -> InvocationHelper.invoke(method, input, old));
            }
        }
    }

    private void configureBuildMethod() {
        if (representation.getBuildMethod() == null && !representation.isReadOnly()) {

            if (Objects.equals(representation.getInputClass(), representation.getOutputClass())) {
                representation.setBuildMethod(value -> (O) value);
            } else {
                Method method = MethodUtils.getMatchingAccessibleMethod(representation.getInputClass(), "build");
                if (method != null) {
                    representation.setBuildMethod(target -> InvocationHelper.invoke(method, target));
                } else {
                    throw new RepresentationException("builder class should have a build method with no args");
                }
            }

        }
    }

    private void configureCreateMethod() {
        if (representation.getCreateMethod() == null && !representation.isReadOnly()) {
            Method method = MethodUtils.getMatchingAccessibleMethod(representation.getInputClass(), "create");
            if (method != null) {
                representation.setCreateMethod(() -> InvocationHelper.invoke(method, null));
            }
        }
    }

    private void configureFromMethod() {
        if (representation.getFromMethod() == null && !representation.isReadOnly()) {
            Method method = MethodUtils.getMatchingAccessibleMethod(representation.getInputClass(), "from",
                    representation.getOutputClass());
            if (method != null) {
                representation.setFromMethod(value -> InvocationHelper.invoke(method, null, value));
            }
        }
    }

    private void configureSkipValidateMethod() {
        if (representation.getSkipValidateMethod() == null) {
            Method method = MethodUtils.getMatchingAccessibleMethod(representation.getInputClass(), "skipValidate");
            if (method != null) {
                representation.setSkipValidateMethod(intance -> InvocationHelper.invoke(method, intance));
            }
        }
    }

    private void configureName() {
        if (nonNull(representation.getName())) {
            return;
        }

        MetaName annotation = representation.getOutputClass().getAnnotation(MetaName.class);
        if (nonNull(annotation)) {
            representation.setName(annotation.value());
        } else {
            representation.setName(representation.getOutputClass().getSimpleName());
        }
    }

    private void configureDescription() {
        if (nonNull(representation.getDescription())) {
            return;
        }

        MetaDescription annotation = representation.getOutputClass().getAnnotation(MetaDescription.class);
        if (nonNull(annotation)) {
            representation.setDescription(annotation.value());
        }
    }

    public RepresentationBuilder<O, I> readOnly() {
        representation.setReadOnly(true);
        return this;
    }

    public RepresentationBuilder<O, I> alwaysCreate() {
        representation.setAlwaysCreate(true);
        return this;
    }

    public RepresentationBuilder<O, I> customizer(RepresentationCustomizer<O, I> customizer) {
        representation.addCustomizer(customizer);
        return this;
    }

    public RepresentationBuilder<O, I> ignoreUnknownFields() {
        representation.setIgnoreUnknownFields(true);
        return this;
    }

    public RepresentationBuilder<O, I> expecialization(Class<? extends RepresentationProvider> representationClass,
                                                       Function<JsonNode, Boolean> resolveFromJson,
                                                       Function<Object, Boolean> resolveFromEntity) {
        representation.addExpecialization(ConditionalExpecialization.of(representationClass,
                resolveFromJson, resolveFromEntity));
        return this;
    }

    public RepresentationBuilder<O, I> parent(Class<? extends RepresentationProvider> parent) {
        representation.setParent(parent);
        return this;
    }

    public RepresentationBuilder<O, I> createMethod(Supplier<I> createMethod) {
        representation.setCreateMethod(createMethod);
        return this;
    }

    public <F> FieldDescriptorBuilder<O, I, F> collection(Class<F> type, String name,
                                                          Function<O, Collection<F>> getterMethod) {
        return new FieldDescriptorBuilder<>(representation, type, this, null)
                .name(name)
                .getterMethod(getterMethod)
                .collection();
    }

    public <F> FieldDescriptorBuilder<O, I, F> collection(Class<F> type, String name) {
        return new FieldDescriptorBuilder<>(representation, type, this, null)
                .name(name)
                .collection();
    }

    public <F> FieldDescriptorBuilder<O, I, F> collection(SetPath<F, ?> path) {
        return new FieldDescriptorBuilder<>(representation, path.getElementType(), this, null)
                .collectionType(Set.class)
                .name(path.getMetadata().getName())
                .collection();
    }

    public <F> FieldDescriptorBuilder<O, I, F> collection(ListPath<F, ?> path) {
        return new FieldDescriptorBuilder<>(representation, path.getElementType(), this, null)
                .collectionType(List.class)
                .name(path.getMetadata().getName())
                .collection();
    }
}