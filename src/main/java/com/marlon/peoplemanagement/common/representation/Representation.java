package com.marlon.peoplemanagement.common.representation;

import com.marlon.peoplemanagement.common.model.Sort;
import com.querydsl.core.types.dsl.SimpleExpression;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class Representation<O, I> {

    private String name;

    private String description;

    private final Class<O> outputClass;

    private final Class<I> inputClass;

    private final Map<String, FieldDescriptor<O, I, ?>> fields = new LinkedHashMap<>();

    private final List<RepresentationCustomizer> customizers = new ArrayList<>();

    private final Map<String, SimpleExpression<?>> filterExpressions = new HashMap<>();

    private final Map<String, SimpleExpression<?>> sortExpressions = new HashMap<>();

    private final Map<String, Expression> expressions = new HashMap<>();

    private Sort defaultSort;

    private FieldDescriptor<O, I, ?> identifier;

    private boolean ignoreUnknownFields;

    private boolean readOnly;

    private boolean alwaysCreate;

    private boolean skipValidate;

    private boolean keepOldValues;

    private Supplier<I> createMethod;

    private Function<O, I> fromMethod;

    private Function<I, O> buildMethod;

    private Consumer<I> skipValidateMethod;

    private BiConsumer<I, O> oldValuesMethod;

    private Class<? extends RepresentationProvider> parent;

    private List<ConditionalExpecialization> expecializations = new ArrayList<>();

    public Representation(Class<O> outputClass, Class<I> inputClass) {
        Objects.requireNonNull(outputClass, "output class is required");
        Objects.requireNonNull(inputClass, "input class is required");

        this.outputClass = outputClass;
        this.inputClass = inputClass;
    }

    void setParent(Class<? extends RepresentationProvider> parent) {
        this.parent = parent;
    }

    void setName(String name) {
        this.name = name;
    }

    void setDescription(String description) {
        this.description = description;
    }

    void addField(FieldDescriptor<O, I, ?> field) {
        if (fields.containsKey(field.getName())) {
            throw new IllegalArgumentException("field with name " + field.getName() + " is already registered");
        }

        fields.put(field.getName(), field);

        if (field.isIdentifier()) {
            identifier = field;
        }
    }

    void addSortExpression(String name, SimpleExpression<?> expressionValue, String description) {
        this.sortExpressions.put(name, expressionValue);

        Expression expression = this.expressions.computeIfAbsent(name,
                s -> new Expression(name, expressionValue));

        expression.setSort(true);
        expression.setDescription(description);
    }

    void addFilterExpression(String name, SimpleExpression<?> expressionValue, String description) {
        this.filterExpressions.put(name, expressionValue);

        Expression expression = this.expressions.computeIfAbsent(name,
                s -> new Expression(name, expressionValue));

        expression.setFilter(true);
        expression.setDescription(description);
    }

    void setDefaultSort(Sort defaultSort) {
        this.defaultSort = defaultSort;
    }

    void addCustomizer(RepresentationCustomizer<O, I> customizer) {
        this.customizers.add(customizer);
    }

    void setIgnoreUnknownFields(boolean ignoreUnknownFields) {
        this.ignoreUnknownFields = ignoreUnknownFields;
    }

    void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    void setSkipValidate(boolean skipValidate) {
        this.skipValidate = skipValidate;
    }

    public void setSkipValidateMethod(Consumer<I> skipValidateMethod) {
        this.skipValidateMethod = skipValidateMethod;
    }

    void setAlwaysCreate(boolean alwaysCreate) {
        this.alwaysCreate = alwaysCreate;
    }

    void setCreateMethod(Supplier<I> createMethod) {
        this.createMethod = createMethod;
    }

    void setFromMethod(Function<O, I> fromMethod) {
        this.fromMethod = fromMethod;
    }

    void setBuildMethod(Function<I, O> buildMethod) {
        this.buildMethod = buildMethod;
    }

    void setOldValuesMethod(BiConsumer<I, O> oldValuesMethod) {
        this.oldValuesMethod = oldValuesMethod;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Class<O> getOutputClass() {
        return outputClass;
    }

    public Class<I> getInputClass() {
        return inputClass;
    }

    public Map<String, FieldDescriptor<O, I, ?>> getFields() {
        return fields;
    }

    public FieldDescriptor<O, I, ?> getField(String propertyName) {
        return fields.get(propertyName);
    }

    public List<RepresentationCustomizer> getCustomizers() {
        return customizers;
    }

    public boolean ignoreUnknownFields() {
        return ignoreUnknownFields;
    }

    public boolean skipValidate() {
        return skipValidate;
    }

    public boolean alwaysCreate() {
        return alwaysCreate;
    }

    public boolean keepOldValues() {
        return keepOldValues;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public Supplier<I> getCreateMethod() {
        return createMethod;
    }

    public Function<O, I> getFromMethod() {
        return fromMethod;
    }

    public Function<I, O> getBuildMethod() {
        return buildMethod;
    }

    public Consumer<I> getSkipValidateMethod() {
        return skipValidateMethod;
    }

    public BiConsumer<I, O> getOldValuesMethod() {
        return oldValuesMethod;
    }

    public Class<? extends RepresentationProvider> getParent() {
        return parent;
    }

    public List<ConditionalExpecialization> getExpecializations() {
        return expecializations;
    }

    public Map<String, SimpleExpression<?>> getFilterExpressions() {
        return filterExpressions;
    }

    public Sort getDefaultSort() {
        return defaultSort;
    }

    public Map<String, SimpleExpression<?>> getSortExpressions() {
        return sortExpressions;
    }

    public FieldDescriptor<O, I, ?> getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {
        return name;
    }

    public void setExpecializations(List<ConditionalExpecialization> expecializations) {
        this.expecializations = expecializations;
    }

    void addExpecialization(ConditionalExpecialization expecialization) {
        this.expecializations.add(expecialization);
    }
}
