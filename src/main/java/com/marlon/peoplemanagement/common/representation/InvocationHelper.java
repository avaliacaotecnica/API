package com.marlon.peoplemanagement.common.representation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class InvocationHelper {

    private InvocationHelper() {
    }

    public static <T> T invoke(Method method, Object target, Object... args) {
        try {
            return (T) method.invoke(target, args);
        } catch (IllegalAccessException | InvocationTargetException e) {
            if (e.getCause() instanceof RuntimeException) {
                throw (RuntimeException) e.getCause();
            }
            throw new RuntimeException("não foi possível acessar o metodo " + method.getName() +
                    " da classe " + target.getClass().getName(), e);
        }
    }
}
