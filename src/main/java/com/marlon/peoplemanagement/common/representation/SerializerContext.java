package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SerializerContext<C, B> extends Context<SerializerContext, C, B> {

    private Serializer serializer;

    private JsonGenerator generator;

    private C object;

    private SerializerContext(Representation<C, B> representation, Serializer serializer,
                              ObjectMapper objectMapper, JsonGenerator generator,
                              RepresentationCache representationCache) {
        super(objectMapper, representationCache, representation);
        this.serializer = serializer;
        this.generator = generator;
    }

    private SerializerContext(SerializerContext<C, B> context) {
        super(context);
        this.serializer = context.serializer;
        this.generator = context.generator;
        this.object = context.object;
    }

    private SerializerContext(SerializerContext context, Representation<C, B> representation) {
        super(context, representation);
        this.serializer = context.serializer;
        this.generator = context.generator;
    }

    public static <C, B> SerializerContext<C, B> of(Representation<C, B> representation, Serializer serializer,
                                                    ObjectMapper objectMapper, JsonGenerator generator,
                                                    RepresentationCache representationCache) {
        return new SerializerContext<>(representation, serializer, objectMapper, generator, representationCache);
    }

    public SerializerContext<C, B> cloneContext() {
        return new SerializerContext<>(this);
    }

    public SerializerContext<C, B> cloneContext(Class<? extends RepresentationProvider<C, B>> provider) {
        return cloneContext(getRepresentationCache().resolve(provider));
    }

    public SerializerContext<C, B> cloneContext(Representation<C, B> newRepresentation) {
        return new SerializerContext<>(this).representation(newRepresentation);
    }

    public <X, Z> SerializerContext<X, Z> child(X object, Class<? extends RepresentationProvider<X, Z>> provider) {
        return child(object, getRepresentationCache().resolve(provider));
    }

    public <X, Z> SerializerContext<X, Z> child(X object, Representation<X, Z> representation) {
        return new SerializerContext<>(this, representation).with(object);
    }

    public SerializerContext<C, B> with(C object) {
        this.object = object;
        return this;
    }

    public JsonGenerator getGenerator() {
        return generator;
    }

    public C getObject() {
        return object;
    }

    public Serializer getSerializer() {
        return serializer;
    }
}
