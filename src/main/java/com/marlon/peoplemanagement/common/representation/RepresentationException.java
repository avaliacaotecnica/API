package com.marlon.peoplemanagement.common.representation;

public class RepresentationException extends RuntimeException {

    public RepresentationException(String message) {
        super(message);
    }

    public RepresentationException(String message, Throwable cause) {
        super(message, cause);
    }
}
