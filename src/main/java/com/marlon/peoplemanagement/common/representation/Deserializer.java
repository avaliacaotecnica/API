package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marlon.peoplemanagement.common.model.Page;
import com.marlon.peoplemanagement.common.model.Pageable;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.function.BiConsumer;

import static java.util.Objects.nonNull;

public class Deserializer {

    private final ApplicationContext applicationContext;
    private final RepresentationCache representationCache;
    private final ObjectMapper objectMapper;

    public Deserializer(ApplicationContext applicationContext,
                        RepresentationCache representationCache,
                        ObjectMapper objectMapper) {
        this.applicationContext = applicationContext;
        this.representationCache = representationCache;
        this.objectMapper = objectMapper;
    }

    public <C, B> C fromJson(DeserializerContext<C, B> context) {

        JsonNode node = context.getNode();

        if (node == null || node.isNull()) {
            return null;
        }

        if (!node.isObject()) {
            throw new IllegalArgumentException("O json fornecido não contém um objeto válido");
        }

        representationCache.resolveExpecialization(context.getRepresentation(), node)
                .ifPresent(context::representation);

        Representation<C, B> representation = context.getRepresentation();

        if (context.isCopyOnly() && !representation.isReadOnly()) {
            context.destination(createNewInstance(representation.getOutputClass()));
        }

        if (!context.hasDestination() && !representation.alwaysCreate()) {
            context.getLocator().locate(context.getRepresentation(), context.getNode(), context.getObjectMapper())
                    .ifPresent(context::destination);
        }

        if (representation.isReadOnly()) {
            return context.getDestination();
        }

        B input;
        if (context.hasDestination() && representation.getFromMethod() != null) {
            input = representation.getFromMethod().apply(context.getDestination());
        } else {
            input = representation.getCreateMethod().get();
        }

        context.input(input);

        applicationContext.publishEvent(new BeforeDeserializeEvent(context));

        readFields(context);

        representation.getCustomizers()
                .forEach(customizer -> customizer.onDeserialize(context));

        if (representation.getSkipValidateMethod() != null &&
                (representation.skipValidate() || context.isCopyOnly())) {
            representation.getSkipValidateMethod().accept(input);
        }

        return representation.getBuildMethod().apply(input);
    }


    public <C, B> C fromJson(Class<? extends RepresentationProvider<C, B>> configuration, String json,
                             ObjectLocator locator) throws IOException {
        return fromJson(representationCache.resolve(configuration), json, locator);
    }

    public <C, B> C fromJson(Representation<C, B> configuration, String json,
                             ObjectLocator locator) throws IOException {
        final JsonNode node = objectMapper.getFactory().createParser(json).readValueAsTree();
        return fromJson(configuration, node, locator);
    }
    public <C, B> C fromJson(Representation<C, B> representation, JsonNode node, ObjectLocator locator) {
        return fromJson(of(representation, this, objectMapper, locator, representationCache)
                .with(node));
    }

    public <C, B> C fromJson(Representation<C, B> representation, C destination, JsonNode node, ObjectLocator locator) {
        return fromJson(of(representation, this, objectMapper, locator, representationCache)
                .with(node, destination));
    }

    public <C, B> C fromJson(Representation<C, B> configuration, C destination, String json,
                             ObjectLocator locator) throws IOException {
        final JsonNode node = objectMapper.getFactory().createParser(json).readValueAsTree();

        return fromJson(of(configuration, this, objectMapper, locator, representationCache)
                .with(node, destination));

    }

    public static <C, B> DeserializerContext<C, B> of(Representation<C, B> representation, Deserializer deserializer,
                                                      ObjectMapper objectMapper, ObjectLocator objectLocator,
                                                      RepresentationCache representationCache) {
        return new DeserializerContext<>(representation, deserializer, objectMapper, objectLocator, representationCache);
    }


    private <T> T createNewInstance(Class<T> clazz) {
        try {
            Constructor<T> constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            return constructor.newInstance();
        } catch (InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
            throw new IllegalArgumentException("falha ao criar uma nova instancia da classe " + clazz.getName(), e);
        }
    }

    public void readFields(DeserializerContext context) {

        JsonNode node = context.getNode();
        Representation representation = context.getRepresentation();

        node.fields().forEachRemaining(entry -> {
            String propertyName = entry.getKey();

            FieldDescriptor field = getField(propertyName, representation);
            if (field == null) {
                if (representation.ignoreUnknownFields()) {
                    return;
                }

                throw new IllegalArgumentException("campo " + propertyName +
                        " não foi encontrado na configuração da classe " + representation.getOutputClass());
            }

            if (field.isReadOnly()) {
                return;
            }

            readField(field, entry.getValue(), context);
        });
    }

    private FieldDescriptor getField(String propertyName, Representation representation) {
        FieldDescriptor field = representation.getField(propertyName);

        if (field == null) {
            Representation parent = representationCache.resolve(representation.getParent());
            if (parent != null) {
                field = parent.getField(propertyName);
            }
        }

        return field;
    }

    private void readField(FieldDescriptor field, JsonNode node, DeserializerContext context) {
        Object value;
        if (field.isCollection()) {
            value = readCollectionField(field, node, context);
        } else if (field.isMap()) {
            value = readMapField(field, node, context);
        } else {
            value = readSimpleField(field, node, context);
        }

        if (field.isIdentifier() && context.isCopyOnly() && context.hasDestination()) {
            try {
                FieldUtils.writeField(context.getDestination(), field.getName(), value, true);
            } catch (IllegalAccessException e) {
                throw new RepresentationException("falha ao setar o id durante a cópia do objeto " +
                        context.getRepresentation().getOutputClass().getName(), e);
            }
        }

        BiConsumer setterMethod = field.getSetterMethod();
        if (nonNull(setterMethod)) {
            setterMethod.accept(context.getInput(), value);
        }
    }

    private Object readMapField(FieldDescriptor field, JsonNode node, DeserializerContext context) {
        if (node.isNull()) {
            return Collections.emptyMap();
        }

        if (!node.isObject()) {
            throw new IllegalArgumentException("o campo " + field + " não é um mapa válido");
        }

        Map map = field.createMap();
        node.fields().forEachRemaining(entry -> {
            Object key = objectMapper.convertValue(entry.getKey(), field.getMapKeyType());
            Object value = readSimpleField(field, entry.getValue(), context);

            map.put(key, value);
        });

        return map;
    }

    private Object readCollectionField(FieldDescriptor field, JsonNode node, DeserializerContext context) {
        if (node.isNull()) {
            return field.createEmptyCollection();
        }

        if (!node.isArray()) {
            throw new IllegalArgumentException("o campo " + field + " não é uma coleção válida");
        }

        Collection collection = field.createCollection();
        node.elements().forEachRemaining(current -> {
            if (!current.isNull()) {
                Object value = readSimpleField(field, current, context);
                if (value != null) {
                    collection.add(value);
                }
            }
        });

        return collection;
    }


    private Object readSimpleField(FieldDescriptor field, JsonNode node, DeserializerContext context) {
        try {
            if (field.getConfiguration() != null) {
                return fromJson(context.child(field.getConfiguration(), node));
            }

            if (field.getConfigurationProvider() != null) {
                return fromJson(context.child(representationCache.resolve(field.getConfigurationProvider()), node));
            }

            Object value = node.traverse(objectMapper).readValueAs(field.getOutputType());

            if (field.hasConverter()) {
                return field.getConverter().fromRepresentation(value, context);
            }

            return value;
        } catch (DateTimeParseException | IOException e) {
            throw new IllegalArgumentException("não foi possível ler o valor do campo " + field, e);
        }
    }

    public <C, B> C objectFromStream(Representation<C, B> configuration, C destination, InputStream inputStream,
                                     ObjectLocator locator) throws IOException {
        final JsonNode node = objectMapper.getFactory().createParser(inputStream).readValueAsTree();

        return fromJson(of(configuration, this, objectMapper, locator, representationCache)
                .with(node, destination));
    }

    public <C, B> C objectFromStream(Representation<C, B> configuration, InputStream inputStream,
                                     ObjectLocator locator) throws IOException {
        final JsonNode node = objectMapper.getFactory().createParser(inputStream).readValueAsTree();
        return fromJson(configuration, node, locator);
    }

    public <C, B> Collection<C> collectionFromJson(Representation<C, B> configuration,
                                                   JsonNode node, ObjectLocator locator) {

        if (node.isNull()) {
            return Collections.emptySet();
        }

        if (!node.isArray()) {
            throw new IllegalArgumentException("o json fornecido não contém uma coleção válida");
        }

        List<C> collection = new ArrayList<>();

        node.elements().forEachRemaining(element ->
                collection.add(fromJson(configuration, element, locator)));

        return collection;
    }

    public <C, B> Collection<C> collectionFromJson(Representation<C, B> configuration,
                                                   String json, ObjectLocator locator) throws IOException {

        final JsonNode node = objectMapper.getFactory().createParser(json).readValueAsTree();
        return collectionFromJson(configuration, node, locator);
    }

    public <C, B> Collection<C> collectionFromStream(Representation<C, B> configuration,
                                                     InputStream inputStream, ObjectLocator locator) throws IOException {

        final JsonNode node = objectMapper.getFactory().createParser(inputStream).readValueAsTree();
        return collectionFromJson(configuration, node, locator);
    }

    public <C, B> Page<C> pageFromJson(Representation<C, B> configuration, String json, ObjectLocator locator) throws IOException {
        final JsonNode node = objectMapper.getFactory().createParser(json).readValueAsTree();
        return pageFromJson(configuration, node, locator);
    }

    public <C, B> Page<C> pageFromJson(Representation<C, B> configuration, JsonNode node, ObjectLocator locator) {

        if (node.isNull()) {
            return Page.empty();
        }

        if (!node.isObject()) {
            throw new IllegalArgumentException("o json fornecido não contém uma página válida");
        }

        List<C> collection = new ArrayList<>();

        node.get("content").elements().forEachRemaining(element ->
                collection.add(fromJson(configuration, element, locator)));

        return new Page<>(collection, Pageable.of(node.get("limit").asInt(),
                node.get("hasNext").asInt()), node.get("total").asInt());
    }

    public <C, B> C copyFromJson(Representation<C, B> representation, String json,
                                 ObjectLocator objectLocator) throws IOException {
        JsonNode node = objectMapper.getFactory().createParser(json).readValueAsTree();
        return fromJson(of(representation, this, objectMapper, objectLocator, representationCache)
                .with(node).copyOnly(true));
    }
}
