package com.marlon.peoplemanagement.common.representation;

import org.springframework.context.ApplicationEvent;

public class BeforeDeserializeEvent extends ApplicationEvent {

    private final DeserializerContext context;

    BeforeDeserializeEvent(DeserializerContext context) {
        super(context);
        this.context = context;
    }

    public DeserializerContext getContext() {
        return context;
    }
}
