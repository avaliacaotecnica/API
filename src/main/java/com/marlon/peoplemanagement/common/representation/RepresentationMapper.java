package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.JsonNode;
import com.marlon.peoplemanagement.common.querydsl.ExpressionOperationResolver;
import com.marlon.peoplemanagement.common.model.Page;
import com.marlon.peoplemanagement.common.model.Sort;
import com.marlon.peoplemanagement.common.querydsl.QueryDslExpressionEvaluator;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserConfig;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.SimpleExpression;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

public class RepresentationMapper {

    private static final String MSG_FALHA_AO_SERIALIZAR_O_OBJETO = "falha ao serializar o objeto";
    private static final String MSG_FALHA_AO_SERIALIZAR_A_PAGINA = "falha ao serializar a página";
    private static final String MSG_FALHA_AO_SERIALIZAR_A_COLECAO = "falha ao serializar a coleção";
    private static final String MSG_FALHA_AO_DESERIALIZAR_O_OBJETO = "falha ao deserializar o objeto";
    private static final String MSG_FALHA_AO_DESERIALIZAR_A_COLECAO = "falha ao deserializar a coleção";

    private final RepresentationCache representationCache;

    private final Serializer serializer;

    private final Deserializer deserializer;

    private final ExpressionOperationResolver expressionOperationResolver;

    private final FilterExpressionParserConfig filterExpressionParserConfig;

    private final ObjectLocator objectLocator;


    public RepresentationMapper(RepresentationCache representationCache, Serializer serializer,
                                Deserializer deserializer, ExpressionOperationResolver expressionOperationResolver,
                                FilterExpressionParserConfig filterExpressionParserConfig, ObjectLocator objectLocator) {
        this.representationCache = representationCache;
        this.serializer = serializer;
        this.deserializer = deserializer;
        this.expressionOperationResolver = expressionOperationResolver;
        this.filterExpressionParserConfig = filterExpressionParserConfig;
        this.objectLocator = objectLocator;
    }

    public <C, B> void objectToStream(Class<? extends RepresentationProvider<C, B>> provider, C object, OutputStream outputStream) {
        try {
            Representation<C, B> representation = representationCache.resolve(provider);
            serializer.toStream(representation, object, outputStream);
        } catch (IOException e) {
            throw new RepresentationException(MSG_FALHA_AO_SERIALIZAR_O_OBJETO, e);
        }
    }

    public <C, B> void pageToStream(Class<? extends RepresentationProvider<C, B>> provider, Page<C> page, OutputStream outputStream) {
        try {
            Representation<C, B> representation = representationCache.resolve(provider);
            serializer.toStream(representation, page, outputStream);
        } catch (IOException e) {
            throw new RepresentationException(MSG_FALHA_AO_SERIALIZAR_A_PAGINA, e);
        }
    }

    public <C, B> void collectionToStream(Class<? extends RepresentationProvider<C, B>> provider, Collection<C> collection,
                                          OutputStream outputStream) {
        try {
            Representation<C, B> representation = representationCache.resolve(provider);
            serializer.toStream(representation, collection, outputStream);
        } catch (IOException e) {
            throw new RepresentationException(MSG_FALHA_AO_SERIALIZAR_A_COLECAO, e);
        }
    }

    public <C, B> BooleanBuilder parseFilter(Class<? extends RepresentationProvider<C, B>> provider, String filter) {
        if(Objects.isNull(provider)){
            return null;
        }
        Representation<C, B> representation = representationCache.resolve(provider);
        return parseFilter(filter, representation.getFilterExpressions());
    }

    public BooleanBuilder parseFilter(String filter, Map<String, SimpleExpression<?>> expressions) {
        QueryDslExpressionEvaluator evaluator = QueryDslExpressionEvaluator.builder(expressions)
                .expressionOperationResolver(expressionOperationResolver)
                .filterExpressionParserConfig(filterExpressionParserConfig)
                .build();

        final Iterable<Predicate> predicates = evaluator.filter(filter).getPredicates();

        final Iterator<Predicate> predicateIt = predicates.iterator();
        final Predicate predicate = predicateIt.hasNext() ? predicateIt.next() : null;

        return predicate != null ? new BooleanBuilder().and(predicate) : new BooleanBuilder();
    }

    public <C, B> Sort parseSort(Class<? extends RepresentationProvider<C, B>> provider, final String sort, Sort defaultSort) {
        Representation<C, B> representation = representationCache.resolve(provider);
        return parseSort(sort, defaultSort != null ? defaultSort : representation.getDefaultSort(),
                representation.getSortExpressions());
    }

    public Sort parseSort(final String sort, final Sort defaultSort, Map<String, SimpleExpression<?>> expressions) {
        final Iterable<OrderSpecifier<?>> orders = QueryDslExpressionEvaluator.builder(expressions).build()
                .sort(sort)
                .getOrders();

        if (!orders.iterator().hasNext()) {
            return defaultSort;
        }

        return Sort.of(orders);
    }


    public <C, B> Collection<C> collectionFromStream(Class<? extends RepresentationProvider<C, B>> provider, InputStream json) {
        try {
            Representation<C, B> representation = representationCache.resolve(provider);
            return deserializer.collectionFromStream(representation, json, objectLocator);
        } catch (IOException e) {
            throw new RepresentationException(MSG_FALHA_AO_DESERIALIZAR_A_COLECAO, e);
        }
    }

    public <C, B> C objectFromStream(Class<? extends RepresentationProvider<C, B>> provider, InputStream json) {
        try {
            Representation<C, B> representation = representationCache.resolve(provider);
            return deserializer.objectFromStream(representation, json, objectLocator);
        } catch (IOException e) {
            throw new RepresentationException(MSG_FALHA_AO_DESERIALIZAR_O_OBJETO, e);
        }
    }

    public <C, B> String objectToJson(Class<? extends RepresentationProvider<C, B>> provider, C object) {
        return serializer.toJson(representationCache.resolve(provider), object);
    }

    public <C, B> C objectFromJson(Class<? extends RepresentationProvider<C, B>> provider, JsonNode json) {
        Representation<C, B> representation = representationCache.resolve(provider);
        return deserializer.fromJson(representation, json, objectLocator);
    }

    public <C, B> C objectFromJson(Class<? extends RepresentationProvider<C, B>> provider, String json) {
        try {
            Representation<C, B> representation = representationCache.resolve(provider);
            return deserializer.fromJson(representation, json, objectLocator);
        } catch (IOException e) {
            throw new RepresentationException(MSG_FALHA_AO_DESERIALIZAR_O_OBJETO, e);
        }
    }
}
