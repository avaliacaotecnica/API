package com.marlon.peoplemanagement.common.representation;

import com.querydsl.core.types.dsl.SimpleExpression;

public class Expression {
    private final String attribute;
    private String description;
    private final SimpleExpression<?> expressionValue;
    private boolean filter;
    private boolean sort;
    private boolean field;

    public Expression(String attribute, SimpleExpression<?> expressionValue) {
        this.attribute = attribute;
        this.expressionValue = expressionValue;
    }

    public String getAttribute() {
        return attribute;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SimpleExpression<?> getExpressionValue() {
        return expressionValue;
    }

    public boolean isFilter() {
        return filter;
    }

    public void setFilter(boolean filter) {
        this.filter = filter;
    }

    public boolean isSort() {
        return sort;
    }

    public void setSort(boolean sort) {
        this.sort = sort;
    }

    public boolean isField() {
        return field;
    }

    public void setField(boolean field) {
        this.field = field;
    }
}

