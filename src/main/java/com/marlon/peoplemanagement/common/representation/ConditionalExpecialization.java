package com.marlon.peoplemanagement.common.representation;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.function.Function;

public class ConditionalExpecialization {

    private final Class<? extends RepresentationProvider> representationClass;
    private final Function<JsonNode, Boolean> applyJson;
    private final Function<Object, Boolean> applyEntity;

    public ConditionalExpecialization(Class<? extends RepresentationProvider> representationClass,
                                      Function<JsonNode, Boolean> applyJson,
                                      Function<Object, Boolean> applyEntity) {

        this.representationClass = representationClass;
        this.applyJson = applyJson;
        this.applyEntity = applyEntity;
    }


    public Class<? extends RepresentationProvider> getRepresentationClass() {
        return representationClass;
    }

    public Function<JsonNode, Boolean> getApplyJson() {
        return applyJson;
    }

    public Function<Object, Boolean> getApplyEntity() {
        return applyEntity;
    }

    public static ConditionalExpecialization of(Class<? extends RepresentationProvider> representationClass,
                                                Function<JsonNode, Boolean> resolveFromJson,
                                                Function<Object, Boolean> resolveFromEntity) {
        return new ConditionalExpecialization(representationClass, resolveFromJson, resolveFromEntity);
    }

    public static Function<JsonNode, Boolean> jsonAttribute(String attribute, String value) {
        return node -> node.get(attribute).asText("").equals(value);
    }
}
