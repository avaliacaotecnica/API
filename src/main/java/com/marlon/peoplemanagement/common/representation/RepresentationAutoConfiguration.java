package com.marlon.peoplemanagement.common.representation;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.marlon.peoplemanagement.common.querydsl.ExpressionOperationResolver;
import com.marlon.peoplemanagement.common.querydsl.KeepOldValuesEventListener;
import com.marlon.peoplemanagement.repositorys.BasicRepository;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserConfig;
import com.marlon.peoplemanagement.common.querydsl.config.FilterExpressionParserDefaultConfig;
import com.marlon.peoplemanagement.common.querydsl.config.expression.CustomExpressionOperationResolver;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigureAfter({JacksonAutoConfiguration.class})
public class RepresentationAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public RepresentationProperties representationProperties() {
        return new RepresentationProperties();
    }

    @Bean
    @ConditionalOnMissingBean
    public RepresentationCache representationCache() {
        return new RepresentationCache();
    }

    @Bean
    @ConditionalOnMissingBean
    public ObjectInitializer objectInitializer() {
        return new ObjectInitializer();
    }

    @Bean
    @ConditionalOnMissingBean
    public Deserializer deserializer(ApplicationContext applicationContext,
                                     RepresentationCache representationCache,
                                     ObjectMapper objectMapper) {
        return new Deserializer(applicationContext, representationCache, objectMapper);
    }

    @Bean
    @ConditionalOnMissingBean
    public Serializer serializer(RepresentationCache representationCache,
                                 ObjectMapper objectMapper,
                                 ObjectInitializer objectInitializer,
                                 RepresentationProperties configuration) {
        return new Serializer(representationCache, objectMapper, objectInitializer,
                configuration);
    }

    @Bean
    @ConditionalOnMissingBean
    public ExpressionOperationResolver expressionOperationResolver() {
        return new CustomExpressionOperationResolver();
    }


    @Bean
    @ConditionalOnMissingBean
    public FilterExpressionParserConfig producesFilterExpressionParserConfig() {
        return FilterExpressionParserDefaultConfig.builder()
                .filterEncoded(true)
                .build();
    }

    @Bean
    @ConditionalOnMissingBean
    public ObjectLocator objectLocator(BasicRepository repository) {
        return new JpaObjectLocator(repository);
    }

    @Bean
    @ConditionalOnMissingBean
    public RepresentationMapper representationMapper(RepresentationCache representationCache,
                                                     Serializer serializer,
                                                     Deserializer deserializer,
                                                     ExpressionOperationResolver expressionOperationResolver,
                                                     FilterExpressionParserConfig filterExpressionParserConfig,
                                                     ObjectLocator objectLocator) {
        return new RepresentationMapper(representationCache, serializer, deserializer,
                expressionOperationResolver, filterExpressionParserConfig, objectLocator);
    }

    @Bean
    @ConditionalOnMissingBean
    public KeepOldValuesEventListener keepOldValuesEventListener(Serializer serializer) {
        return new KeepOldValuesEventListener(serializer);
    }


}
