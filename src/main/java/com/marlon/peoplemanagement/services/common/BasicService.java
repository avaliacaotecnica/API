package com.marlon.peoplemanagement.services.common;

import com.marlon.peoplemanagement.common.model.IEntity;
import com.marlon.peoplemanagement.common.model.Page;
import com.marlon.peoplemanagement.common.model.Pageable;

public interface BasicService<E, ID> {

        Page<E> getPage(Pageable pageable, Object filters);

        E getById(ID id);

        public <E extends IEntity> E save(E entity);

        public <E extends IEntity> void remove(E entity);

        public <E extends IEntity> void remove(ID id);
}
