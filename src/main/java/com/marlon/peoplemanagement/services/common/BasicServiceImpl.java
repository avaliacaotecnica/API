package com.marlon.peoplemanagement.services.common;

import com.marlon.peoplemanagement.common.model.IEntity;
import com.marlon.peoplemanagement.repositorys.BasicRepository;
import com.marlon.peoplemanagement.common.model.Page;
import com.marlon.peoplemanagement.common.model.Pageable;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import java.lang.reflect.ParameterizedType;


public class BasicServiceImpl<E extends IEntity, ID> implements BasicService<E, ID> {

    private Class<E> classe;

    BasicRepository basicRepository;

    public BasicServiceImpl(BasicRepository basicRepository) {

        classe = (Class<E>)((ParameterizedType)getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
        this.basicRepository = basicRepository;
    }

    public Page<E> getPage(Pageable pageable, Object filters) {
        BooleanBuilder predicate = new BooleanBuilder();
        if(filters instanceof Predicate) {
            predicate.and((Predicate) filters);
        }
        return this.basicRepository.findAll(classe, pageable, predicate);
    }

    @Override
    public E getById(ID id) {
        return this.basicRepository.find(classe, id);
    }

    public <E extends IEntity> E save(E entity) {
        return this.basicRepository.save(entity);
    }

    public <E extends IEntity> void remove(E entity) {
        this.basicRepository.remove(entity);
    }

    public <E extends IEntity> void remove(ID id) {
        this.basicRepository.remove(classe, id);
    }
}
