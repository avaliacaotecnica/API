package com.marlon.peoplemanagement.services.contatos;

import com.marlon.peoplemanagement.model.Contato;
import com.marlon.peoplemanagement.services.common.BasicService;

public interface ContatoService extends BasicService<Contato, Long> {
}
