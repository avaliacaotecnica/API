package com.marlon.peoplemanagement.services.contatos;

import com.marlon.peoplemanagement.model.Contato;
import com.marlon.peoplemanagement.repositorys.BasicRepository;
import com.marlon.peoplemanagement.services.common.BasicServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class ContatoServiceImpl extends BasicServiceImpl<Contato, Long> implements ContatoService {

    public ContatoServiceImpl(BasicRepository basicRepository) {
        super(basicRepository);
    }
}
