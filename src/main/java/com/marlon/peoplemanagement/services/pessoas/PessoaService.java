package com.marlon.peoplemanagement.services.pessoas;

import com.marlon.peoplemanagement.model.Pessoa;
import com.marlon.peoplemanagement.services.common.BasicService;

public interface PessoaService extends BasicService<Pessoa, Long> {

    Pessoa save(Pessoa entity);
}
