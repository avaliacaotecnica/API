package com.marlon.peoplemanagement.services.pessoas;

import com.marlon.peoplemanagement.model.Contato;
import com.marlon.peoplemanagement.model.Pessoa;
import com.marlon.peoplemanagement.repositorys.BasicRepository;
import com.marlon.peoplemanagement.services.common.BasicServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Set;

@Service
public class PessoaServiceImpl extends BasicServiceImpl<Pessoa, Long> implements PessoaService {

    public PessoaServiceImpl(BasicRepository basicRepository) {
        super(basicRepository);
    }

    public Pessoa save(Pessoa entity) {
        Set<Contato> contatos = entity.getContatos();
        if(Objects.isNull(entity.getId())){
            entity.setContatos(null);
            super.save(entity);
        }
       contatos.stream().forEach(c->c.setPessoa(entity));
       entity.setContatos(contatos);
       return super.save(entity);
    }
}
