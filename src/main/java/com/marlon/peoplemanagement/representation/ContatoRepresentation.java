package com.marlon.peoplemanagement.representation;

import com.marlon.peoplemanagement.model.Contato;
import com.marlon.peoplemanagement.model.QContato;
import com.marlon.peoplemanagement.common.representation.Representation;
import com.marlon.peoplemanagement.common.representation.RepresentationBuilder;
import com.marlon.peoplemanagement.common.representation.RepresentationProvider;


public interface ContatoRepresentation {

    final class Completa implements RepresentationProvider<Contato, Contato.Builder> {
        @Override
        public Representation<Contato, Contato.Builder> getRepresentation() {
            QContato conatato = QContato.contato;
            return RepresentationBuilder.create(Contato.class, Contato.Builder.class)
                    .name("Contato")
                    .field(conatato.id).add()
                    .field(conatato.nome).add()
                    .field(conatato.email).add()
                    .field(conatato.telefone).add()
                    .field(conatato.pessoa).configuration(PessoaRepresentation.Minimal.class).add()
                    .expression("id", conatato.id)
                    .expression("nome", conatato.nome)
                    .expression("email", conatato.email)
                    .expression("telefone", conatato.telefone)
                    .expression("pessoa.id", conatato.pessoa.id)
                    .expression("pessoa.nome", conatato.pessoa.nome)
                    .build();
        }
    }

    final class Minimal implements RepresentationProvider<Contato, Contato.Builder> {
        @Override
        public Representation<Contato, Contato.Builder> getRepresentation() {
            QContato conatato = QContato.contato;
            return RepresentationBuilder.create(Contato.class, Contato.Builder.class)
                    .name("Contato")
                    .field(conatato.id).add()
                    .field(conatato.nome).add()
                    .field(conatato.email).add()
                    .field(conatato.telefone).add()
                    .expression("id", conatato.id)
                    .expression("nome", conatato.nome)
                    .expression("email", conatato.email)
                    .expression("pessoa.id", conatato.pessoa.id)
                    .expression("pessoa.nome", conatato.pessoa.nome)
                    .build();
        }
    }
}
