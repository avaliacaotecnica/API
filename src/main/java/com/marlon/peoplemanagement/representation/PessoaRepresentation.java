package com.marlon.peoplemanagement.representation;

import com.marlon.peoplemanagement.common.representation.Representation;
import com.marlon.peoplemanagement.common.representation.RepresentationBuilder;
import com.marlon.peoplemanagement.common.representation.RepresentationProvider;
import com.marlon.peoplemanagement.model.Pessoa;
import com.marlon.peoplemanagement.model.QPessoa;


public interface PessoaRepresentation {

    final class Completa implements RepresentationProvider<Pessoa, Pessoa.Builder> {
        @Override
        public Representation<Pessoa, Pessoa.Builder> getRepresentation() {
            QPessoa pessoa = QPessoa.pessoa;
            return RepresentationBuilder.create(Pessoa.class, Pessoa.Builder.class)
                    .name("Pessoa")
                    .identifier(pessoa.id).add()
                    .field(pessoa.nome).add()
                    .field(pessoa.cpf).add()
                    .field(pessoa.dataNascimento).add()
                    .collection(pessoa.contatos).configuration(ContatoRepresentation.Minimal.class).add()
                    .expression("id", pessoa.id)
                    .expression("nome", pessoa.nome)
                    .expression("cpf", pessoa.cpf)
                    .expression("dataNascimento", pessoa.dataNascimento)
                    .expression("contatos.id", pessoa.contatos.any().id)
                    .expression("contatos.nome", pessoa.contatos.any().nome)
                    .build();
        }
    }

    final class Minimal implements RepresentationProvider<Pessoa, Pessoa.Builder> {
        @Override
        public Representation<Pessoa, Pessoa.Builder> getRepresentation() {
            QPessoa pessoa = QPessoa.pessoa;
            return RepresentationBuilder.create(Pessoa.class, Pessoa.Builder.class)
                    .name("Pessoa")
                    .identifier(pessoa.id).add()
                    .field(pessoa.nome).add()
                    .build();
        }
    }
}
