package com.marlon.peoplemanagement.model;

import com.marlon.peoplemanagement.common.model.PeopleManagementEntity;
import com.marlon.peoplemanagement.common.model.builder.EntityBuilder;
import com.marlon.peoplemanagement.common.model.validation.BasicSpecification;
import com.marlon.peoplemanagement.common.model.validation.Specifications;
import com.marlon.peoplemanagement.common.model.validation.Validator;
import com.marlon.peoplemanagement.common.util.CPFValidator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "PESSOA")
public class Pessoa extends PeopleManagementEntity<Long> {

    @Column(name = "NOME", length = 150)
    @NotNull(message = "O campo de nome deve ser informado")
    private String nome;

    @Column(name = "CPF", length = 11)
    @Pattern(regexp = "([0-9]{11})", message = "O telefone deve possuir tamanho de 11 caracteres")
    @NotNull(message = "O campo de cpf deve ser informado")
    private String cpf;

    @Column(name = "DT_NASCIMENTO", length = 60)
    @NotNull(message = "O campo da data de nascimento deve ser informado")
    private LocalDate dataNascimento;

    @OneToMany(mappedBy = "pessoa", cascade = {CascadeType.ALL}, orphanRemoval = true)
    @NotEmpty(message = "Ao menos uma contato deve ser vinculado.")
    private Set<Contato> contatos;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Set<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(Set<Contato> contatos) {
        this.contatos = contatos;
    }

    public static class Builder extends EntityBuilder<Pessoa> {

        protected Builder(Pessoa entity, EntityState state) {
            super(entity, state);
        }

        public static Builder create() {
            return new Builder(new Pessoa(), EntityState.NEW);
        }

        public static Builder from(Pessoa entity) {
            return new Builder(entity, EntityState.BUILT);
        }

        public Builder id(Long id) {
            entity.setId(id);
            return this;
        }

        public Builder nome(String nome) {
            entity.setNome(nome);
            return this;
        }

        public Builder cpf(String cpf) {
            entity.setCpf(cpf);
            return this;
        }

        public Builder dataNascimento(LocalDate dataNascimento) {
            entity.setDataNascimento(dataNascimento);
            return this;
        }

        public Builder contatos(Set<Contato> contatos) {
            entity.setContatos(contatos);
            return this;
        }

        @Override
        protected void validate(Validator validator) {
            super.validate(validator);
            validator.validate(entity, Specification.cpfInvalido());
            validator.validate(entity, Specification.dataNascimentoMaiorQueAtual());
        }
    }
    public static final class Specification {

        private Specification() {
        }

        static BasicSpecification<Pessoa> cpfInvalido() {
            return Specifications.basic("O cpf informado é invalido",
                    (entity, spec) -> CPFValidator.validaCPF(entity.getCpf()));
        }

        static BasicSpecification<Pessoa> dataNascimentoMaiorQueAtual() {
            return Specifications.basic("A data de nascimento não pode ser maior que a atual",
                    (entity, spec) -> (LocalDate.now().isAfter(entity.getDataNascimento())));
        }
    }
}
