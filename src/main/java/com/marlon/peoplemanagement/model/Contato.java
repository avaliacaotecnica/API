package com.marlon.peoplemanagement.model;

import com.marlon.peoplemanagement.common.model.PeopleManagementEntity;
import com.marlon.peoplemanagement.common.model.builder.EntityBuilder;
import com.marlon.peoplemanagement.common.model.validation.Validator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "CONTATO")
public class Contato extends PeopleManagementEntity<Long> {

    @Column(name = "NOME", length = 150)
    @NotNull(message = "O campo de nome deve ser informado")
    private String nome;

    @Column(name = "TELEFONE", length = 11)
    @Pattern(regexp = "([0-9]{10})|([0-9]{11})", message = "O telefone deve possuir tamanho entre 10 a 11 caracteres")
    @NotNull(message = "O campo de telefone deve ser informado")
    private String telefone;

    @Column(name = "EMAIL", length = 60)
    @NotNull(message = "O campo de e-mail deve ser informado")
    @Pattern(regexp = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$",
            message = "Formato de e-mail incorreto")
    private String email;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="I_PESSOAS", referencedColumnName = "ID")
    private Pessoa pessoa;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public static class Builder extends EntityBuilder<Contato> {

        protected Builder(Contato entity, EntityState state) {
            super(entity, state);
        }

        public static Builder create() {
            return new Builder(new Contato(), EntityState.NEW);
        }

        public static Builder from(Contato entity) {
            return new Builder(entity, EntityState.BUILT);
        }

        public Builder id(Long id) {
            entity.setId(id);
            return this;
        }

        public Builder nome(String nome) {
            entity.setNome(nome);
            return this;
        }

        public Builder telefone(String telefone) {
            entity.setTelefone(telefone);
            return this;
        }

        public Builder email(String email) {
            entity.setEmail(email);
            return this;
        }

        public Builder pessoa(Pessoa pessoa) {
            entity.setPessoa(pessoa);
            return this;
        }
    }
}
