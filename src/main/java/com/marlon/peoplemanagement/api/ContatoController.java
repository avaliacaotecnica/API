package com.marlon.peoplemanagement.api;

import com.marlon.peoplemanagement.common.Constants;
import com.marlon.peoplemanagement.model.Contato;
import com.marlon.peoplemanagement.common.model.Page;
import com.marlon.peoplemanagement.common.model.Pageable;
import com.marlon.peoplemanagement.common.model.Sort;
import com.marlon.peoplemanagement.representation.ContatoRepresentation;
import com.marlon.peoplemanagement.common.mvc.Representation;
import com.marlon.peoplemanagement.common.mvc.RepresentationFilter;
import com.marlon.peoplemanagement.common.mvc.RepresentationSort;
import com.marlon.peoplemanagement.services.contatos.ContatoService;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@RestController
@Transactional(propagation = SUPPORTS)
@RequestMapping("contatos")
public class ContatoController {

    private final ContatoService contatoService;

    ContatoController(ContatoService contatoService){
        this.contatoService = contatoService;
    }

    @GetMapping
    @Representation(ContatoRepresentation.Completa.class)
    public Page<Contato> getPage(@RequestParam(value = "offset", required = false, defaultValue = Constants.DEFAULT_OFFSET_SIZE) int offset,
                                 @RequestParam(value = "limit", required = false, defaultValue = Constants.DEFAULT_LIMIT_SIZE) int limit,
                                 @RepresentationFilter Object filter,
                                 @RepresentationSort Sort sort) {

        return contatoService.getPage(Pageable.of(offset, limit, sort), filter);
    }


    @GetMapping("{id}")
    @Representation(ContatoRepresentation.Completa.class)
    public Contato getById(@PathVariable("id") Long id) {
        return contatoService.getById(id);
    }


    @PostMapping
    @Transactional
    @Representation(ContatoRepresentation.Completa.class)
    public Contato create(@RequestBody Contato entity) {
        return contatoService.save(entity);
    }

    @Transactional
    @PutMapping("{id}")
    @Representation(ContatoRepresentation.Completa.class)
    public Contato update(@PathVariable("id") Long id, @Valid @RequestBody Contato contato) {
        contato.setId(id);
        return contatoService.save(contato);
    }

    @Transactional
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        contatoService.remove(id);
        return ResponseEntity.noContent().build();
    }
}
