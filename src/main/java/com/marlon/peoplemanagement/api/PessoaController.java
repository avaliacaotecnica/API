package com.marlon.peoplemanagement.api;

import com.marlon.peoplemanagement.common.Constants;
import com.marlon.peoplemanagement.common.model.Page;
import com.marlon.peoplemanagement.common.model.Pageable;
import com.marlon.peoplemanagement.common.model.Sort;
import com.marlon.peoplemanagement.common.mvc.Representation;
import com.marlon.peoplemanagement.common.mvc.RepresentationFilter;
import com.marlon.peoplemanagement.common.mvc.RepresentationSort;
import com.marlon.peoplemanagement.model.Pessoa;
import com.marlon.peoplemanagement.representation.PessoaRepresentation;
import com.marlon.peoplemanagement.services.pessoas.PessoaService;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.transaction.annotation.Propagation.SUPPORTS;

@RestController
@Transactional(propagation = SUPPORTS)
@RequestMapping("pessoas")
public class PessoaController {

    private final PessoaService pessoaService;

    PessoaController(PessoaService pessoaService){
        this.pessoaService = pessoaService;
    }

    @GetMapping
    @Representation(PessoaRepresentation.Completa.class)
    public Page<Pessoa> getPage(@RequestParam(value = "offset", required = false, defaultValue = Constants.DEFAULT_OFFSET_SIZE) int offset,
                                 @RequestParam(value = "limit", required = false, defaultValue = Constants.DEFAULT_LIMIT_SIZE) int limit,
                                 @RepresentationFilter Object filter,
                                 @RepresentationSort Sort sort) {

        return pessoaService.getPage(Pageable.of(offset, limit, sort), filter);
    }


    @GetMapping("{id}")
    @Representation(PessoaRepresentation.Completa.class)
    public Pessoa getById(@PathVariable("id") Long id) {
        return pessoaService.getById(id);
    }


    @PostMapping
    @Transactional
    @Representation(PessoaRepresentation.Completa.class)
    public Pessoa create(@RequestBody Pessoa entity) {
        return pessoaService.save(entity);
    }

    @Transactional
    @PutMapping("{id}")
    @Representation(PessoaRepresentation.Completa.class)
    public Pessoa update(@PathVariable("id") Long id, @Valid @RequestBody Pessoa contato) {
        contato.setId(id);
        return pessoaService.save(contato);
    }

    @Transactional
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        pessoaService.remove(id);
        return ResponseEntity.noContent().build();
    }
}
