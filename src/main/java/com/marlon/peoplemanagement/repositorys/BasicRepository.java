package com.marlon.peoplemanagement.repositorys;

import com.marlon.peoplemanagement.common.model.IEntity;
import com.marlon.peoplemanagement.common.model.validation.ValidationException;
import com.marlon.peoplemanagement.common.model.Page;
import com.marlon.peoplemanagement.common.model.Pageable;
import com.marlon.peoplemanagement.common.model.Sort;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.PathBuilderFactory;
import com.querydsl.jpa.JPQLTemplates;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class BasicRepository {

    private final EntityManager em;
    private final JPQLTemplates jpqlTemplate;
    private final PathBuilderFactory pathBuilderFactory;

    public BasicRepository(EntityManager em, JPQLTemplates jpqlTemplate, PathBuilderFactory pathBuilderFactory) {
        this.em = em;
        this.jpqlTemplate = jpqlTemplate;
        this.pathBuilderFactory = pathBuilderFactory;
    }

    public <E> JPAQuery<E> query(Class<E> entityClass) {
        JPAQuery<E> query = new JPAQuery(this.em, this.jpqlTemplate);
        query.from(this.pathBuilderFactory.create(entityClass));
        return query;
    }
    public <E> Page<E> findAll(Class<E> entityClass, Pageable pageable, Predicate... where) {
        JPAQuery<E> query = this.query(entityClass).where(where);
        long total = query.fetchCount();
        long offset = pageable.getOffset();
        long limit = pageable.getLimit();
        if (offset > total) {
            offset = (int)(total / limit) * limit;
        } else if (offset == total) {
            offset = Math.max(offset - limit, 0);
        }

        query.offset(offset);
        query.limit(limit);
        this.applySorting(query, pageable.getSort());
        List<E> content = query.fetch();
        return new Page(content, pageable, total);
    }


    public <E> JPAQuery<E> applySorting(JPAQuery<E> query, Sort sort) {
        Optional.ofNullable(sort).ifPresent((s) -> {
            s.forEach(query::orderBy);
        });
        return query;
    }

    public <E extends IEntity> E save(E entity) {
        Objects.requireNonNull(entity, "A entidade a ser salva não pode ser nula");

        if (entity.getId() == null) {
            em.persist(entity);
        } else {
            em.merge(entity);
        }
        em.flush();
        return entity;
    }

    public <E extends IEntity> void remove(E entity) {
        Objects.requireNonNull(entity, "A entidade a ser removida não pode ser nula");
        try {
            em.remove(entity);
            em.flush();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
    }

    public <E extends IEntity, ID> void remove(Class<E> entityClass, ID id) {
        E entity = find(entityClass, id);
        Objects.requireNonNull(entity, "A entidade a ser removida não pode ser nula");
        try {
            em.remove(entity);
            em.flush();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
    }

    public <E, ID> E find(Class<E> entityClass, ID id) {
        return this.checkIfFound(this.em.find(entityClass, id), entityClass.getSimpleName(), id);
    }

    protected  <E, ID> E checkIfFound(E entity, String simpleName, ID id2) {
        if (entity != null) {
            return entity;
        } else {
            throw new ValidationException(String.format("Não foi encontrado nenhum registro de %s com o id %s", simpleName, id2));
        }
    }

    public <T> T findOrNull(Class<T> entityClass, Serializable id) {
        if (id == null) {
            return null;
        }

        try {
            return find(entityClass, id);
        } catch (EntityNotFoundException e) {
            return null;
        }
    }
}
