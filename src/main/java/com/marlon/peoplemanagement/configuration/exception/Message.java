package com.marlon.peoplemanagement.configuration.exception;

public class Message {

    public Message(String message) {
        this.message = message;
    }

    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
