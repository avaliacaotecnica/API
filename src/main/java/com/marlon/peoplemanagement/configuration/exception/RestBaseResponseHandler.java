package com.marlon.peoplemanagement.configuration.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.WebRequest;

import java.util.*;
import java.util.stream.Collectors;

public class RestBaseResponseHandler {

    public static ResponseEntity<Object> baseResponseHandler(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status) {
        final List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        Map<String, Set<String>> errorsMap =  fieldErrors.stream().collect(
                Collectors.groupingBy(FieldError::getField,
                        Collectors.mapping(FieldError::getDefaultMessage, Collectors.toSet())
                )
        );
        if(errorsMap.isEmpty()){
            errorsMap.put("server", new HashSet(Arrays.asList("Erro interno no servidor.")));
        }
        return new ResponseEntity(errorsMap.isEmpty()? ex:errorsMap, headers, status);
    }
}
