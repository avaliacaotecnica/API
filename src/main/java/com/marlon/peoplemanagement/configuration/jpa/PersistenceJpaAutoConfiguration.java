package com.marlon.peoplemanagement.configuration.jpa;

import com.marlon.peoplemanagement.repositorys.BasicRepository;
import com.querydsl.core.types.dsl.PathBuilderFactory;
import com.querydsl.jpa.JPQLTemplates;
import com.querydsl.jpa.impl.JPAProvider;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

@Configuration
@AutoConfigureAfter({
        HibernateJpaAutoConfiguration.class
})
public class PersistenceJpaAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public PathBuilderFactory pathBuilderFactory() {
        return new PathBuilderFactory();
    }

    @Bean
    @ConditionalOnMissingBean
    public JPQLTemplates jpqlTemplates(EntityManager em) {
        return JPAProvider.getTemplates(em);
    }

    @Bean
    @ConditionalOnMissingBean
    public JPAQueryFactory jpaQueryFactory(EntityManager em) {
        return new JPAQueryFactory(jpqlTemplates(em), em);
    }

    @Bean
    @ConditionalOnMissingBean
    public BasicRepository repository(EntityManager em,
                                      PathBuilderFactory pathBuilderFactory,
                                      JPQLTemplates jpqlTemplate) {
        return new BasicRepository(em, jpqlTemplate, pathBuilderFactory);
    }

}
